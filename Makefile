TARGET = main
all: $(TARGET)

DIRS := $(dir $(wildcard */Makefile))

include common.mk

$(TARGET): dirs
	@$(MAKE) -C $(TARGET)

flash:
	@$(MAKE) flash -C $(TARGET)

debug:
	@$(MAKE) debug -C $(TARGET)

tests:
	@$(MAKE) -C test tests

dirs: driverlib
	@for i in $(filter-out $(TARGET)/ test/,$(DIRS)); \
	 do                                         \
	     $(MAKE) -C $$i || exit $$?;            \
	 done

clean: clean(driverlib)
	@for i in $(DIRS);                          \
	 do                                         \
	     $(MAKE) clean -C $$i || exit $$?;      \
	 done
