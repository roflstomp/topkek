# Default to debug build
ifndef DEBUG
DEBUG := 1
endif

# Tool names
PREFIX = arm-none-eabi-

CC = $(PREFIX)gcc
AS = $(PREFIX)gcc
LD = $(PREFIX)ld
AR = $(PREFIX)ar
GDB = $(PREFIX)gdb
OBJCOPY = $(PREFIX)objcopy
MAKE = make $(and $(DEBUG),DEBUG=1) --no-print-directory

OPENOCDCFG := $(shell find /usr/share/openocd -iname *tm4c123*)
OPENOCD = openocd -f $(OPENOCDCFG)
SCREEN = screen
TELNET = telnet

# Paths for compilation
ROOT := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
TI := $(ROOT)/../Tiva

LIBS += $(wildcard $(TI)/driverlib/gcc*/libdriver*.a)
LIBS += $(shell $(CC) $(CFLAGS) -print-file-name=libm.a)
LIBS += $(shell $(CC) $(CFLAGS) -print-file-name=libc.a)
LIBS += $(shell $(CC) $(CFLAGS) -print-libgcc-file-name)

INCS += $(ROOT)
INCS += $(TI)

# Flag definitions
CFLAGS += -std=c99
CFLAGS += -Wall
CFLAGS += -MD
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += $(addprefix -I, $(INCS))
# lm4f specific definitions
CFLAGS += -mthumb 
CFLAGS += -mcpu=cortex-m4 
CFLAGS += -mfpu=fpv4-sp-d16 -mfloat-abi=softfp
CFLAGS += -Dgcc -DPART_TM4C123GH6PM -DTARGET_IS_BLIZZARD_RB1
# Debug or optimize
ifeq ($(DEBUG), 1)
CFLAGS += -O0 -DDEBUG -g3 -gdwarf-2 -ggdb
else
CFLAGS += -Os
endif

SFLAGS += -x assembler-with-cpp
SFLAGS += $(CFLAGS)

LFLAGS += --gc-sections
LFLAGS += --entry reset_isr


driverlib:
	@$(MAKE) VERBOSE=1 PART=TM4C123GH6PM -C $(TI)/driverlib

clean(driverlib):
	@$(MAKE) VERBOSE=1 PART=TM4C123GH6PM clean -C $(TI)/driverlib

flash(%): %.axf
	$(OPENOCD) -c "init"                           \
	           -c "halt"                           \
	           -c "flash write_image erase $*.axf" \
	           -c "verify_image $*.axf"            \
	           -c "halt"                           \
	           -c "shutdown"

debug(%): %.out %.axf flash(%)
	$(OPENOCD) -c "init" -c "halt" 1>/dev/null 2>/dev/null & \
	$(GDB) $*.out -x tm4c.gdb ; \
	kill $$!

uart:
	$(SCREEN) /dev/tm4c 115200

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.s
	$(AS) -c $(SFLAGS) -o $@ $<

%.a: $(OBJ)
	$(AR) rc $@ $^

%.out: $(OBJ)
	$(LD) -T $(LDNAME) $(LFLAGS) -o $@ $^ $(LIBS)

%.axf: %.out
	$(OBJCOPY) -O binary $< $@

clean(%):
	rm -f $*
