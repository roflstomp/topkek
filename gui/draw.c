/* filename ******** draw.c **************
 *
 * Double buffered drawing on TFT display
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#include "lib/types.h"
#include <string.h>
#include "lib/common.h"
#include "gui/draw.h"
#include "gui/font.h"
#include "periph/tft.h"

// static uint16_t display_buffer[TFT_WIDTH * TFT_HEIGHT];

static uint16_t convert_color(uint32_t color) {
    return ((color & (0xff <<  0)) >> (    3)) |
           ((color & (0xff <<  8)) >> (  2+3)) |
           ((color & (0xff << 16)) >> (3+2+3));
}

void draw_rect(int x, int y, int height, int width, int color) {
    draw_line(x, y, x + height, y, color);
    draw_line(x + height, y, x + height, y + width, color);
    draw_line(x + height, y + width, x, y + width, color);
    draw_line(x, y + width, x, y, color);
}

void draw_line(int x0, int y0, int x1, int y1, int color) {
    int dx = abs(x1-x0);
    int dy = abs(y1-y0);
    int sx = (x0 < x1) ? 1 : -1;
    int sy = (y0 < y1) ? 1 : -1;

    int err = dx - dy;

    while (true) {
        int err2 = 2*err;

        draw_point(x0, y0, color);

        if (x0 == x1 && y0 == y1)
            break;

        if (err2 > -dy) {
            err = err - dy;
            x0 = x0 + sx;
        }

        if (x0 == x1 && y0 == y1)
            break;

        if (err2 < dx) {
            err = err + dx;
            y0 = y0 + sy;
        }
    }

    draw_point(x1, y1, color);
}

void draw_point(int x0, int y0, int color) {
    if (y0 >= TFT_WIDTH || y0 < 0) return;
    if (x0 >= TFT_HEIGHT || x0 < 0) return;
    tft_set_pixel(y0, x0, convert_color(color));
}

void draw_circle(int x0, int y0, int r, int c) {
    int x = r, y = 0;
    int radiusError = 1-x;

    while(x >= y) {
        draw_point(x + x0, y + y0, c);
        draw_point(y + x0, x + y0, c);
        draw_point(-x + x0, y + y0, c);
        draw_point(-y + x0, x + y0, c);
        draw_point(-x + x0, -y + y0, c);
        draw_point(-y + x0, -x + y0, c);
        draw_point(x + x0, -y + y0, c);
        draw_point(y + x0, -x + y0, c);

        y++;
        if(radiusError<0)
            radiusError+=2*y+1;
        else {
            x--;
            radiusError+=2*(y-x+1);
        }
    }
}

void draw_char(char ch, int x, int y, int color) {
    int xi;
    int yi;

    if (ch >= ' ' && ch <= '~') {
        tft_set_rect(x, y, FONT_WIDTH, FONT_HEIGHT);

        for (xi=0; xi < FONT_WIDTH; xi++) {
            unsigned char cdata = g_pucFont[ch-' '][xi];

            for (yi=0; yi < FONT_HEIGHT; yi++) {
                tft_set_color(1 & (cdata >> yi) ? convert_color(color) : 0);
            }
        }

        tft_clear_rect();
    }
}


void draw_string(char* text, int x, int y, int color) {
    for (; *text != 0; text++) {
        if (*text >= ' ') {
            draw_char(*text, x, y, color);
            x += FONT_WIDTH+1;
        }
    }
}

void draw_clear(void) {
    tft_clear();
}

