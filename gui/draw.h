/* filename ******** draw.h **************
 *
 * Double buffered drawing on TFT display
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#ifndef __DRAW_H
#define __DRAW_H

#include "font.h"
#define RITWIDTH 128
#define RITHEIGHT 96

extern unsigned char RITbuffer[RITWIDTH/2 * RITHEIGHT];

void draw_point(int x0, int y0, int color);
void draw_line(int x0, int y0, int x1, int y1, int color);
void draw_rect(int x, int y, int height, int width, int color);
void draw_circle(int x0, int y0, int r, int c);
void draw_char(char ch, int x, int y, int color);
void draw_string(char* text, int x, int y, int color);

void draw_clear(void);
void draw_flip(void);

#endif
