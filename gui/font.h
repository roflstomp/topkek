/* filename ******** font.c **************
 *
 * Font definition to save space
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#ifndef __FONT_H
#define __FONT_H

#include "gui/font.h"
#include "lib/types.h"

#define FONT_WIDTH 5
#define FONT_HEIGHT 7

const extern uint8_t g_pucFont[96][5];

#endif
