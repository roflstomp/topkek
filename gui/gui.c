/* filename ******** gui.c **************
 *
 * Provides GUI library
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/8/2014
 */

#include "lib/types.h"
#include "lib/llist.h"
#include "gui/gui.h"
#include "gui/draw.h"
#include <string.h>

#define WIN_TITLE_HEIGHT (FONT_HEIGHT+4)
#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))

//static int depth; //, max_color, mid_color, min_color;
static bool dirty, draw;
static struct content *current_selection;
struct content main_window;

void gui_draw_button(struct content* but, coord from, coord bound);
void gui_draw_text(struct content* _text, coord from, coord bound);
void gui_draw_item(struct content* cont, coord from, coord bound);
void gui_draw_container(struct content* win, coord from, coord bound);
struct content* move_focus(struct content *in, dir d);

static bool actionable(struct content *a)
{ return (a->type & GUI_CONT_BUTTON); }

// TODO(kilogram): Add wrapping
bool in_window(struct content *a, struct content *b) {
    while(a->type != GUI_CONT_WINDOW)
        a = a->parent;
    while(b->type != GUI_CONT_WINDOW)
        b = b->parent;
    return a == b;
}

int adjacency_score(struct content *a, struct content *b) {
    int x_over = max(0,
            min(a->corner.x + a->bound.x,
                b->corner.x + b->bound.x) -
            max(a->corner.x, b->corner.x));
    int y_over = max(0,
            min(a->corner.y + a->bound.y,
                b->corner.y + b->bound.y) -
            max(a->corner.y, b->corner.y));
    if (x_over > 0 && (a->corner.y + a->bound.y == b->corner.y ||
                       b->corner.y + b->bound.y == a->corner.y))
        return x_over;
    if (y_over > 0 && (a->corner.x + a->bound.x == b->corner.x ||
                       b->corner.x + b->bound.x == a->corner.x))
        return y_over;
    return 0;
}

bool in_direction(struct content *a, struct content *b, dir d) {
    if (d == LEFT) return a->corner.y < b->corner.y;
    else if (d == RIGHT) return a->corner.y > b->corner.y;
    else if (d == UP) return a->corner.x < b->corner.x;
    else return a->corner.x > b->corner.x;
}

int container_length(struct content *container) {
    int len = 0;
    struct _llist *e;
    foreach(e, &container->_e.container.contents)
        len++;
    return len;
}

void gui_init(char* name, int x, int y, int z) {
    coord start = {0, 0};
    gui_init_window(&main_window, "main", start, x, y, name);
    main_window.bound.y = y;
    main_window.bound.x = x;
    main_window.parent = NULL;
    main_window.type = GUI_CONT_WINDOW;
    current_selection = &main_window;
    //buffer = buf;
    // depth = z;
    draw = dirty = false;
}

void gui_init_window(struct content* win, char* name,
        coord corner, int height, int width, char* title) {
    strncpy(win->name, name, 10);
    win->name[10] = '\0';
    // win->dirty = true;
    win->corner = corner;
    win->_e.window._text = title;
    win->bound.x = corner.x + height;
    win->bound.y = corner.y + width;
}

void gui_init_button(struct content* cont, gui_cb_func bt_cb,
        char* name, char* _text) {
    strncpy(cont->name, name, 10);
    cont->name[10] = '\0';
    cont->type = GUI_CONT_BUTTON;
    // cont->dirty = true;
    // cont->corner = corner;
    cont->_e.button._text = _text;
    cont->_e.button.length = strlen(_text);
    cont->_e.button.state = GUI_BUTTON_NONE;
    cont->_e.button.cb = bt_cb;
    current_selection = cont;
}

void gui_init_textbox(struct content* cont, char* name, char* _text) {
    strncpy(cont->name, name, 10);
    cont->name[10] = '\0';
    cont->type = GUI_CONT_TEXT;
    cont->_e.text._text = _text;
}

void gui_init_hbox(struct content* cont) {
    cont->type = GUI_CONT_HBOX;
    cont->name[0] = '\0';
    // cont->dirty = true;
    llist_init(&cont->_e.container.contents);
}

void gui_init_vbox(struct content* cont) {
    cont->type = GUI_CONT_VBOX;
    cont->name[0] = '\0';
    // cont->dirty = true;
    llist_init(&cont->_e.container.contents);
}

void gui_update_textbox(struct content* cont, char* _text) {
    dirty = true;
    if (cont->type == GUI_CONT_TEXT)
        cont->_e.text._text = _text;
}

void gui_update_button(struct content* cont, gui_cb_func bt_cb, char* _text) {
    dirty = true;
    if(cont->type != GUI_CONT_BUTTON) return;
    // cont->dirty = true;
    // cont->corner = corner;
    cont->_e.button._text = _text;
    cont->_e.button.length = strlen(_text);
    cont->_e.button.cb = bt_cb;
}


void gui_add_content(struct content* cont, struct content* obj) {
    dirty = true;
    if ((cont->type & (GUI_CONT_WINDOW | GUI_CONT_HBOX | GUI_CONT_VBOX))
            == 0x00)
        return;
    if (cont->type == GUI_CONT_WINDOW)
        cont->_e.window.content = obj;
    else
        llist_append(&cont->_e.container.contents, &obj->node);
    obj->parent = cont;
}

void gui_rem_content(struct content* cont, struct content* obj) {
    struct content *parent;
    dirty = true;
    if ((cont->type & (GUI_CONT_WINDOW | GUI_CONT_HBOX | GUI_CONT_VBOX))
            == 0x00)
        return;
    if (cont->type == GUI_CONT_WINDOW) cont->_e.window.content = NULL;
    else                               llist_remove(&obj->node);
    obj->parent = NULL;

    parent = current_selection;
    while (parent->type != GUI_CONT_INV && parent != cont)
        parent = parent->parent;
    if (parent == cont) current_selection = &main_window;
    // Perform search for obj in cont
}

void gui_draw_item(struct content* cont, coord from, coord bound) {
    switch(cont->type) {
        case GUI_CONT_WINDOW:
        case GUI_CONT_HBOX:
        case GUI_CONT_VBOX:
            gui_draw_container(cont, from, bound); break;
        case GUI_CONT_BUTTON:
            gui_draw_button(cont, from, bound); break;
        case GUI_CONT_TEXT:
            gui_draw_text(cont, from, bound); break;
        case GUI_CONT_INV:
            break;  // Raise error?
    }
}

void gui_draw_text(struct content* txt, coord from, coord bound) {
    // Actual drawing code
    txt->corner = from;
    txt->bound = bound;
    draw_string(txt->_e.text._text,
                from.x + (bound.x/2 - FONT_HEIGHT),
                from.y,
                15);
}

void gui_draw_button(struct content* but, coord from, coord bound) {
    // Actual drawing code
    // TODO(kilogram): Different fill options
    int string_color = 15;
    but->corner = from;
    but->bound = bound;
    //coord act = {from.x + (bound.x/2) - FONT_HEIGHT - 1,
    //    from.y + (bound.y/2) - but->button.length*FONT_WIDTH - 1};
    //draw_rect(act.x - 2, act.y - 2, FONT_HEIGHT + 3,
    //    but->button.length*FONT_HEIGHT, max_color);

    if (but->_e.button.state != GUI_BUTTON_NONE) string_color = 4;
    //if (but->button.state == GUI_BUTTON_HOVER)
    //    draw_rect(act.x + 1, act.y + 1, FONT_HEIGHT,
    //        but->button.length*FONT_HEIGHT - 3, 8);
    //if (but->button.state == GUI_BUTTON_PRESS)
    //    draw_rect(act.x + 1, act.y + 1, FONT_HEIGHT,
    //        but->button.length*FONT_HEIGHT - 3, 15);

    draw_string(but->_e.button._text, from.x, from.y, string_color);
}

void gui_draw_container(struct content* win, coord from, coord bound) {
    struct _llist *e;

    if (win->type == GUI_CONT_WINDOW)
    {
        from.x += WIN_TITLE_HEIGHT;
        draw_rect(from.x, from.y, bound.x, bound.y, 15);
        draw_line(from.x + WIN_TITLE_HEIGHT, from.y,
            from.x + WIN_TITLE_HEIGHT, from.y + bound.y, 15);
        draw_string(win->_e.window._text, from.x + 2, from.y + 4, 15);
        from.x += WIN_TITLE_HEIGHT+2;
        from.y += 2;
        bound.x -= WIN_TITLE_HEIGHT+2;
        bound.y -= 2;
        gui_draw_item(win->_e.window.content, from, bound);
        return;
    }
    if (win->type == GUI_CONT_VBOX) bound.x /= container_length(win);
    else if (win->type == GUI_CONT_HBOX) bound.y /= container_length(win);
    else return; // Raise error?

    foreach(e, &win->_e.container.contents)
    {
        struct content* cont = llist_get(struct content, e, node);
        gui_draw_item(cont, from, bound);
        if (win->type == GUI_CONT_VBOX) from.x += bound.x;
        else if (win->type == GUI_CONT_HBOX) from.y += bound.y;
    }
}

void gui_draw() {
    if (!draw) return;
    dirty = false;
    if (current_selection == NULL) current_selection = &main_window;
    draw_clear();
    gui_draw_container(&main_window, main_window.corner, main_window.bound);
}

bool gui_on() {
    return draw;
}

void GUI_SetOn() {
    draw = true;
}

void gui_set_off(struct content* UNUSED) {
    draw_clear();
    draw = false;
}

void gui_selection() {
    dirty = true;
    if (draw == false)
    {
        draw = true;
        return;
    }
    if (dirty == false) dirty = true;
    if (current_selection->type == GUI_CONT_BUTTON &&
        current_selection->_e.button.cb != NULL)
        current_selection->_e.button.cb(current_selection);
}

void gui_move_focus(dir d) {
    struct content *best, *parent;
    parent = current_selection;
    while (parent->type != GUI_CONT_WINDOW)
        parent = parent->parent;
    best = move_focus(parent, d);
    if (current_selection->type == GUI_CONT_BUTTON)
        current_selection->_e.button.state = GUI_BUTTON_NONE;
    if (best->type == GUI_CONT_BUTTON)
        best->_e.button.state = GUI_BUTTON_HOVER;
    current_selection = best;
    dirty = true;
}

struct content* move_focus(struct content *in, dir d) {
    struct content *best = current_selection;
    int best_score = 0, cur_score = 0; if (in->type == GUI_CONT_WINDOW)
        return move_focus(in->_e.window.content, d);
    else if (in->type & (GUI_CONT_HBOX | GUI_CONT_VBOX)) {
        struct _llist *e;
        foreach(e, &in->_e.container.contents) {
            struct content *cont = llist_get(struct content, e, node);
            if (cont->type & (GUI_CONT_HBOX | GUI_CONT_VBOX)) {
                struct content *t_best = move_focus(cont, d);
                if ((cur_score = adjacency_score(t_best, current_selection)) >
                        best_score) {
                    best_score = cur_score;
                    best = t_best;
                }
            }
            else if (actionable(cont)) {
                if ((cur_score = adjacency_score(cont, current_selection)) >
                        best_score &&
                        in_direction(cont, current_selection, d)) {
                    best_score = cur_score;
                    best = cont;
                }
            }
        }
    }
    else return in;
    return best;
}

