/* filename ******** gui.h **************
 *
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/8/2014
 */

#include "lib/llist.h"

struct content;
typedef enum {
    GUI_CONT_INV    = 0x00,
    GUI_CONT_WINDOW = 0x01,
    GUI_CONT_HBOX   = 0x02,
    GUI_CONT_VBOX   = 0x04,
    GUI_CONT_BUTTON = 0x08,
    GUI_CONT_TEXT   = 0x10
} content_type;
typedef enum { UP = 0x01,
            DOWN = 0x02,
            LEFT = 0x04,
            RIGHT = 0x08 } dir;
typedef struct { int x; int y; } coord;

typedef void gui_cb_func (struct content*);

struct content {
    struct _llist node;
    char name[11];
    // bool dirty;
    coord corner, bound;
    struct content* parent;
    content_type type;
    union {
        struct button {
            char* _text;
            enum { GUI_BUTTON_NONE, GUI_BUTTON_HOVER, GUI_BUTTON_PRESS } state;
            int length;
            gui_cb_func *cb;
        } button;
        struct container {
            struct llist contents;
        } container;
        struct window {
            char* _text;
            struct content* content;
        } window;
        struct text {
            char* _text;
        } text;
    } _e;
};
extern struct content main_window;


void gui_init(char* name, int x_size, int y_size, int depth);

void gui_init_window(struct content*, char* name,
        coord corner, int height, int width, char* title);
void gui_init_button(struct content*, gui_cb_func bt_cb,
        char* name, char* text);
void gui_init_textbox(struct content*, char* name, char* _text);
void gui_init_hbox(struct content*);
void gui_init_vbox(struct content*);

void gui_update_button(struct content* cont, gui_cb_func bt_cb, char* _text);
void gui_update_textbox(struct content*, char* _text);

void gui_add_content(struct content* container, struct content* content);
void gui_rem_content(struct content* container, struct content* content);

// TODO(kilogram): For now, exporting MoveFocus
// but should be switch to actionlistner model.
void gui_move_focus(dir d);
void gui_selection(void);

bool gui_on(void);
void gui_set_on(void);
void gui_set_off(struct content*);
void gui_draw(void);
