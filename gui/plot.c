/* filename ******** plot.h **************
 *
 * Plotting
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#include "plot.h"
#include <periph/tft.h>

static uint_t pos;

void plot_init(void) {
    tft_init();
    pos = 0;
}

void plot_next(float value) {
    plot_at(pos, value);
    pos = (pos+1) % (TFT_WIDTH-1);
}

void plot_at(uint_t x, float value) {
    tft_set_rect(x, 0, 1, TFT_HEIGHT);
    
    for (uint_t i = 0; i < TFT_HEIGHT; i++) {
        tft_set_color(i > TFT_HEIGHT*(1.0f-value) ? 0xffffff : 0x000000);
    }
}
