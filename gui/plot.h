/* filename ******** plot.h **************
 *
 * Plotting
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#ifndef __PLOT_H
#define __PLOT_H

#include <lib/types.h>

void plot_init(void);
void plot_next(float value);
void plot_at(uint_t x, float value);

#endif // __PLOT_H
