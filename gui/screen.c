/* filename ******** tft.c **************
 *
 * Driver for the TFT_320QVT
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "gui/font.h"
#include <periph/tft.h>
#include <os/sync.h>

spinlock_t screen_lock;

uint16_t left_x = 0, left_y = 0;
uint16_t right_x = 28, right_y = 0;

void screen_init(void) {
    tft_init();
    spinlock_init(&screen_lock);
}

void screen_putc_left(char ch) {
    int xi;
    int yi;

//    spinlock_acquire(&screen_lock);

    if (ch >= ' ' && ch <= '~') {
        tft_set_rect(left_x*(FONT_WIDTH+1), left_y*(FONT_HEIGHT+1),
                     FONT_WIDTH, FONT_HEIGHT);

        for (xi=0; xi < FONT_WIDTH; xi++) {
            unsigned char cdata = g_pucFont[ch-' '][xi];

            for (yi=0; yi < FONT_HEIGHT; yi++) {
                tft_set_color(1 & (cdata >> yi) ? 0xffff : 0x0000);
            }
        }

        tft_clear_rect();

        left_x += 1;

    } else if (ch == '\n') {
        left_x = 0;
        left_y += 1;

        if (left_y > 28)
            left_y = 0;

    } else if (ch == '\r') {
        left_x = 0;
    }

//    spinlock_release(&screen_lock);
}

void screen_putc_right(char ch) {
    int xi;
    int yi;

//    spinlock_acquire(&screen_lock);

    if (ch >= ' ' && ch <= '~') {
        tft_set_rect(right_x*(FONT_WIDTH+1), right_y*(FONT_HEIGHT+1),
                     FONT_WIDTH, FONT_HEIGHT);

        for (xi=0; xi < FONT_WIDTH; xi++) {
            unsigned char cdata = g_pucFont[ch-' '][xi];

            for (yi=0; yi < FONT_HEIGHT; yi++) {
                tft_set_color(1 & (cdata >> yi) ? 0xffff : 0x0000);
            }
        }

        tft_clear_rect();

        right_x += 1;

    } else if (ch == '\n') {
        right_x = 28;
        right_y += 1;

        if (right_y > 28)
            right_y = 0;

    } else if (ch == '\r') {
        right_x = 28;
    }

//    spinlock_release(&screen_lock);
}
