/* filename ******** screen.h **************
 *
 * Thread safe screen printing using the tft
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#ifndef __SCREEN_H
#define __SCREEN_H

#include <os/sync.h>
#include <io/print.h>

extern spinlock_t screen_lock;
extern uint16_t left_x, left_y;
extern uint16_t right_x, right_y;

void screen_init(void);
void screen_putc_left(char c);  // NOT thread safe atm
void screen_putc_right(char c); //

// Horrible hack here cause I'm too lazy to
// figure out things with varargs
#define screen_message(screen, offset, ...) ({  \
    spinlock_acquire(&screen_lock);             \
    if (screen == 0) {                          \
        left_x = 0;                             \
        left_y = offset;                        \
        cprintf(screen_putc_left, __VA_ARGS__); \
    } else {                                    \
        right_x = 28;                           \
        right_y = offset;                       \
        cprintf(screen_putc_right, __VA_ARGS__);\
    }                                           \
    spinlock_release(&screen_lock);             \
})

#endif
