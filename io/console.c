/* filename ******** console.c **************
 *
 * Command line interpreter
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */

#include "console.h"

#include <string.h>

#include <io/print.h>
#include <lib/vector.h>
#include <lib/mem.h>
#include <os/sync.h>
#include <inc/hw_memmap.h>
#include <inc/hw_ints.h>
#include <driverlib/pin_map.h>
#include <driverlib/gpio.h>
#include <driverlib/uart.h>
#include <driverlib/sysctl.h>
#include <driverlib/interrupt.h>

struct command {
    const char *name;
    const char *help;
    struct task *task;
};

// Command table
static struct vector commands;
static uint_t command_count;

// Input buffer
static char input_buffer[CONSOLE_INPUT_SIZE];
static uint_t input_count;


// Example help command
TASK(command_help, CONSOLE_PRIORITY, 128)(void) {
    console_printf("commands:\n");

    for (uint_t i = 0; i < command_count; i++) {
        struct command *command = vector_get(&commands, i);
        console_printf("     %s - %s\n", command->name, command->help);
    }
}

// Default command
TASK(command_not_found, CONSOLE_PRIORITY, 128)(void) {
    console_printf("Command \"%s\" not found\n", input_buffer);
}


void console_init(void) {
    // Start with no commands or input
    vector_init(&commands);
    command_count = 0;
    input_count = 0;

    // Setup default commands
    console_register("help", "prints this message", command_help_task);

    // Initialize UART
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // configure pin muxing
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // setup UART for console IO
    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
                        (UART_CONFIG_PAR_NONE |
                         UART_CONFIG_STOP_ONE |
                         UART_CONFIG_WLEN_8));

    // enable uart
    UARTIntEnable(UART0_BASE, UART_INT_RX);
    IntEnable(INT_UART0);
    UARTEnable(UART0_BASE);

    // This must be disabled to interrupt on single characters
    // and it must be disabled after UARTEnable which enables
    // fifo for whatever reason
    UARTFIFODisable(UART0_BASE);
}

void console_register(const char *name, const char *help,
                      struct task *task) {
    // TODO Yes I know this is wrong,
    // just want to get this code out there before semaphores
    // are introduced to the system
    uint_t gl = gl_acquire();

    struct command *command = mem_alloc(sizeof(struct command));
    command->name = name;
    command->help = help;
    command->task = task;

    vector_set(&commands, command_count++, command);

    gl_release(gl);
}

static void console_putc(char c) {
    UARTCharPut(UART0_BASE, c);
}

void console_printf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    cvprintf(console_putc, fmt, args);

    va_end(args);
}

static void console_spawn(void) {
    // find everything after the command
    char *args = input_buffer;

    while (*args != 0 && *args != ' ') {
        args++;
    }

    if (args != 0) {
        *args = 0;
        args++;
    }

    // Find the (now null terminated) command
    struct task *task = 0;
    for (uint_t i = 0; i < command_count; i++) {
        struct command *console = vector_get(&commands, i);

        if (strcmp(console->name, input_buffer) == 0) {
            task = console->task;
            break;
        }
    }

    // spawn the thread
    thread_spawn(task ?: command_not_found_task, args);

    // this will be fine as long as no one types much in a command
    input_count = 0;
}

void console_isr(void) {
    UARTIntClear(UART0_BASE, UART_INT_RX);

    char c = UARTCharGetNonBlocking(UART0_BASE);

    switch (c) {
        case 0x7f:
        case '\b':
            if (input_count > 0) {
                input_count--;
                UARTCharPut(UART0_BASE, '\b');
                UARTCharPut(UART0_BASE, ' ');
                UARTCharPut(UART0_BASE, '\b');
            }
            break;

        case '\n':
            break;

        case '\r':
            input_buffer[input_count++] = 0;
            UARTCharPut(UART0_BASE, '\r');
            UARTCharPut(UART0_BASE, '\n');
            console_spawn();
            break;

        default:
            input_buffer[input_count++] = c;
            UARTCharPutNonBlocking(UART0_BASE, c);
            break;
    }
}

