/* filename ******** console.h **************
 *
 * Command line interpreter
 *
 * TODO decouple this from UART?
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */

#include <os/thread.h>

#define CONSOLE_INPUT_SIZE 128
#define CONSOLE_PRIORITY 0x7f

void console_init(void);

void console_printf(const char *fmt, ...);

void console_register(const char *name, const char *help,
                      struct task *task);

