/* filename ******** fat.c **************
 *
 * FAT filesystem driver
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include <lib/common.h>
#include <lib/types.h>
#include "fat.h"
#include "fs.h"
#include "io.h"

#include <ctype.h>
#include <string.h>

#define ROUND(x, y) ((x) - ((x) % (y)))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

// Macros for FAT field access
#define ALLOW_UNALIGNED_ACCESS
#ifndef ALLOW_UNALIGNED_ACCESS
#define ALIGN(AT, S) ((uint32_t)(AT) & ~((S) - 1))
#define BYTESL(AT, B) (8 * (uint32_t)(AT - ALIGN(AT, B)))
#define BYTESR(AT, B) (8 * (B - (uint32_t)(AT - ALIGN(AT, B))))

#define HIGHER_BITS(AT, B, T)       \
    (((*((T*)(ALIGN(AT, B) + B))) & \
      ((1 << BYTESL(AT, B)) - 1)) <<\
     BYTESR(AT, B))
#define LOWER_BITS(AT, B, T)        \
    (((*((T*)ALIGN(AT, B))) >>      \
      BYTESL(AT, B)) &              \
     ((1 << BYTESR(AT, B)) - 1))

#define ALIGNED(X, AT, S)                               \
    (HIGHER_BITS(((X) + (AT)), (S/8), uint##S##_t) |   \
     LOWER_BITS(((X) + (AT)), (S/8), uint##S##_t))

#else

#define ALIGNED(X, AT, S) (*((uint##S##_t*) (X + AT)))

#endif

#define BPB_BytsPerSec(X)   (ALIGNED(X, 11, 16))
#define BPB_SecPerClus(X)   (ALIGNED(X, 13, 8))
#define BPB_RsvdSecCnt(X)   (ALIGNED(X, 14, 16))
#define BPB_NumFATs(X)      (ALIGNED(X, 16, 8))
#define BPB_RootEncCnt(X)   (ALIGNED(X, 17, 16))
#define BPB_TotSec16(X)     (ALIGNED(X, 19, 16))
#define BPB_Media(X)        (ALIGNED(X, 21, 8))
#define BPB_FATSz16(X)      (ALIGNED(X, 22, 16))
#define BPB_SecPerTrk(X)    (ALIGNED(X, 24, 16))
#define BPB_NumHeads(X)     (ALIGNED(X, 26, 16))
#define BPB_HiddSec(X)      (ALIGNED(X, 28, 32))
#define BPB_TotSec32(X)     (ALIGNED(X, 32, 32))

#define BS_16_DrvNum(X)     (ALIGNED(X, 36, 8))
#define BS_16_Reserved1(X)  (ALIGNED(X, 37, 8))
#define BS_16_BootSig(X)    (ALIGNED(X, 38, 8))
#define BS_16_VolID(X)      (ALIGNED(X, 39, 32))
#define BS_16_VolLab(X)     ((uint8_t[11])  &X[43])
#define BS_16_FilSysType(X) ((uint8_t[8])   &X[54])
//Not to be used to check filesystem type

#define BPB_FATSz32(X)      (ALIGNED(X, 36, 32))
#define BPB_ExtFlags(X)     (ALIGNED(X, 40, 16))
#define BPB_FSVer(X)        (ALIGNED(X, 42, 16))
#define BPB_RootClus(X)     (ALIGNED(X, 44, 32))
#define BPB_FSInfo(X)       (ALIGNED(X, 48, 16))
#define BPB_BkBootSec(X)    (ALIGNED(X, 50, 16))
#define BPB_Reserved(X)     ((uint8_t[12])  &X[52])
#define BS_32_DrvNum(X)     (ALIGNED(X, 64, 8))
#define BS_32_Reserved1(X)  (ALIGNED(X, 65, 8))
#define BS_32_BootSig(X)    (ALIGNED(X, 66, 8))
#define BS_32_VolID(X)      (ALIGNED(X, 67, 32))
#define BS_32_VolLab(X)     ((uint8_t[11])  &X[71])
#define BS_32_FilSysType(X) ((uint8_t[8])   &X[82])

#define DIR_Name(X)         ((uint8_t*) X)
#define DIR_Attr(X)         (ALIGNED(X, 11, 8))
#define DIR_NTRes(X)        (ALIGNED(X, 12, 8))
#define DIR_CrtTimeTenth(X) (ALIGNED(X, 13, 8))
#define DIR_CrtTime(X)      (ALIGNED(X, 14, 16))
#define DIR_CrtDate(X)      (ALIGNED(X, 16, 16))
#define DIR_LstAccTime(X)   (ALIGNED(X, 18, 16))
#define DIR_FClusHI(X)      (ALIGNED(X, 20, 16))
#define DIR_WrtTime(X)      (ALIGNED(X, 22, 16))
#define DIR_WrtDate(X)      (ALIGNED(X, 24, 16))
#define DIR_FClusLO(X)      (ALIGNED(X, 26, 16))
#define DIR_FileSize(X)     (ALIGNED(X, 28, 32))

#define DIR_ATTR_RDONLY     0x01
#define DIR_ATTR_HIDDEN     0x02
#define DIR_ATTR_SYSTEM     0x04
#define DIR_ATTR_VOL_ID     0x08
#define DIR_ATTR_DIR        0x10
#define DIR_ATTR_ARCHIVE    0x20
#define DIR_ATTR_IGNORE     (DIR_ATTR_RDONLY | DIR_ATTR_HIDDEN | \
                             DIR_ATTR_SYSTEM | DIR_ATTR_VOL_ID)

#define BPB_SigChk(X)       (ALIGNED(X, 510, 16) == 0xAA55)
// TODO(kilogram): Check this pl0x
#define BPB_TotSecChk(X)    \
    ((BPB_TotSec16(X) == 0) != (BPB_TotSec32(X) == 0))
#define BPB_BytsClusChk(X)  \
    ((BPB_BytsPerSec(X) * BPB_SecPerClus(X)) <= (32 * 1024))

// TODO(kilogram): Perhaps implement FSInfo sector reading

static block_t sector_in_cluster(device_t *dev, block_t clus, off_t sec) {
    return ((clus-2) * dev->_device.fat.clussecs) +
        dev->_device.fat.first_sec + sec;
}

static bool chain_end(device_t *dev, uint32_t entry) {
    switch (dev->fs) {
        case FS_FAT12: return (entry >= 0x0ff8);
        case FS_FAT16: return (entry >= 0xfff8);
        case FS_FAT32: return (entry >= 0x0ffffff8);
        default: return false;
    }
}

static uint32_t get_cluster_entry(device_t *dev, block_t clus) {
    // NOTE(kilogram): We can add fatsz to get to the second FAT
    uint32_t cluster_entry;
    block_t secnum;
    size_t entoffset;

    switch (dev->fs) {
        case FS_FAT12:
            secnum = dev->_device.fat.rsvdsec_cnt + ((clus + (clus / 2)) /
                    dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus + (clus / 2)) % dev->_device.fat.secbytes;

            if (entoffset == (dev->_device.fat.secbytes - 1)) {
                // This access spans sectors. Rejoice.
                // Try to always load two sectors
            }

            dev->read_sector(secnum, dev->buffer);
            cluster_entry = (uint32_t) *((uint16_t*) &dev->buffer[entoffset]);

            if (clus & 0x01)
                return cluster_entry >> 4;
            else
                return cluster_entry & 0x0fff;

        case FS_FAT16:
            secnum = dev->_device.fat.rsvdsec_cnt +
                (clus * 2 / dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus * 2) % dev->_device.fat.secbytes;

            dev->read_sector(secnum, dev->buffer);
            return (uint32_t) *((uint16_t*) &dev->buffer[entoffset]);

        case FS_FAT32:
            secnum = dev->_device.fat.rsvdsec_cnt +
                (clus * 4 / dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus * 4) % dev->_device.fat.secbytes;

            dev->read_sector(secnum, dev->buffer);
            return *((uint32_t*) &dev->buffer[entoffset]) & 0x0fffffff;

        default:
            return 0;
    }
}

static void set_cluster_entry(struct device* dev, block_t clus, int32_t entry) {
    block_t secnum;
    size_t entoffset;

    // NOTE(kilogram): We can add fatsz to get to the second FAT
    // TODO(kilogram): Loop for each fat
    switch (dev->fs) {
        case FS_FAT12:
            secnum = dev->_device.fat.rsvdsec_cnt + ((clus + (clus / 2)) /
                    dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus + (clus / 2)) % dev->_device.fat.secbytes;

            if (entoffset == (dev->_device.fat.secbytes - 1)) {
                // This access spans sectors. Rejoice.
                // Try to always load two sectors
            }

            dev->read_sector(secnum, dev->buffer);

            if (clus & 0x01)
                *((uint16_t*) &dev->buffer[entoffset]) =
                    (uint16_t) ((entry << 4) & 0x0fff);
            else
                *((uint16_t*) &dev->buffer[entoffset]) =
                    (uint16_t) (entry | 0xf000);

            dev->write_sector(secnum, dev->buffer);
            return;

        case FS_FAT16:
            secnum = dev->_device.fat.rsvdsec_cnt +
                (clus * 2 / dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus * 2) % dev->_device.fat.secbytes;

            dev->read_sector(secnum, dev->buffer);
            *((uint16_t*) &dev->buffer[entoffset]) = (uint16_t) entry;
            dev->write_sector(secnum, dev->buffer);
            return;

        case FS_FAT32:
            secnum = dev->_device.fat.rsvdsec_cnt +
                (clus * 4 / dev->_device.fat.secbytes) + dev->offset;
            entoffset = (clus * 4) % dev->_device.fat.secbytes;

            dev->read_sector(secnum, dev->buffer);
            *((uint32_t*) &dev->buffer[entoffset]) =
                0xf0000000 | (0x0fffffff & (uint32_t) entry);
            dev->write_sector(secnum, dev->buffer);
            return;

        case FS_NONE:
            unreachable();
    }
}

static uint32_t alloc_cluster_entry(device_t *dev) {
    struct fat *fatdev = &dev->_device.fat;

    uint32_t max_clus = (dev->size /
            (fatdev->clussecs * fatdev->secbytes)) - 2;
    for (uint32_t new_clus = 2; new_clus < max_clus; new_clus++)
        if (get_cluster_entry(dev, new_clus) == 0)
            return new_clus;

    return -1;
}

static void name_from_FAT(const uint8_t* FAT_name, char* buf) {
    int i, j;

    for(i = 0, j = 0; i < 8; i++, j++) {
        if (FAT_name[i] == ' ') break;
        buf[j] = tolower(FAT_name[i]);
    }
    buf[j++] = '.';
    i = 8;

    for(; i < 11; i++, j++) {
        if (FAT_name[i] == ' ') break;
        buf[j] = tolower(FAT_name[i]);
    }

    buf[j] = '\0';
}

static void name_to_FAT(const char *name, uint8_t *buf) {
    // NOTE: does NOT write NULL-terminator to buf, as it is expected to be a
    // directory entry
    int i, j;
    for (i = 0, j = 0; j < 8; i++, j++) {
        if (name[i] == '\0' || name[i] == '.') break;
        buf[j] = toupper(name[i]);
    }
    for (; j < 8; j++) {
        buf[j] = ' ';
    }

    while (name[i] != '\0' && name[i] != '.') i++;
    if(name[i]) i++;

    for (; j < 11; i++, j++) {
        if (name[i] == '\0') break;
        buf[j] = toupper(name[i]);
    }
    for (; j < 11; j++) {
        buf[j] = ' ';
    }
}

static int namecmp(const char* name, uint8_t* FAT_name) {
    int i = 0, diff = 0;

    for(i = 0; i < 8; i++) {
        if (FAT_name[i] == ' ' || *name == 0) break;
        diff = ((*name++ & ~0x60) | 0x40) - FAT_name[i];
        if (diff != 0) return diff;
    }
    if (*name == 0) {
        for (; i < 11; i++)
            if (FAT_name[i] != ' ') return -1;
        return 0;
    }
    diff = *name++ - '.';
    if (diff != 0) return diff;
    for(; i < 11; i++) {
        if (FAT_name[i] == ' ' || *name == 0) break;
        diff = ((*name++ & ~0x60) | 0x40) - FAT_name[i];
        if (diff != 0) return diff;
    }
    if (*name == 0) {
        for (; i < 11; i++)
            if (FAT_name[i] != ' ') return -1;
    }
    return 0;
}

static bool fat_cursor_next(file_t *f) {
    struct fat *fatdev = &f->dev->_device.fat;
    struct fat_file *fatf = &f->_device_file.fat;

    if (fatf->sec + 1 == fatdev->clussecs) {
        uint32_t nclus = get_cluster_entry(f->dev, fatf->clus);
        if (chain_end(f->dev, nclus)) {
            return false;
        } else {
            fatf->clus = nclus;
            fatf->sec = 0;
            fatf->offset += fatdev->secbytes;
            return true;
        }
    } else {
        fatf->sec++;
        fatf->offset += fatdev->secbytes;
        return true;
    }
}

static bool fat_cursor_alloc(file_t *f) {
    struct fat *fatdev = &f->dev->_device.fat;
    struct fat_file *fatf = &f->_device_file.fat;

    if (fatf->sec + 1 == fatdev->clussecs) {
        uint32_t nclus = get_cluster_entry(f->dev, fatf->clus);
        if (chain_end(f->dev, nclus)) {
            // Find a free cluster and add it to the chain
            uint32_t new_clus = alloc_cluster_entry(f->dev);
            if (new_clus == 1) return false;
            set_cluster_entry(f->dev, fatf->clus, new_clus);
            set_cluster_entry(f->dev, new_clus, 0x0ffffff8);
            fatf->clus = new_clus;
            fatf->sec = 0;
            fatf->offset += fatdev->secbytes;
            return true;
        } else {
            fatf->clus = nclus;
            fatf->sec = 0;
            fatf->offset += fatdev->secbytes;
            return true;
        }
    } else {
        fatf->sec++;
        fatf->offset += fatdev->secbytes;
        return true;
    }
}

static bool fat_cursor_jump(file_t *f, off_t offset) {
    // TODO(kilogram): Does not appear to work beyond one block.
    // Seems to repeat the same sector.
    struct fat_file *fatf = &f->_device_file.fat;
    struct fat *fatdev = &f->dev->_device.fat;
    uint32_t clusbytes = fatdev->secbytes * fatdev->clussecs;
    off_t tar_sec_off = ROUND(offset, clusbytes);
    off_t cur_sec_off = ROUND(fatf->offset, clusbytes);

    if (tar_sec_off != cur_sec_off) {
        fatf->clus = fatf->first_cluster;
        fatf->offset = 0;
        while (ROUND(fatf->offset, clusbytes) < tar_sec_off)
            if(!fat_cursor_next(f)) return false;
    }

    fatf->sec = ROUND(offset, fatdev->clussecs * fatdev->secbytes) %
        fatdev->clussecs;
    fatf->offset = offset;
    return true;
}

static uint32_t fat_cursor_get(file_t *f) {
    struct fat_file *fatf = &f->_device_file.fat;
    return fatf->offset;
}

static bool fat_dir_entry_next(file_t *d, uint8_t buf[32]) {
    struct device *dev = d->dev;
    struct fat *fatdev = &d->dev->_device.fat;
    struct fat_file *fatf = &d->_device_file.fat;

    fat_cursor_jump(d, fatf->offset);

    do {
        dev->read_sector(sector_in_cluster(d->dev, fatf->clus, fatf->sec), dev->buffer);

        // Search the current sector
        for (uint32_t de = fatf->offset % fatdev->secbytes;
                de < dev->bytes_per_block; de += 32) {
            fatf->offset += 32;
            if (DIR_Attr(dev->buffer + de) & DIR_ATTR_IGNORE) continue;
            if (*(char *)(dev->buffer + de) == 0xe5) continue;
            if (*(char *)(dev->buffer + de) == 0x00) break;
            memcpy(buf, dev->buffer + de, 32);
            return true;
        }

    } while (fat_cursor_next(d));

    fat_cursor_jump(d, 0);
    return false;
}

static bool fat_dir_entry_new(file_t *d, uint8_t buf[32]) {
    struct device *dev = d->dev;
    struct fat *fatdev = &d->dev->_device.fat;
    struct fat_file *fatf = &d->_device_file.fat;

    fat_cursor_jump(d, 0);

    do {
        block_t sec = sector_in_cluster(d->dev, fatf->clus, fatf->sec);
        dev->read_sector(sec, dev->buffer);

        // Search the current sector
        uint32_t de;
        for (de = fatf->offset % fatdev->secbytes;
                de < dev->bytes_per_block; de += 32) {
            fatf->offset += 32;
            if (DIR_Attr(dev->buffer + de) & DIR_ATTR_IGNORE) continue;
            if (*(char *)(dev->buffer + de) == 0xe5) break;
            if (*(char *)(dev->buffer + de) == 0x00) break;
        }

        if (de != 512) {
            bool clear_next = *(char *)(dev->buffer + de) == 0x00;
            memcpy(dev->buffer + de, buf, 32);
            dev->write_sector(sec, dev->buffer);
            if (de == 31 && fat_cursor_next(d)) {
                // If we're at the end of a sector and there are sectors left
                block_t sec = sector_in_cluster(d->dev, fatf->clus, fatf->sec);
                dev->read_sector(sec, dev->buffer);
                memset(dev->buffer + de, 0, 32);
                dev->write_sector(sec, dev->buffer);
            }
            return true;
        }

    } while (fat_cursor_next(d));

    return false;
}

static void fat_dir_entry_get(file_t *d, off_t entry, uint8_t buf[32]) {
    struct device *dev = d->dev;
    struct fat *fatdev = &d->dev->_device.fat;
    struct fat_file *fatf = &d->_device_file.fat;

    off_t offset = entry - 32;
    fat_cursor_jump(d, offset);

    uint32_t de = offset % fatdev->secbytes;
    dev->read_sector(sector_in_cluster(
                d->dev, fatf->clus, fatf->sec), dev->buffer);
    memcpy(buf, dev->buffer + de, 32);

    return;
}

static void fat_dir_entry_set(file_t *d, off_t entry, uint8_t buf[32]) {
    struct device *dev = d->dev;
    struct fat *fatdev = &d->dev->_device.fat;
    struct fat_file *fatf = &d->_device_file.fat;

    off_t offset = entry - 32;
    fat_cursor_jump(d, offset);

    uint32_t de = (entry - 32) % fatdev->secbytes;
    dev->read_sector(sector_in_cluster(
                d->dev, fatf->clus, fatf->sec), dev->buffer);
    memcpy(dev->buffer + de, buf, 32);
    dev->write_sector(sector_in_cluster(
                d->dev, fatf->clus, fatf->sec), dev->buffer);

    return;
}

bool fat_mount(struct device* dev) {
    uint32_t fatsz, sec_cnt, datasec_cnt, clusters;

    dev->read_sector(dev->offset, dev->buffer);

    // Populate basic information
    dev->_device.fat.secbytes = BPB_BytsPerSec(dev->buffer);
    dev->_device.fat.clussecs = BPB_SecPerClus(dev->buffer);
    dev->_device.fat.rsvdsec_cnt = BPB_RsvdSecCnt(dev->buffer);

    if (!(  BPB_SigChk(dev->buffer) &&
            BPB_TotSecChk(dev->buffer) &&
            BPB_BytsClusChk(dev->buffer)
        ))
        return false;

    // Basic information
    dev->_device.fat.rootsec_cnt = ((BPB_RootEncCnt(dev->buffer) * 32) +
            (dev->_device.fat.secbytes - 1)) / dev->_device.fat.secbytes;

    fatsz = BPB_FATSz16(dev->buffer);
    if (fatsz == 0) fatsz = BPB_FATSz32(dev->buffer);

    // Populate counts
    sec_cnt = BPB_TotSec16(dev->buffer);
    if (sec_cnt == 0) sec_cnt = BPB_TotSec32(dev->buffer);

    // Get first sector (used in sector calculations)
    dev->_device.fat.first_sec = dev->_device.fat.rsvdsec_cnt +
        (BPB_NumFATs(dev->buffer) * fatsz) +
        dev->_device.fat.rootsec_cnt + dev->offset;

    datasec_cnt = sec_cnt - dev->_device.fat.first_sec;

    // Determine FAT type
    clusters = datasec_cnt / dev->_device.fat.clussecs;

    if (clusters < 4085)
        dev->fs = FS_FAT12;
    else if (clusters < 65525)
        dev->fs = FS_FAT16;
    else
        dev->fs = FS_FAT32;

    if (dev->fs == FS_FAT12 || dev->fs == FS_FAT16) {
        dev->_device.fat.root_sec = dev->_device.fat.rsvdsec_cnt +
            (BPB_NumFATs(dev->buffer) * fatsz);
        // Use root_sectors for size
    } else {
        dev->_device.fat.root_sec =
            sector_in_cluster(dev, BPB_RootClus(dev->buffer), 0);
        // Is a cluster chain
    }

    //dev->read_sector(dev->_device.fat.root_sec + dev->offset, dev->buffer);
    return true;
}

static void fat_file_init(file_t *f, device_t *dev, uint32_t start_clus,
        enum file_type_t type, size_t file_size) {

    struct fat_file *fatf = &f->_device_file.fat;
    memset(f, 0, sizeof(file_t));
    f->dev = dev;
    f->type = type;
    f->size = file_size;

    fatf->first_cluster = start_clus;
    fatf->clus = start_clus;
    fatf->sec = 0;
    fatf->offset = 0;
}

bool fat_file_open(device_t* dev, const char *name, file_t *d, file_t *f) {
    bool ret = false;

    struct fat *fatdev = &dev->_device.fat;
    struct fat_file *fatf = &f->_device_file.fat;

    if (name == NULL && d == NULL) {
        // Open the root directory
        // TODO(kilogram): get the first cluster of the root sector
        block_t root_clus =
            ((fatdev->root_sec - fatdev->first_sec) / fatdev->clussecs) + 2;
        fat_file_init(f, dev, root_clus, FILE_TYPE_DIR,
                fatdev->rootsec_cnt * fatdev->secbytes);

        if (f->size == 0) {
            uint32_t clus = fatf->first_cluster;
            while (!chain_end(dev, clus)) {
                clus = get_cluster_entry(dev, clus);
                f->size += dev->_device.fat.secbytes *
                    dev->_device.fat.clussecs;
            }
        }
        return true;
    }

    assert(name != NULL);
    assert(d != NULL);
    assert(f != NULL);

    uint8_t buf[32];

    uint32_t old_dir_entry = fat_cursor_get(d);
    fat_cursor_jump(d, 0);
    while(fat_dir_entry_next(d, buf)) {
        if (namecmp(name, DIR_Name(buf)) == 0) {
            fat_file_init(
                    f, dev, DIR_FClusHI(buf) << 16 | DIR_FClusLO(buf),
                    ((DIR_Attr(buf) & DIR_ATTR_DIR) ?
                     FILE_TYPE_DIR : FILE_TYPE_FILE),
                    DIR_FileSize(buf));
            fatf->dir_entry = fat_cursor_get(d);
            ret = true;
            break;
        }
    }

    fat_cursor_jump(d, old_dir_entry);
    return ret;
}

bool fat_file_create(file_t *d, const char *name, bool dir) {
    uint8_t buf[32];
    uint32_t old_dir_entry = fat_cursor_get(d);

    fat_cursor_jump(d, 0);
    while(fat_dir_entry_next(d, buf))
        if (namecmp(name, DIR_Name(buf)) == 0)
            return false;

    // TODO(kilogram): allocate the cluster lazily
    uint32_t new_clus = alloc_cluster_entry(d->dev);
    if (new_clus == -1) return false;
    set_cluster_entry(d->dev, new_clus, 0x0ffffff8);

    // Prepare and write the directory entry
    memset(buf, 0, 32);
    name_to_FAT(name, buf);
    if (dir) DIR_Attr(buf) |= DIR_ATTR_DIR;
    DIR_FClusHI(buf) = (new_clus & 0xffff0000) >> 16;
    DIR_FClusLO(buf) = new_clus & 0x0000ffff;
    DIR_FileSize(buf) = 0;
    if (!fat_dir_entry_new(d, buf)) {
        set_cluster_entry(d->dev, new_clus, 0);
        return false;
    }
    return true;
}

bool fat_file_remove(file_t *d, const char *name) {
    // TODO(kilogram): check and fail for directories
    file_t f;
    struct fat_file *fatf = &f._device_file.fat;
    uint8_t buf[32];
    uint32_t old_dir_entry = fat_cursor_get(d);

    fat_cursor_jump(d, 0);
    if (!fat_file_open(d->dev, name, d, &f)) return false;

    uint32_t cur_clus = fatf->first_cluster;
    while (!chain_end(d->dev, cur_clus)) {
        uint32_t next_clus = get_cluster_entry(d->dev, cur_clus);
        set_cluster_entry(d->dev, cur_clus, 0x0);
        cur_clus = next_clus;
    }

    memset(buf, 0, 32);
    buf[0] = 0xe5;
    fat_dir_entry_set(d, fatf->dir_entry, buf);

    fat_cursor_jump(d, old_dir_entry);
    return true;
}

off_t fat_file_read(file_t *f, uint8_t *buf, off_t offset, off_t len) {
    off_t count = 0;
    struct fat *fatdev = &f->dev->_device.fat;
    struct fat_file *fatf = &f->_device_file.fat;

    if (offset > f->size || len == 0 || !fat_cursor_jump(f, offset))
        return 0;

    do {
        off_t start = (offset - count) % fatdev->secbytes,
              req_left = len - count,
              len_left = f->size - count - offset,
              off_left = fatdev->secbytes - start,
              size = MIN(req_left, MIN(len_left, off_left));

        block_t sec = sector_in_cluster(f->dev, fatf->clus, fatf->sec);

        f->dev->read_sector(sec, f->dev->buffer);
        memcpy(buf + count, f->dev->buffer + start, size);
        count += size;
    } while (count < len && offset + count < f->size && fat_cursor_next(f));

    return count;
}

off_t fat_file_write(file_t *f, uint8_t *buf, off_t offset, off_t len) {
    off_t count = 0;
    struct fat *fatdev = &f->dev->_device.fat;
    struct fat_file *fatf = &f->_device_file.fat;

    if (len == 0)
        return 0;

    if (!fat_cursor_jump(f, offset)) {
        // TODO(kilogram): allocate empty clusters
        return 0;
    }

    do {
        off_t start = (offset - count) % fatdev->secbytes,
              req_left = len - count,
              off_left = fatdev->secbytes - start,
              size = MIN(req_left, off_left);

        block_t sec = sector_in_cluster(f->dev, fatf->clus, fatf->sec);

        f->dev->read_sector(sec, f->dev->buffer);
        memcpy(f->dev->buffer + start, buf + count, size);
        f->dev->write_sector(sec, f->dev->buffer);
        count += size;
        if (!fat_cursor_next(f)) {
            fat_cursor_alloc(f);
        }
    } while (count < len);

    f->size = MAX(f->size, count + offset);
    // Write the new filesize to the directory entry
    uint32_t old_entry = fat_cursor_get(f->parent);

    uint8_t entry[32];
    fat_dir_entry_get(f->parent, fatf->dir_entry, entry);
    DIR_FileSize(entry) = f->size;
    fat_dir_entry_set(f->parent, fatf->dir_entry, entry);

    fat_cursor_jump(f->parent, old_entry);
    return count;
}

bool fat_dir_next(file_t *d, char *buf) {
    uint8_t entry[32];
    if (!fat_dir_entry_next(d, entry))
        return false;
    name_from_FAT(entry, buf);
    return true;
}

