/* filename ******** fat.h **************
 *
 * FAT filesystem driver
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "lib/types.h"
#include "io/fs.h"

bool fat_mount(device_t *dev);
bool fat_file_open(device_t *dev, const char *name, file_t *d, file_t *f);
bool fat_file_create(file_t *d, const char *name, bool dir);
bool fat_file_remove(file_t *d, const char *name);
off_t fat_file_read(file_t *f, uint8_t *buf, off_t offset, off_t len);
off_t fat_file_write(file_t *f, uint8_t *buf, off_t offset, off_t len);
bool fat_dir_next(file_t *d, char *buf);
