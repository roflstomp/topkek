/* filename ******** device.h **************
 *
 * Abstraction for devices with different drivers.
 * Lies between filesystem driver code and io code.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#ifndef __DEVICE_H
#define __DEVICE_H

#include "lib/types.h"
#include "lib/common.h"

// Individual filesystem info
struct fat {
    uint8_t clussecs;
    uint16_t secbytes;
    uint16_t rootsec_cnt;
    uint16_t rsvdsec_cnt;
    block_t first_sec;
    block_t root_sec;
};

struct fat_file {
    uint32_t dir_entry;
    off_t offset;
    block_t first_cluster;
    block_t clus;
    uint8_t sec;
};

// collective filesystems
#define MAX_BYTES_PER_BLOCK 512
#define MAX_FILENAME_LENGTH 127

typedef enum {
    FS_NONE,
    FS_FAT12,
    FS_FAT16,
    FS_FAT32,
} fs_t;

enum file_type_t {
    FILE_TYPE_NONE,
    FILE_TYPE_FILE,
    FILE_TYPE_DIR,
};

// file struct, size is the only consistent field
struct file {
    enum file_type_t type;
    size_t size;
    struct file *parent;

    device_t *dev;

    union {
        struct fat_file fat;
    } _device_file;
};

// device struct, of which the first three fields should
// be supplied by the individual device drivers
struct device {
    bool (*read_sector)(block_t lba, uint8_t *buf);
    bool (*write_sector)(block_t lba, uint8_t *buf);
    size_t bytes_per_block;

    fs_t fs;

    block_t offset;
    size_t size;

    union {
        struct fat fat;
    } _device;

    uint8_t buffer[MAX_BYTES_PER_BLOCK];
};

#endif

