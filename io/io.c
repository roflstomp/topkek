/* filename ******** io.c **************
 *
 * Abstraction for most I/O functionality
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "io.h"
#include "fs.h"

#include <inc/hw_types.h>
#include <inc/hw_gpio.h>
#include <inc/hw_sysctl.h>
#include <inc/hw_memmap.h>
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>

#include <periph/tft.h>
#include "tftio.h"


#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))

#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000


void io_init(void) {
}

int _write(int file, char *ptr, int len) {
    int n;
    switch (file) {
        case STDERR_FD:
#ifdef DEBUG
            for (n = 0; n < len; n++) {
                if (DEMCR & TRCENA) {
                    while (ITM_Port32(0) == 0);
                    ITM_Port8(0) = ptr[n];
                }
            }
#endif

            return UARTwrite((const char*)ptr, len);

        case STDOUT_FD:
            for (n = 0; n < len; n++) {
                tft_putc(ptr[n]);
            }
            return n;

        default:
            return 0;
    }
}

int _read(int file, char *ptr, int len) {
    int n;
    switch (file) {
        case STDIN_FD:
            for (n = 0; n < len; n++) {
                ptr[n] = UARTgetc();
                if (ptr[n] == 0)
                    break;
                if (ptr[n] == '\r') {
                    return n - 1;
                    break;
                }
            }
            return n;

        default:
            return EOF;
    }
}

int _close(int file) {
    return -1;
}

int _fstat(int file, struct stat *st) {
    return -1;
}

int _isatty(int file) {
    switch (file) {
        case STDERR_FD:
            return true;

        default:
            return false;
    }
}

int _lseek(int file, int ptr, int dir) {
    return -1;
}

#define RSVD_ID 4

uint64_t idmap = 0x000000000000001F;

uint8_t next_id(void) {
    uint64_t mask = 1;
    uint8_t ret = 0;
    while(mask < 64 && (idmap & mask)) {
        mask <<= 1;
        ret += 1;
    }
    if (mask == 64) return (uint8_t) -1;
    idmap &= ~mask;
    return ret;
}

void free_id(uint8_t id) {
    uint64_t mask = 1 << id;
    if (id >= 64 || id <= RSVD_ID) return;
    idmap |= mask;
}
