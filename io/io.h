/* filename ******** io.h **************
 *
 * Abstraction for most I/O functionality
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#ifndef __OUTPUT_H
#define OUTPUT_H

#include "lib/types.h"

#define EOF (-1)

#define STDIN_FD    0
#define STDOUT_FD   1
#define STDERR_FD   2

void io_init(void);

uint8_t next_id(void);
void free_id(uint8_t id);

#endif
