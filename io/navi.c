/* filename ******** navi.h **************
 *
 * File navigator and FS interface
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "io/navi.h"
#include "io/fat.h"


#define WORD(x, o) ((((uint32_t)(x)[o+3]) << 24) | \
                    (((uint32_t)(x)[o+2]) << 16) | \
                    (((uint32_t)(x)[o+1]) <<  8) | \
                    (((uint32_t)(x)[o+0]) <<  0))

// Macros for partition lookup
#define PART_TABLE(x) ((x) + 0x1be)

#define PART_TYPE(x)   (PART_TABLE(x)[4])
#define PART_OFFSET(x) WORD(PART_TABLE(x),  8)
#define PART_SIZE(x)   WORD(PART_TABLE(x), 12)


// Internal lookup table for filesystems
struct fs {
    bool (*mount)(device_t *dev);
    bool (*open)(device_t *dev, const char *name, file_t *d, file_t *f);
    bool (*create)(file_t *d, const char *name, bool dir);
    bool (*remove)(file_t *d, const char *name);
    off_t (*read)(file_t *f, uint8_t *buf, off_t offset, off_t len);
    off_t (*write)(file_t *f, uint8_t *buf, off_t offset, off_t len);
    bool (*next_entry)(file_t *f, char *buf);
};

const struct fs systems[] = {
/* FS_NONE  */  { 0, 0, 0, 0, 0, 0 },
/* FS_FAT12 */  { fat_mount, fat_file_open, fat_file_create, fat_file_remove,
                    fat_file_read, fat_file_write, fat_dir_next },
/* FS_FAT16 */  { fat_mount, fat_file_open, fat_file_create, fat_file_remove,
                    fat_file_read, fat_file_write, fat_dir_next },
/* FS_FAT32 */  { fat_mount, fat_file_open, fat_file_create, fat_file_remove,
                fat_file_read, fat_file_write, fat_dir_next },
};


bool navi_mount(device_t *dev) {
    dev->read_sector(0, dev->buffer);

    dev->offset = PART_OFFSET(dev->buffer);
    dev->size = PART_SIZE(dev->buffer);

    // TODO(geky): iterate over partition entries,
    // although it should always be the first one
    switch (PART_TYPE(dev->buffer)) {
        case 1:
            dev->fs = FS_FAT12;
            break;

        case 0x4: case 0x6: case 0xe:
            dev->fs = FS_FAT16;
            break;

        case 0xb: case 0xc:
            dev->fs = FS_FAT32;
            break;

        default:
            dev->fs = FS_NONE;
            return false;
    }

    return systems[dev->fs].mount(dev);
}

bool navi_load_menu(device_t *dev, const char *name, uint8_t *buf, size_t *len);
bool navi_load(device_t *dev, const char *name, uint8_t *buf, size_t *len);

bool navi_file_menu(device_t *dev, file_t *f);

bool navi_file_open(device_t *dev, const char *name, file_t *d, file_t *f) {
    bool ret = systems[dev->fs].open(dev, name, d, f);
    f->parent = (d) ? d : f;
    return ret;
}

bool navi_file_create(file_t *d, const char *name, bool dir) {
    return systems[d->dev->fs].create(d, name, dir);
}

bool navi_file_remove(file_t *d, const char *name) {
    return systems[d->dev->fs].remove(d, name);
}

off_t navi_file_read(file_t *f, uint8_t *buf, off_t offset, off_t len) {
    return systems[f->dev->fs].read(f, buf, offset, len);
}

off_t navi_file_write(file_t *f, uint8_t *buf, off_t offset, off_t len) {
    return systems[f->dev->fs].write(f, buf, offset, len);
}

bool navi_next_entry(file_t *f, char *name) {
    return systems[f->dev->fs].next_entry(f, name);
}
