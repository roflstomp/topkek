/* filename ******** navi.h **************
 *
 * File navigator and FS interface
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "lib/types.h"
#include "io/fs.h"

bool navi_mount(device_t *dev);

bool navi_load_menu(device_t *dev, const char *name, uint8_t *buf, size_t *len);
bool navi_load(device_t *dev, const char *name, uint8_t *buf, size_t *len);

bool navi_file_menu(device_t *dev, file_t *f);
bool navi_file_open(device_t *dev, const char *name, file_t *d, file_t *f);
bool navi_file_create(file_t *d, const char *name, bool dir);
bool navi_file_remove(file_t *d, const char *name);
off_t navi_file_read(file_t *f, uint8_t *buf, off_t offset, off_t len);
off_t navi_file_write(file_t *f, uint8_t *buf, off_t offset, off_t len);
bool navi_next_entry(file_t *f, char *name);
