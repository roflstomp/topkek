/* filename ******** print.c **************
 *
 * Simple Printf
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */

#include <stdbool.h>
#include <stdarg.h>
#include <math.h>


// Character lookup tables for numbers
static const char *upper = "0123456789ABCDEF";
static const char *lower = "0123456789abcdef";

// Printing state
struct print {
    void (*putc)(char);

    const char *ascii;

    int left;
    int sign;
    int width;
    int prec;
    int base;
};


// Helper functions for getting format stuff
static int get_int(const char **pos) {
    int res = 0;
    const char *off = *pos;

    while (*off >= '0' && *off <= '9') {
        res *= 10;
        res += *off - '0';
        off++;
    }

    *pos = off;
    return res;
}

// String printing
static int string_size(int count, const char *pos) {
    for (int i = 0; i < count; i++) {
        if (pos[i] == '\0')
            return i;
    }

    return count;
}

static void put_string(struct print *p, int count, const char *pos) {
    if (count < 0)
        count = 0x7fffffff;

    int len = string_size(count, pos);

    if (len > p->width) {
        for (int i = 0; i < len; i++)
            p->putc(pos[i]);
    } else if (p->left) {
        for (int i = 0; i < len; i++)
            p->putc(pos[i]);

        for (int i = len; i < p->width; i++)
            p->putc(' ');
    } else {
        for (int i = len; i < p->width; i++)
            p->putc(' ');

        for (int i = 0; i < len; i++)
            p->putc(pos[i]);
    }
}

// Integer printing
static int number_size(struct print *p, unsigned int n) {
    int i = 0;

    while (n) {
        i++;
        n /= p->base;
    }

    return i;
}

static void put_number_rec(struct print *p, int count, unsigned int n) {
    if (!n && count <= 0)
        return;

    put_number_rec(p, count-1, n/p->base);
    p->putc(p->ascii[n % p->base]);
}

static void put_number(struct print *p, unsigned int n) {
    if (p->sign == 1) {
        p->putc('+');
        p->width--;
    } else if (p->sign == 2) {
        p->putc(' ');
        p->width--;
    }

    if (p->width < 1)
        p->width = 1;

    if (p->left) {
        put_number_rec(p, 1, n);
        for (int i = number_size(p, n) + (n == 0); i < p->width; i++)
            p->putc(' ');
    } else {
        put_number_rec(p, p->width, n);
    }
}

static void put_signed(struct print *p, signed int d) {
    if (d < 0) {
        p->putc('-');
        p->sign = 0;
        p->width--;
        put_number(p, -d);
    } else {
        put_number(p, d);
    }
}

// Floating point printing
static void put_normal_float(struct print *p, float f) {
    if (p->prec < 0)
        p->prec = 6;

    if (f < 0) {
        p->putc('-');
        p->sign = 0;
        p->width--;
        f = -f;
    }

    if (isnan(f)) {
        put_string(p, 3, "nan");
    } else if (isinf(f)) {
        put_string(p, 3, "inf");
    } else {
        int padding;

        if (p->left) {
            padding = p->width - (number_size(p, (int)f) + 1) - p->prec;
            p->width = 0;
        } else {
            padding = 0;
            p->width = p->width - (p->prec+1);
        }

        put_number(p, (int)f);
        p->putc('.');

        p->sign = 0;
        p->width = p->prec;
        f = f - floorf(f);
        put_number(p, (int)(f*powf(10, p->prec)));

        for (int i = 0; i < padding; i++)
            p->putc(' ');
    }
}

static void put_science_float(struct print *p, float f) {
    if (p->prec < 0)
        p->prec = 6;

    if (f < 0) {
        p->putc('-');
        p->sign = 0;
        p->width--;
        f = -f;
    }

    if (f == 0) {
        put_string(p, 1, "0");
    } else if (isnan(f)) {
        put_string(p, 3, "nan");
    } else if (isinf(f)) {
        put_string(p, 3, "inf");
    } else {
        float exp = floorf(log10f(f));
        int ewidth;

        if (p->left) {
            ewidth = p->width - (number_size(p, (int)f) + 1);
            p->width = 0;
        } else {
            ewidth = 0;
            p->width = p->width - (p->prec + number_size(p, (int)exp) + 2);
        }

        put_number(p, (int)f);
        p->putc('.');

        p->width = p->prec;
        f = f - floorf(f);
        put_number(p, (int)(f*powf(10, p->prec)));

        p->putc(p->ascii[0xe]);

        p->left = true;
        p->sign = 1;
        p->width = ewidth;
        put_number(p, (int)exp);
    }
}


// And finally printf itself
void cvprintf(void (*putc)(char), const char *buffer, va_list args) {
    struct print p = { .putc = putc };

    while (*buffer) {
        // Check for escape characters
next:   if (*buffer == '%') {
            buffer++;

            // Flags
            p.ascii = lower;
            p.base = 10;
            p.left = false;
            p.sign = 0;
            p.width = -1;
            p.prec = -1;

            while (true) {
                switch (*buffer++) {
                    // Flags
                    case '0': break;
                    case '-': p.left = true; break;
                    case '+': p.sign = 1; break;
                    case ' ': p.sign = 2; break;

                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9': buffer--; p.width = get_int(&buffer); break;

                    case '.': p.prec = get_int(&buffer); break;
                    case '*': p.prec = va_arg(args, int); break;

                    // Numbers
                    case 'i':
                    case 'd': put_signed(&p, va_arg(args, signed int)); goto next;

                    case 'X': p.ascii = upper;
                    case 'p':
                    case 'x': p.base = 16; put_number(&p, va_arg(args, unsigned int)); goto next;
                    case 'u': p.base = 10; put_number(&p, va_arg(args, unsigned int)); goto next;
                    case 'o': p.base = 8;  put_number(&p, va_arg(args, unsigned int)); goto next;

                    case 'F': p.ascii = upper;
                    case 'f': put_normal_float(&p, (float)va_arg(args, double)); goto next;

                    case 'E':
                    case 'G': p.ascii = upper;
                    case 'e':
                    case 'g': put_science_float(&p, (float)va_arg(args, double)); goto next;

                    // Strings and characters
                    case 's': put_string(&p, p.width, va_arg(args, char *)); goto next;
                    case 'c': putc((char)va_arg(args, int)); goto next;
                    default:  putc('%'); goto next;
                }
            }
        } else if (*buffer == '\0') {
        } else {
            // Print out the next character
            if (*buffer == '\n')
                putc('\r');

            putc(*buffer++);
        }
    }
}

// Handle varargs
void cprintf(void (*putc)(char), const char *buffer, ...) {
    va_list args;
    va_start(args, buffer);

    cvprintf(putc, buffer, args);

    va_end(args);
}
