/* filename ******** print.h **************
 *
 * Simple Printf
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */

#include <stdarg.h>


// Printf definition
void cvprintf(void (*putc)(char), const char *buffer, va_list args);
void cprintf(void (*putc)(char), const char *buffer, ...);

