/* filename ******** sd.h **************
 *
 * Interface for reading SD cards
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */

#include "sd.h"
#include "io.h"

#include <periph/gpio.h>
#include <inc/hw_memmap.h>
#include <driverlib/gpio.h>
#include <driverlib/ssi.h>
#include <driverlib/sysctl.h>
#include <driverlib/pin_map.h>


// SD Commands
#define CMD_IDLE        0
#define CMD_IF          8
#define CMD_BLOCK_LEN   16
#define CMD_READ        17
#define CMD_WRITE       24
#define CMD_ACMD        55
#define CMD_OCR         58
#define CMD_INIT        (0x80 | 41)
#define CMD_INIT_MMC    1

// Card types
#define CARD_NONE       0
#define CARD_SDv2_HC    1
#define CARD_SDv2_SC    2
#define CARD_SDv1       3
#define CARD_MMC        4


// SD Card device
device_t sd_card = {
    sd_read,
    sd_write,
    512,
};

// state variables
static int card_type = 0;


// Select SD card
static void sd_select(void) {
    unsigned long eh;
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0x00);
    SSIDataPut(SSI2_BASE, 0xff);
    SSIDataGet(SSI2_BASE, &eh);
}

// Deselect SD card
static void sd_deselect(void) {
    unsigned long eh;
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0xff);
    SSIDataPut(SSI2_BASE, 0xff);
    SSIDataGet(SSI2_BASE, &eh);
}


// Send command and argument
// returns R1 response value
static unsigned char sd_cmd(unsigned char cmd, unsigned int arg) {
    unsigned long resp;
    unsigned long eh;
    int i;

    if (cmd & 0xc0) {    // check for long commands
        unsigned char resp;
        cmd &= 0x3f;

        resp = sd_cmd(CMD_ACMD, 0);
        if (resp >> 1) return resp;
    }

    // Send a command
    SSIDataPut(SSI2_BASE, 0x40 | cmd);
    SSIDataGet(SSI2_BASE, &eh);
    SSIDataPut(SSI2_BASE, (unsigned char)(arg >> 24));
    SSIDataGet(SSI2_BASE, &eh);
    SSIDataPut(SSI2_BASE, (unsigned char)(arg >> 16));
    SSIDataGet(SSI2_BASE, &eh);
    SSIDataPut(SSI2_BASE, (unsigned char)(arg >>  8));
    SSIDataGet(SSI2_BASE, &eh);
    SSIDataPut(SSI2_BASE, (unsigned char)(arg >>  0));
    SSIDataGet(SSI2_BASE, &eh);

    if (cmd == CMD_IDLE) {
        SSIDataPut(SSI2_BASE, 0x95);
        SSIDataGet(SSI2_BASE, &eh);
    } else if (cmd == CMD_IF) {
        SSIDataPut(SSI2_BASE, 0x87);
        SSIDataGet(SSI2_BASE, &eh);
    } else {
        SSIDataPut(SSI2_BASE, 0x01);
        SSIDataGet(SSI2_BASE, &eh);
    }

    // Wait for a response up to 20 times
    for (i = 0; i < 20; i++) {
        SSIDataPut(SSI2_BASE, 0xff);
        SSIDataGet(SSI2_BASE, &resp);

        if (!(resp & 0x80))
            return (unsigned char)resp;
    }

    return (unsigned char)resp;
}


// Obtains following word in R7
static unsigned long sd_resp(void) {
    unsigned long resp;
    unsigned long data = 0;

    SSIDataPut(SSI2_BASE, 0xff); SSIDataGet(SSI2_BASE, &resp); data |= resp << 24;
    SSIDataPut(SSI2_BASE, 0xff); SSIDataGet(SSI2_BASE, &resp); data |= resp << 16;
    SSIDataPut(SSI2_BASE, 0xff); SSIDataGet(SSI2_BASE, &resp); data |= resp <<  8;
    SSIDataPut(SSI2_BASE, 0xff); SSIDataGet(SSI2_BASE, &resp); data |= resp <<  0;

    return data;
}


// Poll data to and from spi
static unsigned char sd_poll(unsigned char tx) {
    unsigned long rx;

    SSIDataPut(SSI2_BASE, tx);
    SSIDataGet(SSI2_BASE, &rx);

    return (unsigned char)rx;
}

//*************sd_get_freq**************************************
// Obtains the SD frequency
// Inputs: none
// Outputs: frequency in Hz
unsigned long sd_get_freq(void) {
    if (card_type == CARD_MMC)
        return SD_MMC_FREQ;
    else
        return SD_FREQ;
}

//*************sd_init**************************************
// Initializes the SD card reader
// Inputs: none
// Outputs: true if card is present
bool sd_init(void) {
    uint32_t resp;
    int i;

    // Enable the SPI hardware
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

    // configure pin muxing
    GPIOPinConfigure(GPIO_PB4_SSI2CLK);
    GPIOPinConfigure(GPIO_PB6_SSI2RX);
    GPIOPinConfigure(GPIO_PB7_SSI2TX);

    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5);
    GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_6 | GPIO_PIN_7);

    SSIDisable(SSI2_BASE);
    SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(),
                       SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, SD_MMC_FREQ, 8);
    SSIEnable(SSI2_BASE);


    // SD Initialization
    do {
        SSIDisable(SSI2_BASE);
        SSIEnable(SSI2_BASE);

        sd_deselect();
        for (i = 0; i < 20; i++) {
            sd_poll(0xff);
        }

        sd_select();

        resp = sd_cmd(CMD_IDLE, 0);

        // No card inserted
        if (resp == 0xff) {
            sd_deselect();
            return (card_type = 0);
        }
    } while (resp != 1);


    // Figure out the card type
    if (sd_cmd(CMD_IF, 0x1AA) == 1) { // SDv2
        sd_resp();

        while (sd_cmd(CMD_INIT, 1 << 30) == 1)
            ;

        if (sd_cmd(CMD_OCR, 0) != 0)
            return (card_type = 0);

        resp = sd_resp();

        if (resp & (1 << 30)) // SDv2 SC
            card_type = CARD_SDv2_HC;
        else
            card_type = CARD_SDv2_SC;

    } else if (sd_cmd(CMD_INIT, 0) == 0) { // SDv1
        while (sd_cmd(CMD_INIT, 1 << 30) == 1)
            ;

        card_type = CARD_SDv1;
    } else { // MMC
        while (sd_cmd(CMD_INIT_MMC, 1 << 30) == 1)
            ;

        card_type = CARD_MMC;
    }


    // Set block size to 512
    sd_cmd(CMD_BLOCK_LEN, 512);

    // Speed up clock
    SSIDisable(SSI2_BASE);
    SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(),
                       SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, sd_get_freq(), 8);
    SSIEnable(SSI2_BASE);


    sd_deselect();
    return card_type;
}

//*************sd_read**************************************
// Read a block
// Inputs: Block number to read
//         Destion of data
// Outputs: none
bool sd_read(block_t lba, uint8_t *dest) {
    int i;

    if (card_type == CARD_NONE)
        return false;

    if (card_type != CARD_SDv2_HC)
        lba *= 512;


    sd_select();

    if (sd_cmd(CMD_READ, lba) != 0)
        return false;

    while (sd_poll(0xff) != 0xfe)
        ;

    for (i = 0; i < 512; i++) {
        dest[i] = sd_poll(0xff);
    }

    sd_poll(0xff);
    sd_poll(0xff);
    sd_deselect();

    return true;
}

//*************sd_write**************************************
// Read a block
// Inputs: Block number to write
//         Source of data
// Outputs: none
bool sd_write(block_t lba, uint8_t *src) {
    int i;

    if (card_type == CARD_NONE)
        return false;

    if (card_type != CARD_SDv2_HC)
        lba *= 512;


    sd_select();

    if (sd_cmd(CMD_WRITE, lba) != 0)
        return false;

    sd_poll(0xff);
    sd_poll(0xfe);

    for (i = 0; i < 512; i++) {
        sd_poll(src[i]);
    }

    sd_poll(0xff);
    sd_poll(0xff);

    while (sd_poll(0xff) != 0xff)
        ;

    sd_deselect();

    return true;
}
