/* filename ******** sd.h **************
 *
 * Interface for reading SD cards
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
 */


#include "io/fs.h"

// SD Definitions
#define SD_MMC_FREQ 250000
#define SD_FREQ 4000000

// Device struct
extern device_t sd_card;


//*************sd_init**************************************
// Initializes the SD card reader
// Inputs: none
// Outputs: true if card is present
bool sd_init(void);

//*************sd_get_freq**************************************
// Obtains the SD frequency
// Inputs: none
// Outputs: frequency in Hz
unsigned long sd_get_freq(void);

//*************sd_read**************************************
// Read a block
// Inputs: Block number to read
//         Destion of data
// Outputs: none
bool sd_read(block_t lba, uint8_t *dest);

//*************sd_write**************************************
// Read a block
// Inputs: Block number to write
//         Source of data
// Outputs: none
bool sd_write(block_t lba, uint8_t *src);
