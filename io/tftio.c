/* filename ******** tft.c **************
 *
 * Driver for the TFT_320QVT
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "io/tftio.h"
#include "periph/tft.h"
#include <gui/font.h>
#include <io/print.h>
#include <os/sync.h>

static uint16_t x = 0;
static uint16_t y = 0;

static struct sema tftio_lock;

void tftio_init(void) {
    tft_init();
    sema_init(&tftio_lock, 1);
}

static void tft_putc(char ch) {
    int xi;
    int yi;

    if (ch >= ' ' && ch <= '~') {
        tft_set_rect(x*(FONT_WIDTH+1), y*(FONT_HEIGHT+1),
                FONT_WIDTH, FONT_HEIGHT);

        for (xi=0; xi < FONT_WIDTH; xi++) {
            unsigned char cdata = g_pucFont[ch-' '][xi];

            for (yi=0; yi < FONT_HEIGHT; yi++) {
                tft_set_color(1 & (cdata >> yi) ? 0xffff : 0x0000);
            }
        }

        tft_clear_rect();

        x += 1;

    } else if (ch == '\n') {
        x = 0;
        y += 1;

        if (y > 28)
            y = 0;

    } else if (ch == '\r') {
        x = 0;
    }
}

void tft_printf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    sema_down(&tftio_lock);
    cvprintf(tft_putc, fmt, args);
    sema_up(&tftio_lock);

    va_end(args);
}

void tft_printf_at(uint16_t nx, uint16_t ny, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    sema_down(&tftio_lock);
    x = nx;
    y = ny;
    cvprintf(tft_putc, fmt, args);
    sema_up(&tftio_lock);

    va_end(args);
}

