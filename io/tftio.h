/* filename ******** tft.c **************
 *
 * Driver for the TFT_320QVT
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include <lib/types.h>

// TODO move this to tft?
void tftio_init(void);

void tft_printf(const char *fmt, ...);

void tft_printf_at(uint16_t x, uint16_t y, const char *fmt, ...);
