/* filename ******** common.c **************
 *
 * Functions and macros to replace stdlib
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "common.h"

#include <inc/hw_memmap.h>
#include <driverlib/sysctl.h>
#include <driverlib/adc.h>
#include <driverlib/gpio.h>


#ifdef DEBUG
static bool interrupts_enabled = false;
#endif


// Dummy for callbacks
void dummy(void) {}


void DisableInterrupts(void) {
    asm("CPSID I");
#ifdef DEBUG
    interrupts_enabled = false;
#endif
}

void EnableInterrupts(void) {
#ifdef DEBUG
    interrupts_enabled = true;
#endif
    asm("CPSIE I");
}

uint32_t StartCritical(void) {
    uint32_t old;
    asm("MRS %0, PRIMASK\t\n\t\
        CPSID I"
        : "=r" (old));
#ifdef DEBUG
    interrupts_enabled = false;
#endif
    return old;
}

void EndCritical(uint32_t old) {
#ifdef DEBUG
    interrupts_enabled = !old;
#endif
    asm("MSR PRIMASK, %0\n"
        : "=r" (old));
}

asm_fn uint_t get_psr(void) {
    asm(
    "   mrs r0, psr         \n"
    "   bx lr               \n"
    );
}

int_t GetPeripheral(uint_t port, uint_t pin) {
    switch (port) {
        case GPIO_PORTA_BASE: return SYSCTL_PERIPH_GPIOA;
        case GPIO_PORTB_BASE: return SYSCTL_PERIPH_GPIOB;
        case GPIO_PORTC_BASE: return SYSCTL_PERIPH_GPIOC;
        case GPIO_PORTD_BASE: return SYSCTL_PERIPH_GPIOD;
        case GPIO_PORTE_BASE: return SYSCTL_PERIPH_GPIOE;
        case GPIO_PORTF_BASE: return SYSCTL_PERIPH_GPIOF;
        default: return -1;
    }
}

int_t GetADCChannel(uint_t port, uint_t pin) {
    switch (port + pin) {
        case GPIO_PORTB_BASE + GPIO_PIN_4: return ADC_CTL_CH10;
        case GPIO_PORTB_BASE + GPIO_PIN_5: return ADC_CTL_CH11;
        case GPIO_PORTD_BASE + GPIO_PIN_0: return ADC_CTL_CH7;
        case GPIO_PORTD_BASE + GPIO_PIN_1: return ADC_CTL_CH6;
        case GPIO_PORTD_BASE + GPIO_PIN_2: return ADC_CTL_CH5;
        case GPIO_PORTD_BASE + GPIO_PIN_3: return ADC_CTL_CH4;
        case GPIO_PORTE_BASE + GPIO_PIN_0: return ADC_CTL_CH3;
        case GPIO_PORTE_BASE + GPIO_PIN_1: return ADC_CTL_CH2;
        case GPIO_PORTE_BASE + GPIO_PIN_2: return ADC_CTL_CH1;
        case GPIO_PORTE_BASE + GPIO_PIN_3: return ADC_CTL_CH0;
        case GPIO_PORTE_BASE + GPIO_PIN_4: return ADC_CTL_CH9;
        case GPIO_PORTE_BASE + GPIO_PIN_5: return ADC_CTL_CH8;
        default: return -1;
    }
}

// Needed for assertions
void __error__(const char *message) {
    debug_bkpt();
    while (1);
}

