/* filename ******** common.h **************
 *
 * Functions and macros to replace stdlib
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#ifndef __COMMON_H
#define __COMMON_H

#include <stddef.h>
#include "types.h"

#define inline __inline
#define asm __asm volatile
#define noreturn __attribute__((noreturn)) void

#ifndef NULL
#define NULL ((void*)0)
#endif

#define __P_DEBUG(x) #x
#define _P_DEBUG(x) __P_DEBUG(x)
#define P_DEBUG "" __FILE__ ":" _P_DEBUG(__LINE__) ": "

#ifndef HW
#define __HW(port, pin) HWREG(port + (GPIO_O_DATA+(pin<<2)))
#define HW(pin) __HW(pin)
#define __HWOUT(port, pin) (HWREG(port + GPIO_O_DIR) |= pin)
#define HWOUT(pin) __HWOUT(pin)
#define __HWIN(port, pin) (HWREG(port + GPIO_O_DIR) &= ~pin)
#define HWIN(pin) __HWIN(pin)

#define __HWCLR(port, pin) (HWREG(port + GPIO_O_ICR) = pin)
#define HWCLR(pin) __HWCLR(pin)
#define __HWEN(port, pin) (HWREG(port + GPIO_O_IM) |= pin)
#define HWEN(pin) __HWEN(pin)
#define __HWDS(port, pin) (HWREG(port + GPIO_O_IM) &= ~pin)
#define HWDS(pin) __HWDS(pin)
#endif


void DisableInterrupts(void);
void EnableInterrupts(void);
uint32_t StartCritical(void);
void EndCritical(uint32_t);

static inline int abs(int x) {
    return (x > 0) ? x : -x;
}

// Dummy for callbacks
void dummy(void);

// Debugging breakpoints
#ifdef DEBUG
#define debug_bkpt() asm("bkpt #0\n")
#else
#define debug_bkpt()
#endif

// Infinite loop in or out of debug loop
#define debug_halt() ({ while (1) debug_bkpt(); })

// Assertions based on debug breakpoints
#ifdef DEBUG
#define assert(test) ({ \
    if (!(test))        \
        debug_bkpt();   \
})
#else
#define assert(test)
#endif

#define assert_ram(x) \
    assert((uint_t)(x) >= 0x20000000 &&   \
        (uint_t)(x) <  0x20008000 )

#define asm_fn __attribute__((naked))

#ifdef DEBUG
#define unreachable() debug_halt()
#else
#define unreachable() __builtin_unreachable()
#endif

#define npw2(i) (1 << (32 - __builtin_clz(i-1)))


uint_t get_psr(void);

int_t GetPeripheral(uint_t port, uint_t pin);
int_t GetADCChannel(uint_t port, uint_t pin);


#endif
