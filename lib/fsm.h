
#ifdef FSM_SIZE
#ifdef FSM_OUT_T

#ifndef __FSM_H
#define __FSM_H

//#include <assert.h>

struct state {
    FSM_OUT_T output;
    struct state* next[FSM_SIZE];
};

FSM_OUT_T *fsm_step(struct state** f, int in) {
//    assert(in < FSM_SIZE);
    *f = (*f)->next[in];
    return &(*f)->output;
}


#endif
#endif
#endif

