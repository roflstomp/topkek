/* filename ******** llist.c **************
 *
 * Provides linked list data structure embedded in structs.
 * Look to llist_test for usage.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
*/

#include "llist.h"

static bool llist_head (struct _llist *list)
{ return (list != NULL) && (list->prev == NULL) && (list->next != NULL); }

static inline bool llist_tail (struct _llist *list)
{ return (list != NULL) && (list->prev != NULL) && (list->next == NULL); }

static bool llist_inside (struct _llist *list)
{ return (list != NULL) && (list->prev != NULL) && (list->next != NULL); }

bool llist_empty (struct llist *list)
{ return llist_tail (list->head.next); }

void llist_init (struct llist *list)
{
    if (!list) return;
    list->head.prev = NULL;
    list->head.next = &list->tail;
    list->tail.prev = &list->head;
    list->tail.next = NULL;
}

struct _llist* llist_iter_start (struct llist* list)
{ return (list) ? (list->head.next) : NULL; }
struct _llist* llist_iter_next (struct _llist* cur)
{ return (cur) ? (cur->next) : NULL; }
struct _llist* llist_iter_prev (struct _llist* cur)
{ return (cur) ? (cur->prev) : NULL; }
struct _llist* llist_iter_end (struct llist* list)
{ return (list) ? (&list->tail) : NULL; }

struct _llist* llist_riter_start (struct llist* list)
{ return (list) ? (list->tail.prev) : NULL; }
struct _llist* llist_riter_next (struct _llist* cur)
{ return (cur) ? (cur->prev) : NULL; }
struct _llist* llist_riter_prev (struct _llist* cur)
{ return (cur) ? (cur->next): NULL; }
struct _llist* llist_riter_end (struct llist* list)
{ return (list) ? (&list->head) : NULL; }

bool llist_insert (struct _llist *after, struct _llist *ins)
{
    if (!after || !ins ||
            !(llist_inside(after) || llist_head(after)))
        return false;
    ins->next = after->next;
    ins->prev = after;
    // StartCritical
    after->next->prev = ins;
    after->next = ins;
    // EndCritical
    return true;
}

bool llist_insert_ordered(struct llist *llist, struct _llist *ins,
        less_func list_less) {
    struct _llist *cur = &llist->head;
    struct _llist *next = llist_iter_next(cur);
    while (next != llist_iter_end(llist)) {
        if (!list_less(next, ins)) break;
        cur = next;
        next = llist_iter_next(cur);
    }
    return llist_insert(cur, ins);
}

struct _llist* llist_remove (struct _llist* rem)
{
    if (!rem || !llist_inside(rem)) return NULL;
    // StartCritical
    rem->prev->next = rem->next;
    rem->next->prev = rem->prev;
    // EndCritical
    rem->prev = rem->next = NULL;
    return rem;
}

bool llist_append (struct llist *list, struct _llist *ins)
{ return (list && ins) ? (llist_insert (list->tail.prev, ins)) : false; }

bool llist_prepend (struct llist *list, struct _llist *ins)
{ return (list && ins) ? (llist_insert (&list->head, ins)) : false; }

struct _llist* llist_pop_back (struct llist *list)
{ return (list) ? (llist_remove (list->tail.prev)) : NULL; }

struct _llist* llist_pop_front (struct llist *list)
{ return (list) ? (llist_remove (list->head.next)) : NULL; }

