/* filename ******** llist.c **************
 *
 * Provides linked list data structure embedded in structs.
 * Look to llist_test for usage.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
*/

#ifndef _LLIST_H
#define _LLIST_H

#include "lib/common.h"
#include "lib/types.h"
#include <stddef.h>

struct _llist { struct _llist *prev, *next; };
struct llist { struct _llist head, tail; };

#define llist_get(FROM, AT, WITH)       \
    ((FROM *) (((char*) (&(AT)->prev))  \
        - offsetof (FROM, WITH.prev)))

#define foreach(ELEM, LIST) \
    for (e = llist_iter_start(LIST);    \
         e != llist_iter_end(LIST);     \
         e = llist_iter_next(e))        \

void llist_init (struct llist *list);
bool llist_empty (struct llist *list);

struct _llist* llist_iter_start (struct llist* list);
struct _llist* llist_iter_next (struct _llist* cur);
struct _llist* llist_iter_prev (struct _llist* cur);
struct _llist* llist_iter_end (struct llist* list);

struct _llist* llist_riter_start (struct llist* list);
struct _llist* llist_riter_next (struct _llist* cur);
struct _llist* llist_riter_prev (struct _llist* cur);
struct _llist* llist_riter_end (struct llist* list);

bool llist_insert_ordered(struct llist *llist,
        struct _llist *ins, less_func list_less);
bool llist_insert (struct _llist *after, struct _llist *ins);
struct _llist* llist_remove (struct _llist* rem);
bool llist_append (struct llist *list, struct _llist *ins);
bool llist_prepend (struct llist *list, struct _llist *ins);

struct _llist* llist_pop_back (struct llist *list);
struct _llist* llist_pop_front (struct llist *list);

#endif
