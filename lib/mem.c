/* filename ******** mem.c **************
 *
 * Memory management
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/02/2015
*/

#include "mem.h"
#include <os/sync.h>


// Helper functions
#define USED (1 << (8*sizeof(int) - 1))

static size_t ints(size_t s) {
    if (s == 0)
        return 1;

    return (s + (sizeof(int)-1)) / sizeof(int);
}


// Count of used memory
uint_t mem_used;
uint_t mem_max_used;

// Heap and chunk states
static int *heap_start;
static int *heap_end;
static int *chunk_list;
static int chunk_count;

// Initialize memory
void mem_init(void) {
    // Setup basic heap using unused space in device
    mem_used = 0;
    mem_max_used = 0;

    heap_start = MEM_START;
    heap_end = MEM_END;

    int size = heap_end - heap_start;
    size -= 2;
    heap_start[0] = size;
    heap_end[-1] = size;

    // Start with zero chunks in case no one ever uses them
    chunk_list = 0;
    chunk_count = 0;
}

static void mem_update_stats(int_t count) {
    mem_used += count;

    if (mem_used > mem_max_used)
        mem_max_used = mem_used;
}

// Simple memory allocations and deallocations
void *mem_alloc(size_t bytes) {
    int size = ints(bytes);

    if (size <= CHUNK_SIZE/sizeof(int))
        return chunk_alloc();

    volatile uint_t gl = gl_acquire();
    int *block = heap_start;

    // Find the first block that fits
    while (block < heap_end) {
        // Check if block is corrupted
        assert(block[0] == block[(block[0] & ~USED)+1]);

        if (block[0] >= size) {
            // Check if we should split block
            if (block[0] >= size + (CHUNK_SIZE/sizeof(int))+2) {
                block[block[0]+1] = block[size+2] = block[0] - size - 2;
                block[0] = block[size+1] = size;
            }

            // mark block as used and remove from free list
            size = block[0];
            block[0] = block[size+1] = size | USED;

            mem_update_stats((size+2) * sizeof(int));

            gl_release(gl);
            return &block[1];
        }

        block += (block[0] & ~USED) + 2;
    }

    // Could not find enough space
    unreachable();
}

void mem_dealloc(void *p) {
    int *block = p;

    if (block == 0)
        return;

    // Check if this is actually a chunk
    if (block >= heap_end)
        return chunk_dealloc(block);

    block = &block[-1];
    int size = block[0] & ~USED;
    // Check if block is corrupted
    assert(block[0] == block[size+1]);

    volatile uint_t gl = gl_acquire();

    mem_update_stats(-(size+2) * sizeof(int));

    // Check if neighboring block is free
    while (true) {
        if (&block[-1] > heap_start && block[-1] > 0) {
            block = block - (block[-1]+2);
            size += block[0] + 2;
        } else if (&block[size+2] < heap_end && block[size+2] > 0) {
            size += block[size+2] + 2;
        } else {
            break;
        }
    }

    block[0] = block[size+1] = size;

    gl_release(gl);
}


// Permanent memory allocation
void *mem_alloc_permanent(size_t bytes) {
    int size = ints(bytes);
    volatile uint_t gl = gl_acquire();

    // We can only perform a permanent allocation if there is space
    // at the end, this allows checks against mem_end to be used to
    // see if memory is chunk allocated
    if (heap_end[-1]+2 > size) {
        // Check if we should split block
        if (heap_end[-1]+2 >= size + (CHUNK_SIZE/sizeof(int))+2) {
            heap_end[-heap_end[-1]-2] =
            heap_end[-size-1] = heap_end[-1] - size;
        }

        int *block = &heap_end[-size];
        heap_end -= size;

        mem_update_stats(size * sizeof(int));

        gl_release(gl);
        return block;
    }

    // Could not find enough space
    debug_halt();
    unreachable();
}


// Chunk allocation for CHUNK_SIZE blocks
void *chunk_alloc(void) {
    volatile uint_t gl = gl_acquire();

    // Check if we need more chunks
    if (!chunk_list) {
        // Amortize double the sizes we need
        int count = chunk_count ?: 4;
        int *chunks = mem_alloc_permanent(count*CHUNK_SIZE);
        mem_update_stats(-count * CHUNK_SIZE);

        // Could not find enough space
        if (chunks == 0)
            unreachable();

        // Thread in a linked list
        for (int i = 0; i < count-1; i++) {
            ((int **)chunks)[i*(CHUNK_SIZE/sizeof(int))] =
                &chunks[(i+1)*(CHUNK_SIZE/sizeof(int))];
        }

        chunks[(count-1)*(CHUNK_SIZE/sizeof(int))] = 0;

        chunk_list = chunks;
        chunk_count += count;
    }

    int *chunk = chunk_list;
    chunk_list = *(int **)chunk;
    mem_update_stats(CHUNK_SIZE);

    gl_release(gl);
    return chunk;
}

void chunk_dealloc(void *block) {
    volatile uint_t gl = gl_acquire();
    *(int **)block = chunk_list;
    chunk_list = block;
    mem_update_stats(-CHUNK_SIZE);
    gl_release(gl);
}
