/* filename ******** mem.h **************
 *
 * Memory management
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/02/2015
*/

#ifndef _MEM_H
#define _MEM_H

#include <lib/common.h>

// Regions defined as usable by memory management
extern int _ebss;
#define MEM_START ((void*)&_ebss)
#define MEM_END   ((void*)(0x20008000))

#define CHUNK_SIZE (4*sizeof(int))

extern uint_t mem_used;
extern uint_t mem_max_used;

// Initialize memory
void mem_init(void);

// Simple memory allocations and deallocation
void *mem_alloc(size_t size);
void mem_dealloc(void *);

// Permanent memory allocation
void *mem_alloc_permanent(size_t size);

// Chunk alloction for CHUNK_SIZE blocks
// This is arena allocated to allow for very quick
// handling of allocation and deallocation
void *chunk_alloc(void);
void chunk_dealloc(void *);

#endif
