/* filename ******** pqueue.c **************
 *
 * Provides pqueue data structure in an array.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
*/

#include "common.h"
#include "pqueue.h"
#include "mem.h"

#include <string.h>

#define pqueue_MAGIC 0x36c2f41a

#define pq_root 0
#define pq_parent(x) (((x) - 1) / 2)
#define pq_right(x) ((2*(x)) + 2)
#define pq_left(x) ((2*(x)) + 1)
#define pq_is_right(x) (((x) % 2) == 0)
#define pq_is_left(x) (!pq_is_right(x))

#define pq_set(h, x, v) (((h)->array[(x)]) = v)
#define pq_clear(h, x) (pq_set(h, x, PQUEUE_INVALID))


void _pqueue_bubble(struct pqueue *pqueue, uint32_t index);

void pqueue_init(struct pqueue *pqueue, size_t cnt, pqueue_cmp_func cmp) {
    pqueue->array = mem_alloc(sizeof(uint32_t) * cnt);
    pqueue->count = cnt;
    pqueue->compare = cmp;
    for (int i = 0; i < cnt; i++)
        pqueue->array[i] = PQUEUE_INVALID;
    pqueue->magic = pqueue_MAGIC;
}

void pqueue_deinit(struct pqueue *pqueue) {
    assert(pqueue->magic == pqueue_MAGIC);
    mem_dealloc(pqueue->array);
    pqueue->magic = 0;
}

bool pqueue_insert(struct pqueue *pqueue, void *elem) {
    uint32_t index = pqueue->count - 1;
    void *insert = elem;
    assert(pqueue->magic == pqueue_MAGIC);
    assert(elem != PQUEUE_INVALID);

    // Find the first empty leaf
    while (index > pq_root && !pqueue_invalid(pqueue, index))
        index--;
    if (index == pq_root && !pqueue_invalid(pqueue, index))
        // TODO: try growing the pqueue
        return false;

    // Find the first leaf with a non-empty parent
    while (index > pq_root && pqueue_invalid(pqueue, pq_parent(index)))
        index = pq_parent(index);

    uint32_t parent = pq_parent(index);
    while (index > pq_root &&
        !pqueue_invalid(pqueue, parent) &&
        pqueue->compare(insert, pqueue_get(pqueue, parent))) {
        void *temp = pqueue_get(pqueue, parent);
        pq_set(pqueue, index, temp);
        index = parent;
        parent = pq_parent(index);
    }
    pq_set(pqueue, index, elem);
    return true;
}

void *pqueue_peek(struct pqueue *pqueue) {
    return pqueue_get(pqueue, pq_root);
}

void *pqueue_pop(struct pqueue *pqueue) {
    void *root = pqueue_get(pqueue, pq_root);
    _pqueue_bubble(pqueue, pq_root);
    return root;
}

uint32_t pqueue_find(struct pqueue *pqueue, void *elem) {
    assert(pqueue->magic == pqueue_MAGIC);
    assert(elem != PQUEUE_INVALID);
    for (uint32_t i = 0; i < pqueue->count; i++) {
        if (pqueue_get(pqueue, i) != PQUEUE_INVALID &&
                !pqueue->compare(pqueue_get(pqueue, i), elem) &&
                !pqueue->compare(elem, pqueue_get(pqueue, i)))
            return i;
    }
    return PQUEUE_NOT_FOUND;
}

bool pqueue_remove(struct pqueue *pqueue, void *elem) {
    uint32_t tar = pqueue_find(pqueue, elem);
    if (tar == PQUEUE_NOT_FOUND)
        return false;
    _pqueue_bubble(pqueue, tar);
    return true;
}

void _pqueue_bubble(struct pqueue *pqueue, uint32_t index) {
    assert(pqueue->magic == pqueue_MAGIC);
    assert(index < pqueue->count);
    while (true) {
        if (index >= pqueue->count || pqueue_invalid(pqueue, index)) {
            // The path of removals has reached an end
            return;
        } else {
            // We have another element to bubble
            uint32_t left = pq_left(index),
                     right = pq_right(index);

            uint32_t next_index =
                (pqueue_invalid(pqueue, right) && pqueue_invalid(pqueue, left))
                    ? right :
                (pqueue_invalid(pqueue, right) ? left :
                (pqueue_invalid(pqueue, left) ? right :
                (pqueue->compare(pqueue_get(pqueue, right),
                              pqueue_get(pqueue, left)) ?
                    right : left)));
            pq_set(pqueue, index,
                    pqueue_invalid(pqueue, next_index) ? PQUEUE_INVALID :
                    pqueue_get(pqueue, next_index));
            index = next_index;
        }
    }
}

