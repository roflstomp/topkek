/* filename ******** pqueue.h **************
 *
 * Provides pqueue data structure in an array.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
*/


#include <lib/types.h>

#define PQUEUE_INVALID ((void *)-1)
#define PQUEUE_NOT_FOUND -1

#define pqueue_get(h, x) ((h)->array[(x)])
#define pqueue_invalid(h, x) \
    ((x) >= (h)->count || pqueue_get((h), (x)) == PQUEUE_INVALID)

typedef bool (*pqueue_cmp_func)(void *, void *);

struct pqueue {
    void * *array;
    size_t count;
    pqueue_cmp_func compare;
    uint32_t magic;
};

void pqueue_init(struct pqueue *pqueue, size_t cnt, pqueue_cmp_func cmp);
void pqueue_deinit(struct pqueue *pqueue);
bool pqueue_insert(struct pqueue *pqueue, void *elem);
bool pqueue_remove(struct pqueue *pqueue, void *elem);
void *pqueue_peek(struct pqueue *pqueue);
void *pqueue_pop(struct pqueue *pqueue);
uint32_t pqueue_find(struct pqueue *pqueue, void *elem);
