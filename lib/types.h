/* filename ******** types.h **************
 *
 * type definitions to replace stdint.h and stddef.h
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#ifndef __TYPES_H
#define __TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>
#include <inc/hw_types.h>

typedef signed long int_t;
typedef unsigned long uint_t;

typedef unsigned long block_t;

typedef struct file file_t;
typedef struct device device_t;

typedef union word {
    uint_t ui;
    int_t i;
    float f;
    void *p;
    uint16_t ui16[2];
    int16_t i16[2];
    uint8_t ui8[4];
    int8_t i8[4];
} word_t __attribute__((transparent_union));


typedef void (*cb_t)();
typedef bool (less_func)(void *, void *);

#endif // __TYPES_H
