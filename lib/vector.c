/* filename ******** heap.c **************
 *
 * Provides heap data structure in an array.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/9/2014
*/

#include "common.h"
#include "vector.h"
#include "mem.h"

#define VECTOR_START_SIZE 8
#define VECTOR_MAGIC 0x575b9ca7

void vector_init(struct vector *vec) {
    vec->size = VECTOR_START_SIZE;
    vec->buffer = mem_alloc(sizeof (void *) * vec->size);
    assert(vec->buffer != NULL);
    for (int i = 0; i < vec->size; i++)
        vec->buffer[i] = VECTOR_INVALID;
}

void vector_deinit(struct vector *vec) {
    mem_dealloc(vec->buffer);
    vec->size = 0;
}

void *vector_get(struct vector *vec, uint32_t idx) {
    if (idx >= vec->size)
        return VECTOR_INVALID;

    return vec->buffer[idx];
}

void vector_set(struct vector *vec, uint32_t idx, void *val) {
    if (idx >= vec->size) {
        uint_t new_size = vec->size;
        // Amortized doubling until the end of the array
        while (new_size <= idx)
            new_size *= 2;

        void **new_buffer = mem_alloc(sizeof (void *) * new_size);
        assert(new_buffer != NULL);

        int i;
        for (i = 0; i < vec->size; i++)
            new_buffer[i] = vec->buffer[i];
        for (; i < new_size; i++)
            new_buffer[i] = VECTOR_INVALID;
        mem_dealloc(vec->buffer);
        vec->buffer = new_buffer;
        vec->size = new_size;
    }
    vec->buffer[idx] = val;
}

int_t vector_find(struct vector *vec, void *val) {
    for (int i = 0; i < vec->size; i++)
        if (vec->buffer[i] == val)
            return i;
    return -1;
}
