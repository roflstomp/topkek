/* filename ******** vector.h **************
 *
 * Provides resizing array (amortized O(1) insertion)
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/9/2014
*/

#define VECTOR_INVALID (void *) 0

struct vector {
    uint_t size;
    void **buffer;
};

void vector_init(struct vector *vec);
void vector_deinit(struct vector *vec);
void *vector_get(struct vector *vec, uint32_t idx);
void vector_set(struct vector *vec, uint32_t idx, void *val);
int_t vector_find(struct vector *vec, void *val);
