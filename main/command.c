/* filename ******** command.h **************
 *
 * Kernel command line over UART0
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/05/2015
*/

#include <string.h>
#include <lib/types.h>
#include <lib/common.h>

#include <io/console.h>
#include <periph/adc.h>

#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>
#include <inc/hw_memmap.h>

#include <os/time.h>
#include <os/thread.h>
#include <lib/mem.h>

#define NOCASE (~('a' - 'A'))

extern struct task *cmd_stats_task;
extern struct task *cmd_port_task;

void cmd_register(void) {
    console_register("stats", "display useful system stats",
                     cmd_stats_task);
    console_register("port", "read state of a pin",
                     cmd_port_task);
}

TASK(cmd_stats, CONSOLE_PRIORITY, 128)(void) {
    console_printf("thread count %d\n", thread_count);
    console_printf("thread max %d\n", thread_max_count);
    console_printf("avg jitter %dus\n", time_avg_jitter);
    console_printf("max jitter %dus\n", time_max_jitter);

    console_printf("mem usage %d/%d bytes (%d%%)\n",
                   sizeof(uint_t)*mem_used, MEM_END-MEM_START,
                   (100*sizeof(uint_t)*mem_used) / (MEM_END-MEM_START));
    console_printf("mem max %d/%d bytes (%d%%)\n",
                   sizeof(uint_t)*mem_max_used, MEM_END-MEM_START,
                   (100*sizeof(uint_t)*mem_max_used) / (MEM_END-MEM_START));
}

TASK(cmd_port, CONSOLE_PRIORITY, 128)(const char *args) {
    uint32_t gpio_ports[] = {
        GPIO_PORTA_BASE,
        GPIO_PORTB_BASE,
        GPIO_PORTC_BASE,
        GPIO_PORTD_BASE,
        GPIO_PORTE_BASE,
        GPIO_PORTF_BASE,
    };

    if (*args == 0) {
        console_printf("No ports specified\n");
    }

    int len = strlen(args);
    if ((args[0] & NOCASE) == 'G') {                 // GPIO
        uint8_t port = (args[len-2] & NOCASE) - 'A';
        uint8_t pin = args[len-1] - '0';
        if (port > ('F' - 'A') || pin >= 8) {
            console_printf("%s: Malformed port description\n", args);
            return;
        }

        if (!SysCtlPeripheralReady(GetPeripheral(gpio_ports[port], pin)) ||
                GPIODirModeGet(gpio_ports[port], pin) !=
                    GPIO_DIR_MODE_IN) {
            console_printf("GPIO_P%c%d not configured for input\n",
                           'A' + port, pin);
            return;
        }

        console_printf("GPIO_P%c%d: %d\n", 'A' + port, pin,
                GPIOPinRead(gpio_ports[port], (1 << pin)) ? 1 : 0);

    } else if ((args[0] & NOCASE) == 'A') {          // ADC
        uint8_t port = (args[len-2] & NOCASE) - 'A';
        uint8_t pin = args[len-1] - '0';
        if (port > ('F' - 'A') || pin >= 8) {
            console_printf("%s: Malformed port description\n", args);
            return;
        }

        struct adc adc;
        if (false) { //if (adc_channel(gpio_ports[port], pin) == -1) {
            console_printf("%s: Invalid ADC pin %c%d\n", args, port+'a', pin);
            return;
        }

        adc_init(&adc, PIN(gpio_ports[port], pin));

        float value = adc_read(&adc);
        console_printf("ADC_P%c%d: 0.%06d\n",
                       port+'a', pin, (int)(value * 1000000.0f));

    } else {
        console_printf("%s: Malformed port description\n", args);
    }
}
