/* filename ******** command.h **************
 *
 * Kernel command line over UART0
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/05/2015
*/

// Register commands
void cmd_register(void);
