/* filename ******** main.c **************
 *
 * Initialize and run project
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#include <lib/types.h>
#include <lib/mem.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <io/io.h>
#include <io/print.h>
#include <io/tftio.h>
#include <io/console.h>

#include <os/time.h>
#include <os/thread.h>
#include <os/sync.h>
#include <os/pipe.h>

#include <periph/gpio.h>
#include <periph/adc.h>
#include <periph/sonar.h>
#include <periph/tft.h>
#include <periph/can0.h>

#include <gui/screen.h>
#include <gui/draw.h>
#include <gui/plot.h>

#include <inc/hw_gpio.h>
#include <inc/hw_memmap.h>
#include <inc/tm4c123gh6pm.h>

#include <driverlib/interrupt.h>
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>

#include <io/navi.h>
#include <io/fat.h>
#include <io/sd.h>

#include "command.h"

TASK(heartbeat, 0x00, 32)(void) {
    *PIN_F3 ^= 0xff;
}

#define IR_COUNT 3
pin_t ir_pin[IR_COUNT] = {PIN_E0, PIN_E1, PIN_E2};
struct adc ir_module[IR_COUNT];
volatile float ir_dat[IR_COUNT];

word_t motor_vals = {0};

void ir_read(void *arg, word_t res) {
    int index = (int) arg;
    while(true) {
        ir_dat[index] = ((adc_read(&ir_module[index])));
    }
}

struct task __ir0_task = TASK_INIT(ir_read, 0x00, 64);
struct task __ir1_task = TASK_INIT(ir_read, 0x00, 64);
struct task __ir2_task = TASK_INIT(ir_read, 0x00, 64);
struct task *ir_task[IR_COUNT] = {
    &__ir0_task, &__ir1_task, &__ir2_task,
};

TASK(reset_bus, 0x00, 128)(void) {
    CAN0_Open();
}

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) < (y)) ? (y) : (x))
#define SATURATE(x, y, z) (((y) > 0) ? MIN((y), (z)) : MAX((x), (y)))

#define STOP_TIME 180000000

int main(void) {
    // Initialize peripherals
    io_init();
    mem_init();
    time_init();
    thread_init();
    console_init();
    plot_init();
    CAN0_Open();

    gpio_input(PIN_B0);
    // Initialize IR
    for (int i = 0; i < IR_COUNT; i++) {
        adc_init(&ir_module[i], ir_pin[i]);
        thread_spawn(ir_task[i], i);
    }

    gpio_output(PIN_F2);
    gpio_output(PIN_F3);
    thread_preemption(20000);
    time_call_every(500000, heartbeat_task);
    time_wait(80000);
    time_call_every(50000, reset_bus_task);

    time_t start = time_current();
    float old_error = 0;
    while (true) {
        thread_yield();
        if (*PIN_B0) {
            motor_vals.i16[0] = 0x7fff;
            motor_vals.i16[1] = -0x6400;
            time_wait((2 * 1000000) / 3);
        }

        // PID loop
#define P_CONST .01
#define D_CONST 0
#define F_STOP .93f
#define F_CONST 1
#define SPEED 1
        float forward_error = F_CONST * SATURATE(1,
                1 + ir_dat[0] - ((ir_dat[1] + ir_dat[2]) / 2), 2);
        forward_error = 1;

        float error = P_CONST * forward_error * (ir_dat[1] - ir_dat[2]);
        float t_error = error + (D_CONST * (error - old_error));
        old_error = error;

        float fwd_stop = (ir_dat[0] > F_STOP) ? 2 : 0;
        float power_l = SATURATE(-1, 1 + t_error - fwd_stop, 1);
        float power_r = SATURATE(-1, 1 - t_error - fwd_stop, 1);

        // right
        motor_vals.i16[0] = 0x7fff * SPEED * power_r;
        // left
        motor_vals.i16[1] = 0x7fff * SPEED * power_l;

        if ((time_current() - start) > STOP_TIME) {
            break;
        }

        // console_printf("%f %f %f ", error, power_l, power_r);
        for (int i = 0; i < IR_COUNT; i++) {
            console_printf("%7d  ", (int) (ir_dat[i] *  10000));
        }
        console_printf("\n");

        // CAN0_Open();
        CAN0_SendData(motor_vals.ui8);
    }
    motor_vals.ui = 0;
    *PIN_F2 = 0xff;
    console_printf("180s have passed; stopping\n");
    while (true);
}
