/* filename ******** startup.c **************
 *
 * Startup and initialization of board
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#include <lib/common.h>
#include <lib/types.h>
#include <periph/gpio.h>

#include <inc/hw_memmap.h>
#include <inc/hw_nvic.h>
#include <inc/hw_types.h>
#include <driverlib/gpio.h>
#include <driverlib/fpu.h>
#include <driverlib/sysctl.h>
#include <driverlib/pin_map.h>


// Initial stack
#ifndef MAIN_STACK_SIZE
#define MAIN_STACK_SIZE 256
#endif

uint_t main_stack[MAIN_STACK_SIZE];
uint_t isr_stack[128];

// Entry task
extern void main(void);
extern void gpio_portf_isr(void);

// Entry point to the processor
void reset_isr(void) {
    // Copy over data to data segment
    asm(
    "   ldr r0, =_data      \n"
    "   ldr r1, =_edata     \n"
    "   ldr r2, =_etext     \n"
    "data_loop:             \n"
    "   cmp r0, r1          \n"
    "   ittt lt             \n"
    "   ldrlt r3, [r2], #4  \n"
    "   strlt r3, [r0], #4  \n"
    "   blt data_loop       \n"
    );

    // Zero fill the bss segment
    asm(
    "   ldr r0, =_bss       \n"
    "   ldr r1, =_ebss      \n"
    "   mov r2, #0          \n"
    "zero_loop:             \n"
    "   cmp r0, r1          \n"
    "   itt lt              \n"
    "   strlt r2, [r0], #4  \n"
    "   blt zero_loop       \n"
    );

    // Enable PLL
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 |
                   SYSCTL_USE_PLL    |
                   SYSCTL_OSC_MAIN   |
                   SYSCTL_XTAL_16MHZ);

    // Enable the fpu
    FPUEnable();
    FPUStackingEnable();

    // Switch to the main stack
    asm(
    "   msr psp, %0         \n"
    "   mov r0, #0x2        \n"
    "   msr control, r0     \n"
    :: "r" ((void *)main_stack + sizeof main_stack)
    );

    // Enter the main task
    main();

    unreachable();
}

// Fault handler
__attribute__((naked))
void fault_isr(void) {
    debug_bkpt();

    gpio_output(PIN_F1);

    while (true) {
        *PIN_F1 = 0xff;
        for (volatile int i = 0; i < 200000; i++);
        *PIN_F1 = 0x00;
        for (volatile int i = 0; i < 200000; i++);
    }
}

// NMI handler
void nmi_isr(void) {
    debug_halt();
}

// Default handler for debugging
void default_isr(void) {
    debug_halt();
}


// Externally declared ISRs
extern void systick_isr(void);
extern void thread_scheduler(void);
extern void adc_isr(void);
extern void gpio_isr(void);
extern void thread_isr(void);
extern void console_isr(void);
extern void can0_isr(void);


// Interrupt vector table
// resides at memory location 0x00000000
__attribute__((section(".isr_vector")))
void (* const isr_vector[])(void) = {
    ((void *)isr_stack + sizeof isr_stack), // The initial stack pointer
    reset_isr,                              // The reset handler
    nmi_isr,                                // The NMI handler
    fault_isr,                              // The hard fault handler
    default_isr,                            // The MPU fault handler
    default_isr,                            // The bus fault handler
    default_isr,                            // The usage fault handler
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // SVCall handler
    default_isr,                            // Debug monitor handler
    0,                                      // Reserved
    default_isr,                            // The PendSV handler
    systick_isr,                            // The SysTick handler
    gpio_isr,                               // GPIO Port A
    gpio_isr,                               // GPIO Port B
    gpio_isr,                               // GPIO Port C
    gpio_isr,                               // GPIO Port D
    gpio_isr,                               // GPIO Port E
    console_isr,                            // UART0 Rx and Tx
    default_isr,                            // UART1 Rx and Tx
    default_isr,                            // SSI0 Rx and Tx
    default_isr,                            // I2C0 Master and Slave
    default_isr,                            // PWM Fault
    default_isr,                            // PWM Generator 0
    default_isr,                            // PWM Generator 1
    default_isr,                            // PWM Generator 2
    default_isr,                            // Quadrature Encoder 0
    adc_isr,                                // ADC Sequence 0
    default_isr,                            // ADC Sequence 1
    default_isr,                            // ADC Sequence 2
    default_isr,                            // ADC Sequence 3
    default_isr,                            // Watchdog timer
    thread_isr,                             // Timer 0 subtimer A
    default_isr,                            // Timer 0 subtimer B
    default_isr,                            // Timer 1 subtimer A
    default_isr,                            // Timer 1 subtimer B
    default_isr,                            // Timer 2 subtimer A
    default_isr,                            // Timer 2 subtimer B
    default_isr,                            // Analog Comparator 0
    default_isr,                            // Analog Comparator 1
    default_isr,                            // Analog Comparator 2
    default_isr,                            // System Control (PLL, OSC, BO)
    default_isr,                            // FLASH Control
    gpio_isr,                               // GPIO Port F
    default_isr,                            // GPIO Port G
    default_isr,                            // GPIO Port H
    default_isr,                            // UART2 Rx and Tx
    default_isr,                            // SSI1 Rx and Tx
    default_isr,                            // Timer 3 subtimer A
    default_isr,                            // Timer 3 subtimer B
    default_isr,                            // I2C1 Master and Slave
    default_isr,                            // Quadrature Encoder 1
    can0_isr,                               // CAN0
    default_isr,                            // CAN1
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // Hibernate
    default_isr,                            // USB0
    default_isr,                            // PWM Generator 3
    default_isr,                            // uDMA Software Transfer
    default_isr,                            // uDMA Error
    adc_isr,                                // ADC1 Sequence 0
    default_isr,                            // ADC1 Sequence 1
    default_isr,                            // ADC1 Sequence 2
    default_isr,                            // ADC1 Sequence 3
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // GPIO Port J
    default_isr,                            // GPIO Port K
    default_isr,                            // GPIO Port L
    default_isr,                            // SSI2 Rx and Tx
    default_isr,                            // SSI3 Rx and Tx
    default_isr,                            // UART3 Rx and Tx
    default_isr,                            // UART4 Rx and Tx
    default_isr,                            // UART5 Rx and Tx
    default_isr,                            // UART6 Rx and Tx
    default_isr,                            // UART7 Rx and Tx
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // I2C2 Master and Slave
    default_isr,                            // I2C3 Master and Slave
    default_isr,                            // Timer 4 subtimer A
    default_isr,                            // Timer 4 subtimer B
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // Timer 5 subtimer A
    default_isr,                            // Timer 5 subtimer B
    default_isr,                            // Wide Timer 0 subtimer A
    default_isr,                            // Wide Timer 0 subtimer B
    default_isr,                            // Wide Timer 1 subtimer A
    default_isr,                            // Wide Timer 1 subtimer B
    default_isr,                            // Wide Timer 2 subtimer A
    default_isr,                            // Wide Timer 2 subtimer B
    default_isr,                            // Wide Timer 3 subtimer A
    default_isr,                            // Wide Timer 3 subtimer B
    default_isr,                            // Wide Timer 4 subtimer A
    default_isr,                            // Wide Timer 4 subtimer B
    default_isr,                            // Wide Timer 5 subtimer A
    default_isr,                            // Wide Timer 5 subtimer B
    default_isr,                            // FPU
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // I2C4 Master and Slave
    default_isr,                            // I2C5 Master and Slave
    default_isr,                            // GPIO Port M
    default_isr,                            // GPIO Port N
    default_isr,                            // Quadrature Encoder 2
    0,                                      // Reserved
    0,                                      // Reserved
    default_isr,                            // GPIO Port P (Summary or P0)
    default_isr,                            // GPIO Port P1
    default_isr,                            // GPIO Port P2
    default_isr,                            // GPIO Port P3
    default_isr,                            // GPIO Port P4
    default_isr,                            // GPIO Port P5
    default_isr,                            // GPIO Port P6
    default_isr,                            // GPIO Port P7
    default_isr,                            // GPIO Port Q (Summary or Q0)
    default_isr,                            // GPIO Port Q1
    default_isr,                            // GPIO Port Q2
    default_isr,                            // GPIO Port Q3
    default_isr,                            // GPIO Port Q4
    default_isr,                            // GPIO Port Q5
    default_isr,                            // GPIO Port Q6
    default_isr,                            // GPIO Port Q7
    default_isr,                            // GPIO Port R
    default_isr,                            // GPIO Port S
    default_isr,                            // PWM 1 Generator 0
    default_isr,                            // PWM 1 Generator 1
    default_isr,                            // PWM 1 Generator 2
    default_isr,                            // PWM 1 Generator 3
    default_isr,                            // PWM 1 Fault
};
