#!arm-none-eabi-gdb -x
define reset
    monitor reset halt
end

define flash
    monitor halt
    monitor flash write_image erase $arg0.out
    monitor verify_image $arg0.out
end

target remote localhost:3333
reset

define offsetof
    set $rc = (char*)&((struct $arg0 *)0)->$arg1 - (char*)0
end

define llist_get
    offsetof $arg1 $arg2
    set $rc = ((struct $arg1 *) ((char *) ($arg0) - $rc))
end

define dumplist
    set $list = $arg0
    set $e = $list->head.next
    set $i = 0
    while $e != &(($arg0).tail)
        llist_get $e $arg1 $arg2
        set $l = $rc
        printf "dumplist #%d: %p ", $i++, $l
        output *$l
        set $e = $e->next
        printf "\n"
    end
end

define pqueue_get
    set $rc = ((struct $arg2 *) $arg0->array[$arg1])
end

define dumpqueue
    set $pqueue = $arg0
    set $i = 0
    while $i < $arg0->count
        pqueue_get $pqueue $i $arg1
        set $l = $rc
        if $l != 0xffffffff
            printf "dumpqueue #%d: %p ", $i, $l
            output *$l
            printf "\n"
        end
        set $i = $i + 1
    end
end

define memdump
    set $i = heap_start
    set $error = 0

    while !$error && $i < heap_end
        if $i[0] != $i[($i[0] & ~0x80000000)+1]
            printf "0x%08x corrupted block! %d != %d\n", \
                   &$i[1], $i[0], $i[($i[0] & ~0x80000000)+1]
            set $error = 1
        else
            printf "0x%08x ", &$i[1]

            if $i[0] & 0x80000000
                printf "used block "
            else
                printf "free block "
            end

            printf "%d bytes\n", 4*($i[0] & ~0x80000000)
            set $i = &$i[($i[0] & ~0x80000000)+2]
        end
    end

    if !$error
        printf "0x%08x perm block %d bytes\n", \
               heap_end, (0x20008000 - (uint_t)heap_end)
    end
end

define memstats
    set $i = heap_start
    set $heap_used = 0
    set $error = 0

    while !$error && $i < heap_end
        if $i[0] != $i[($i[0] & ~0x80000000)+1]
            printf "0x%08x corrupted block! %d != %d\n", \
                   &$i[1], $i[0], $i[($i[0] & ~0x80000000)+1]
            set $error = 1
        else
            if $i[0] & 0x80000000
                set $heap_used = $heap_used + ($i[0] & ~0x80000000) + 2
            end

            set $i = &$i[($i[0] & ~0x80000000)+2]
        end
    end

    if !$error
        set $heap_free = (heap_end-heap_start) - $heap_used

        printf "heap usage %d/%d words (%d%%)\n", \
               $heap_used, $heap_free + $heap_used, \
               ($heap_free + $heap_used == 0) ? 0 : \
               (100*$heap_used) / ($heap_free + $heap_used)
    end

    if !$error
        set $i = chunk_list
        set $chunks_free = 0

        while $i != 0
            set $chunks_free = $chunks_free + 1
            set $i = *(int **)$i
        end

        set $chunks_used = chunk_count - $chunks_free

        printf "chunk usage %d/%d chunks (%d%%)\n", \
               $chunks_used, $chunks_free + $chunks_used, \
               ($chunks_free + $chunks_used == 0) ? 0 : \
               (100*$chunks_used) / ($chunks_free + $chunks_used)
    end

    if !$error
        if heap_end[-1] & 0x80000000
            set $perm_free = 0
        else
            set $perm_free = (heap_end[-1] & ~0x80000000) + 2
        end

        set $perm_used = ((int *)0x20008000) - heap_end

        printf "perm usage %d/%d words (%d%%)\n", \
               $perm_used, $perm_free + $perm_used, \
               ($perm_free + $perm_used == 0) ? 0 : \
               (100*$perm_used) / ($perm_free + $perm_used)
    end

    if !$error
        set $used = (4*$heap_used) + \
                    (4*4*$chunks_used) + \
                    (4*$perm_used) - (4*4*($chunks_free + $chunks_used))

        set $free = (4*$heap_free) + \
                    (4*4*$chunks_free)

        printf "total usage %d/%d bytes (%d%%)\n", \
               $used, $free + $used, \
               ($free + $used == 0) ? 0 : \
               (100*$used) / ($free + $used)

        printf "max usage %d/%d bytes (%d%%)\n", \
               mem_max_used, $free + $used, \
               ($free + $used == 0) ? 0 : \
               (100*mem_max_used) / ($free + $used)
    end
end

define jitterstats
    printf "avg jitter %dus\n", time_avg_jitter
    printf "max jitter %dus\n", time_max_jitter
end

define osstats
    printf "thread count %d\n", thread_count
    printf "thread max %d\n", thread_max_count
    jitterstats

    printf "mem usage %d/%d bytes (%d%%)\n", \
           4*mem_used, 0x20008000 - heap_start, \
           (100*4*mem_used) / (0x20008000 - heap_start)
    printf "mem max %d/%d bytes (%d%%)\n", \
           4*mem_max_used, 0x20008000 - heap_start, \
           (100*4*mem_max_used) / (0x20008000 - heap_start)
end

define thread_print
    printf $arg0
    printf "{"
    output/a $arg1
    printf ", "
    output/a $arg1->func
    printf ", pri %d, stack %d}\n", \
            $arg1->priority, $arg1->stack
end

define osdump
    thread_print "current   " current_thread
    thread_print "scheduled " scheduled_thread

    if (SCHED_STRUCT == SCHED_STRUCT_PQ)
        set $i = 0
        set $t = (struct task *)thread_pq->array[$i]
        while $i < thread_pq->count && $t != (void *)-1
            thread_print "pending   " $t

            set $i = $i + 1
            set $t = (struct task *)thread_pq->array[$i]
        end
    else
        set $i = 0
        set $e = thread_list->head.next
        while $e != &thread_list->tail
            llist_get $e task sched_elem
            set $t = $rc
            thread_print "pending   " $t

            set $i = $i + 1
            set $e = $e->next
        end
    end

    set $t = time_pending
    while $t != 0
        thread_print "delayed   " $t
        set $t = $t->next
    end
end
