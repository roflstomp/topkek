/* filename ******** pipe.c **************
 *
 * Pipe stuff
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/27/2015
*/


#include "pipe.h"
#include <lib/mem.h>
#include <os/thread.h>


void pipe_init(struct pipe *pipe, uint_t size) {
    pipe->mask = npw2(size) - 1;
    pipe->data = (word_t *)mem_alloc((pipe->mask+1) * sizeof(word_t));

    pipe->give = 0;
    pipe->take = 0;
    pipe->count = 0;
}

void pipe_give(struct pipe *pipe, word_t data) {
    uint_t gl;
    while (true) {
        gl = gl_acquire();
        if (pipe->count <= pipe->mask) break;
        gl_release(gl);
        thread_yield();
    }

    pipe->data[pipe->give++ & pipe->mask] = data;
    pipe->count++;

    gl_release(gl);
}

word_t pipe_take(struct pipe *pipe) {
    uint_t gl;
    while (true) {
        gl = gl_acquire();
        if (pipe->count > 0) break;
        gl_release(gl);
        thread_yield();
    }

    word_t data = pipe->data[pipe->take++ & pipe->mask];
    pipe->count--;

    gl_release(gl);
    return data;
}

bool pipe_try_give(struct pipe *pipe, word_t data) {
    uint_t gl = gl_acquire();
    if (pipe->count > pipe->mask) {
        gl_release(gl);
        return false;
    }

    pipe->data[pipe->give++ & pipe->mask] = data;
    pipe->count++;

    gl_release(gl);
    return true;
}

bool pipe_try_take(struct pipe *pipe, word_t *data) {
    uint_t gl = gl_acquire();
    if (pipe->count <= 0) {
        gl_release(gl);
        return false;
    }

    *data = pipe->data[pipe->take++ & pipe->mask];
    pipe->count--;

    gl_release(gl);
    return true;
}
