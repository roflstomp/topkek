/* filename ******** pipe.c **************
 *
 * Pipe stuff
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/27/2015
*/


#ifndef __PIPE_H
#define __PIPE_H

#include <lib/types.h>

struct pipe {
    word_t *data;
    uint_t give;
    uint_t take;
    uint_t count;
    uint_t mask;
};

void pipe_init(struct pipe *pipe, uint_t size);
void pipe_give(struct pipe *pipe, word_t data);
word_t pipe_take(struct pipe *pipe);
bool pipe_try_give(struct pipe *pipe, word_t data);
bool pipe_try_take(struct pipe *pipe, word_t *data);


#endif // __PIPE_H

