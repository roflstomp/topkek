/* filename ******** llist.c **************
 *
 * Provides linked list data structure embedded in structs.
 * Look to llist_test for usage.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/6/2014
*/


#include <lib/common.h>
#include "sync.h"
#include <os/thread.h>

asm_fn bool ints_get(void) {
    asm(
    "   mrs r0, primask \n"
    "   bx lr           \n"
    );
}

asm_fn void ints_enable(void) {
    asm(
    "   cpsie I         \n"
    "   bx lr           \n"
    );
}

asm_fn void ints_disable(void) {
    asm(
    "   cpsid I         \n"
    "   bx lr           \n"
    );
}

asm_fn uint_t gl_acquire(void) {
    asm(
    "   mrs r0, primask \n"
    "   cpsid I         \n"
    "   bx lr           \n"
    );
}

asm_fn void gl_release(uint_t lock) {
    asm(
    "   msr primask, r0 \n"
    "   bx lr           \n"
    );
}


void spinlock_init(spinlock_t *lock) {
    *lock = 0;
}

void spinlock_acquire(spinlock_t *lock) {
    uint_t gl;

    while (true) {
        gl = gl_acquire();
        if (*lock == 0) break;
        gl_release(gl);
        thread_yield();
    }

    *lock = 1;
    gl_release(gl);
}

void spinlock_release(spinlock_t *lock) {
    *lock = 0;
}

bool sema_thread_order(void *a, void *b) {
    if (!a) return false;
    if (!b) return true;
    return thread_order(
            llist_get(struct task, (struct _llist *) a, wait_elem),
            llist_get(struct task, (struct _llist *) b, wait_elem));
}

void sema_init(struct sema* sema, uint32_t count) {
    spinlock_init(&sema->lock);
    llist_init(&sema->waiters);
    sema->count = count;
}

void sema_deinit(struct sema *sema) {
    // TODO: confirm that there are no waiters
    while (!llist_empty(&sema->waiters)) {
        sema_up(sema);
    }
}

void sema_down(struct sema* sema) {
    // TODO: check if we are in an interrupt handler
    struct task *cur = thread_current();
    spinlock_acquire(&sema->lock);

    volatile uint32_t _count = sema->count;

    while (_count <= 0) {
        llist_insert_ordered(&sema->waiters,
                &cur->wait_elem, sema_thread_order);

        spinlock_release(&sema->lock);

        thread_block(sema);

        spinlock_acquire(&sema->lock);
        _count = sema->count;
    }
    // Give the thread the semaphore
    sema->count--;

    spinlock_release(&sema->lock);
}

void sema_up(struct sema* sema) {
    spinlock_acquire(&sema->lock);

    sema->count++;

    // Find the highest priority waiter and unblock it
    struct task *t = NULL;
    if (!llist_empty(&sema->waiters)) {
        t = llist_get(struct task,
                llist_pop_back(&sema->waiters), wait_elem);
    }
    spinlock_release(&sema->lock);

    if (t) {
        thread_unblock(t);
        thread_yield();
    }
}

