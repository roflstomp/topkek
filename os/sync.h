/* filename ******** sync.h **************
 *
 * Synchronization primitives.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 02/19/2015
*/


#ifndef __SYNC_H
#define __SYNC_H

#include <lib/types.h>
#include <lib/llist.h>

bool ints_get(void);

void ints_disable(void);
void ints_enable(void);

uint_t gl_acquire(void);
void gl_release(uint_t);


typedef volatile uint_t spinlock_t;
struct sema {
    spinlock_t lock;
    volatile uint32_t count;
    struct llist waiters;
};


void spinlock_init(spinlock_t *lock);
void spinlock_acquire(spinlock_t *lock);
void spinlock_release(spinlock_t *lock);

void sema_init(struct sema* sema, uint32_t count);
void sema_deinit(struct sema* sema);
void sema_down(struct sema* sema);
void sema_up(struct sema* sema);


#endif // __SYNC_H
