/* filename ******** thread.c **************
 *
 * RTOS threading functionality and interface
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#include "thread.h"
#include "sync.h"
#include "time.h"

#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#include <lib/mem.h>
#include <lib/pqueue.h>

#include <inc/hw_memmap.h>
#include <inc/hw_ints.h>
#include <inc/hw_nvic.h>
#include <inc/hw_timer.h>
#include <driverlib/gpio.h>
#include <driverlib/sysctl.h>
#include <driverlib/systick.h>
#include <driverlib/interrupt.h>
#include <driverlib/timer.h>


#define MAX_THREAD_COUNT 128

struct saved_regs {
    // This part of the stack is managed by pendsv
    uint32_t r4, r5, r6, r7, r8, r9, r10, r11;
    // The stack as expected by the interrupt return machinery
    uint32_t r0, r1, r2, r3, r12, lr, pc, psr;
};

// Data describing the default thread's existance
extern int main_stack[128];

// Threading state
static struct task *scheduled_thread;
static struct task *current_thread;

#if (SCHED_STRUCT == SCHED_STRUCT_PQ)
static struct pqueue __thread_pq;
static struct pqueue *thread_pq = &__thread_pq;
#elif (SCHED_STRUCT == SCHED_STRUCT_LIST)
static struct llist __thread_list;
static struct llist *thread_list = &__thread_list;
bool thread_list_order(void *a, void *b);
#endif // SCHED_STRUCT == SCHED_STRUCT_LIST

uint32_t preemption_period;
uint_t thread_count;
uint_t thread_max_count;

struct task *compute_current(struct task *from) {
    // Walk the graph from the thread we picked to schedule (highest priority)
    // to find a thread we can run instead which will let us run this thread.
    // We're donating the currently scheduled thread's quantum to another,
    // effectively implementing priority donation.
    struct task *cur = from;
#if (SCHED_ALGO == SCHED_ALGO_PRI_DON)
    assert(cur->magic == THREAD_MAGIC);

    while (cur->state == THR_BLOCKED) {
        // TODO: find a thread to which to donate the quantum
        assert(cur->magic == THREAD_MAGIC);
    }
#endif
    return cur;
}

struct task *thread_current(void) {
    assert(current_thread->magic == THREAD_MAGIC);
    return current_thread;
}

static void thread_insert(const struct task *t) {
    bool success = false;
    uint_t gl = gl_acquire();

#if (SCHED_STRUCT == SCHED_STRUCT_LIST)
#if (SCHED_ALGO == SCHED_ALGO_PRI)
    success = llist_insert_ordered(thread_list,
            &t->sched_elem, thread_list_order);
#elif (SCHED_ALGO == SCHED_ALGO_RROBIN)
    success = llist_append(thread_list, &t->sched_elem);
#endif // SCHED_ALGO
#elif (SCHED_STRUCT == SCHED_STRUCT_PQ)
    success = pqueue_insert(thread_pq, t);
#endif // SCHED_STRUCT

    assert(success);
    if (!success) {
        // We must succeed in returning the thread to the data structure
        unreachable();
    }
    gl_release(gl);
}

static void thread_remove(const struct task *t) {
    bool success = false;
    uint_t gl = gl_acquire();

#if (SCHED_STRUCT == SCHED_STRUCT_PQ)
    success = pqueue_remove(thread_pq, t);
#elif (SCHED_STRUCT == SCHED_STRUCT_LIST)
    success = llist_remove(&t->sched_elem);
#endif // SCHED_STRUCT

    assert(success);
    if (!success) {
        // We must succeed in returning the thread to the data structure
        unreachable();
    }
    gl_release(gl);
}

static struct task *thread_pop(void) {
    struct task *t;
    uint_t gl = gl_acquire();

#if (SCHED_STRUCT == SCHED_STRUCT_PQ)
    t = pqueue_pop(thread_pq);
    assert(t != PQUEUE_INVALID);
#elif (SCHED_STRUCT == SCHED_STRUCT_LIST)
    struct _llist *e = llist_pop_front(thread_list);
    assert(e != NULL);
    t = llist_get(struct task, e, sched_elem);
    assert(t != NULL);
#endif

    gl_release(gl);
    return t;
}

void thread_init(void) {
    // Reset thread count
    thread_count = 0;
    thread_max_count = 0;

    // Create a thread struct describing the main thread
    // on top of the main stack and make that the current thread
    scheduled_thread = (struct task *)main_stack;
    scheduled_thread->func = 0;
    scheduled_thread->priority = 0xff;
    scheduled_thread->state = THR_RUNNING;
    scheduled_thread->stacksize = sizeof(main_stack)/sizeof(uint_t);
    scheduled_thread->magic = THREAD_MAGIC;

    current_thread = scheduled_thread;

    preemption_period = 0;

    // Initialize scheduling data structure
#if (SCHED_STRUCT == SCHED_STRUCT_PQ)
    pqueue_init(thread_pq, MAX_THREAD_COUNT, thread_order);
#elif (SCHED_STRUCT == SCHED_STRUCT_LIST)
    llist_init(thread_list);
#endif

    // Setup timer in case premptive threading is wanted
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    TimerConfigure(TIMER0_BASE, TIMER_CFG_ONE_SHOT);
    TimerControlStall(TIMER0_BASE, TIMER_A, true);

    IntPrioritySet(INT_TIMER0A, 0xff);
    IntEnable(INT_TIMER0A);
}

static void thread_update_stats(int_t count) {
    uint_t gl = gl_acquire();

    thread_count += count;

    if (thread_count > thread_max_count)
        thread_max_count = thread_count;

    gl_release(gl);
}

void thread_preemption(time_t period) {
    // Configure the Timer in one-shot mode so that scheduling overhead isn't
    // included in the quantum. All non-thread code of reasonable duration
    // should call thread_pause and thread_unpause so as to minimize their
    // impact on the thread's quantum.
    if (period > 0) {
        preemption_period = period;
        TimerLoadSet(TIMER0_BASE, TIMER_A,
                period * (SysCtlClockGet() / 1000000));
        TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
        TimerEnable(TIMER0_BASE, TIMER_A);
    } else {
        TimerDisable(TIMER0_BASE, TIMER_A);
    }
}

void thread_pause(void) {
    TimerDisable(TIMER0_BASE, TIMER_A);
}

void thread_unpause(void) {
    TimerEnable(TIMER0_BASE, TIMER_A);
}

void thread_spawn(struct task *thread, ...) {
    va_list args;

    if (!thread || thread->state != THR_DEAD)
        return;

    assert(thread->magic == THREAD_MAGIC);
    assert(sizeof(uint_t)*thread->stacksize > sizeof(struct saved_regs));

    thread->sp = &thread->stack[thread->stacksize
        - (sizeof(struct saved_regs)/sizeof(uint_t))];
    thread->last_run = time_current();

    va_start(args, thread);
    *(struct saved_regs *)thread->sp = (struct saved_regs) {
        .r0 = va_arg(args, uint_t),
        .r1 = va_arg(args, uint_t),
        .r2 = va_arg(args, uint_t),
        .r3 = va_arg(args, uint_t),
        .pc = (uint_t)thread->func,
        .lr = (uint_t)thread_exit,
        .psr = 0x01000000, // Magic number for default PSR
    };
    va_end(args);

    thread->state = THR_READY;

    // Add thread to data structure
    thread_insert(thread);
    thread_update_stats(1);

    // Start the task immediately
    // thread_yield();
}

void thread_yield(void) {
    IntTrigger(INT_TIMER0A);
}

void thread_block(struct sema *blocked) {
    struct task *cur = thread_current();
    cur->state = THR_BLOCKED;
    thread_yield();
}

void thread_unblock(struct task *t) {
    assert(t->magic == THREAD_MAGIC);
    assert(t->state == THR_BLOCKED);
    t->state = THR_READY;
    thread_insert(t);
}

bool thread_list_order(void *a, void *b) {
#if (SCHED_STRUCT != SCHED_STRUCT_LIST)
    unreachable();
#else
    struct task *t1 = llist_get(struct task,
            (struct _llist *) a, sched_elem);
    struct task *t2 = llist_get(struct task,
            (struct _llist *) b, sched_elem);
    return thread_order(t1, t2);
#endif
}

bool thread_order(void *a, void *b) {
    struct task *t1 = (struct task *)a;
    struct task *t2 = (struct task *)b;
    assert(t1 == NULL || t1->magic == THREAD_MAGIC);
    assert(t2 == NULL || t2->magic == THREAD_MAGIC);
    if (t1 == NULL) return false;
    if (t2 == NULL) return true;

    if (t1->state == THR_BLOCKED) return false;
    if (t2->state == THR_BLOCKED) return true;

    int32_t t1_pri = t1->priority;
    int32_t t2_pri = t2->priority;

    if (t1_pri != t2_pri)
        return t1_pri < t2_pri;

#if (SCHED_ALGO == SCHED_ALGO_PRI)
    return (int_t)(t1->last_run - t2->last_run) < 0;
#else
    return false;
#endif
}

noreturn thread_exit(void) {
    thread_current()->state = THR_DEAD;
    // TODO: Release all resources
    thread_update_stats(-1);
    thread_yield();
    unreachable();
}

static uint_t *thread_switch(uint_t *sp) {
    struct task *cur = thread_current();
    assert(cur->magic == THREAD_MAGIC);
    cur->sp = sp;
    cur->last_run = time_current();

    // Managing dying threads is handled here for simplicity
    do {
        switch (cur->state) {
        case THR_RUNNING:
        case THR_READY:
            cur->state = THR_READY;
            thread_insert(scheduled_thread);
        case THR_BLOCKED:
        case THR_DEAD:
            break;
        }

        // Select next thread
        scheduled_thread = thread_pop();
        assert(scheduled_thread->magic == THREAD_MAGIC);
        cur = compute_current(scheduled_thread);
    } while (cur->state != THR_READY);

    // Change thread state
    assert(cur->magic == THREAD_MAGIC);
    assert(cur->state == THR_READY);
    current_thread = cur;
    cur->state = THR_RUNNING;

    return cur->sp;
}

asm_fn void thread_isr(void) {
    uint_t *old_psp, *new_psp;
    asm(
    "   mrs r0, psp         \n"
    "   stmdb r0!, {r4-r11} \n"
    "   mov %0, r0          \n"
    : "=r" (old_psp)
    );
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    new_psp = thread_switch(old_psp);
    thread_preemption(preemption_period);
    asm(
    "   mov r0, %0          \n"
    "   ldmia r0!, {r4-r11} \n"
    "   msr psp, r0         \n"
    "   ldr lr, =0xfffffffd \n"
    "   bx lr               \n"
    :: "r" (new_psp)
    );
}
