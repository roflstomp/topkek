/* filename ******** thread.h **************
 *
 * RTOS threading functionality and interface
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 01/29/2015
*/

#ifndef __THREAD_H
#define __THREAD_H

#include <lib/types.h>
#include <lib/llist.h>
#include "sync.h"

#define SCHED_STRUCT_PQ     0
#define SCHED_STRUCT_LIST   1
#define SCHED_STRUCT    SCHED_STRUCT_LIST

#define SCHED_ALGO_RROBIN   0
#define SCHED_ALGO_PRI      1
#define SCHED_ALGO_PRI_DON  2
#define SCHED_ALGO      SCHED_ALGO_RROBIN

#define THREAD_MAGIC 0x3f0b897c

struct task {
    cb_t func;
    uint8_t priority;
    uint_t stacksize;

    enum thread_state {
        THR_RUNNING,
        THR_READY,
        THR_BLOCKED,
        THR_DEAD,
    } state : 4;

    uint_t *sp;
    struct _llist wait_elem;
    time_t last_run;
#if (SCHED_STRUCT == SCHED_STRUCT_LIST)
    struct _llist sched_elem;
#endif
    uint_t magic;    // Magic canary to catch stack overflows
    uint_t stack[];  // Actual stack follows struct
};

// Macro for defining tasks
// usage:
// struct task a = TASK_INIT(hello_func, 0x7f, 128);
#define TASK_INIT(func_name, pri, size) \
    {                                   \
        .func = func_name,              \
        .priority = pri,                \
        .stacksize = size,              \
                                        \
        .state = THR_DEAD,              \
        .magic = THREAD_MAGIC,          \
                                        \
        .stack = { [0 ... size] = 0 },  \
    }

// Macro for defining tasks
// usage:
//  TASK(hello, 0x7f, 128)(uint_t n) {
//      ...
//  }
// generates:
//  void hello_func(uint_t n);
//  struct task *hello_task;
#define TASK(name, pri, size)                   \
    void name##_func();                         \
                                                \
    struct task _##name##_task =                \
        TASK_INIT(name##_func, pri, size);      \
    struct task *name##_task = &_##name##_task; \
                                                \
    void name##_func


extern uint_t thread_count;
extern uint_t thread_max_count;

struct task *thread_current(void);
void thread_init(void);
void thread_preemption(time_t period);

// Used to save quantum
void thread_pause(void);
void thread_unpause(void);

// Accepts up to three additional arguments to pass to the task
void thread_spawn(struct task *task, ...);

void thread_yield(void);
void thread_block(struct sema *sema);
void thread_unblock(struct task *t);

void thread_exit(void);

less_func thread_order;

#endif // __THREAD_H
