/* filename ******** time.c **************
 *
 * System clock and voluntary scheduler
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#include "time.h"

#include "thread.h"
#include <os/sync.h>
#include <lib/mem.h>
#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <inc/hw_ints.h>
#include <inc/hw_timer.h>
#include <inc/hw_nvic.h>
#include <driverlib/debug.h>
#include <driverlib/sysctl.h>
#include <driverlib/systick.h>
#include <driverlib/timer.h>
#include <driverlib/interrupt.h>
#include <driverlib/gpio.h>


// jitter tracking
uint_t time_avg_jitter;
uint_t time_max_jitter;

// task definition
struct schedule {
    struct schedule *next;
    time_t target;
    time_t period; // zero for single shots

    struct task *task;
} *time_pending;

// timing information
static long usticks;


// Initializes system timer
void time_init(void) {
    // Reset jitter
    time_avg_jitter = 0;
    time_max_jitter = 0;

    // Initialize linked list
    time_pending = 0;

    // 1 US in cycles
    usticks = SysCtlClockGet() / 1000000;

    // WTimer 5 is used as a global clock
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER5);
    TimerConfigure(WTIMER5_BASE, TIMER_CFG_SPLIT_PAIR |
                                 TIMER_CFG_A_PERIODIC |
                                 TIMER_CFG_B_PERIODIC);
    TimerPrescaleSet(WTIMER5_BASE, TIMER_A, usticks-1);
    TimerControlStall(WTIMER5_BASE, TIMER_A, true);
    TimerEnable(WTIMER5_BASE, TIMER_A);

    // Systick is used (unconventionally) as a one shot
    // to trigger pending tasks
    IntEnable(FAULT_SYSTICK);
}

static void time_update_stats(int_t jitter) {
    if (jitter < 0)
        jitter = -jitter;

    if (jitter > time_max_jitter)
        time_max_jitter = jitter;

    time_avg_jitter = ((TIME_AVG_WEIGHT-1)*time_avg_jitter +
                       jitter) / TIME_AVG_WEIGHT;
}

// obtains the current time
time_t time_current(void) {
    return -*(volatile time_t *)(WTIMER5_BASE + TIMER_O_TAR);
}

// basic time wait function
void time_wait(time_t us) {
    time_t target = time_current() + us;

    while (time_current() < target)
        thread_yield();
}


// Setup timer for the next task and enable systick
static void systick_enable(void) {
    if (time_pending) {
        time_t target = time_pending->target - time_current();

        if ((signed)target < 0) {
            target = 2;
        } else {
            target *= usticks;

            if (target > 0x1000000)
                target = 0x1000000;
        }

        // period is off by one
        *(volatile time_t *)NVIC_ST_RELOAD = target - 1;
        *(volatile time_t *)NVIC_ST_CURRENT = 0;
        *(volatile time_t *)NVIC_ST_CTRL = NVIC_ST_CTRL_CLK_SRC |
                                           NVIC_ST_CTRL_INTEN   |
                                           NVIC_ST_CTRL_ENABLE;
    }
}

// Disable systick quickly
static void systick_disable(void) {
    *(volatile time_t *)NVIC_ST_CTRL = NVIC_ST_CTRL_CLK_SRC;
}

// Schedules a schedule
static void time_schedule(time_t us, struct schedule *schedule) {
    schedule->target = time_current() + us;

    struct schedule **next = &time_pending;
    while (*next && (*next)->target <= schedule->target)
        next = &(*next)->next;

    schedule->next = *next;
    *next = schedule;
}

// Timer handler manage enters the next task
// and manages awaiting tasks
void systick_isr(void) {
    systick_disable();

    while (time_pending) {
        int_t diff = (int_t)(time_pending->target - time_current());

        if (diff > 0)
            break;

        struct schedule *current = time_pending;
        time_pending = time_pending->next;

        thread_spawn(current->task, diff);
        time_update_stats(diff);

        if (current->period)
            time_schedule(current->period, current);
        else
            chunk_dealloc(current);
    }

    systick_enable();
}

// Schedules a callback function to be called
void time_call_in(time_t us, struct task *task) {
    uint_t gl = gl_acquire();
    struct schedule *schedule = chunk_alloc();

    schedule->task = task;
    schedule->period = 0;

    systick_disable();
    time_schedule(us, schedule);
    systick_enable();
    gl_release(gl);
}

// Schedules a callback to be called repeatedly
void time_call_every(time_t us, struct task *task) {
    uint_t gl = gl_acquire();
    struct schedule *schedule = chunk_alloc();

    schedule->task = task;
    schedule->period = us;

    systick_disable();
    time_schedule(us, schedule);
    systick_enable();
    gl_release(gl);
}
