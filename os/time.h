/* filename ******** time.h **************
 *
 * System clock and voluntary scheduler
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#include <lib/types.h>
#include <os/thread.h>

// How slowly to adjust repeating calls based on error
#define TIME_WEIGHT 10
// Extra space to allocate
#define TIME_HANDLER_STACK 16
// Weight for jitter average
#define TIME_AVG_WEIGHT 100

extern uint_t time_avg_jitter;
extern uint_t time_max_jitter;

// Initializes system time
void time_init(void);

// Obtains the current time in microseconds
time_t time_current(void);

// Waits for a given time using the scheduler
void time_wait(time_t us);

// Schedules a callback to be called once
void time_call_in(time_t us, struct task *thread);

// Schedules a callback to be called repeatedly
// Attempts to compensate for jitter using simple IIR filter
// This is controlled by the TIME_WEIGHT define
void time_call_every(time_t us, struct task *thread);
