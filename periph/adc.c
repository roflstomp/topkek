/* filename ******** adc.c **************
 *
 * Analog conversion
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#include "adc.h"

#include <os/sync.h>
#include <inc/hw_ints.h>
#include <inc/hw_memmap.h>
#include <driverlib/sysctl.h>
#include <driverlib/interrupt.h>
#include <driverlib/adc.h>
#include <driverlib/gpio.h>


// Definition of ADC Pin-map for the TM4C1233H6PM / LM4F120H5QR
// ADC#  | Pin Assignement
// ------+-----------------
// AIN0  |  PIN_E3
// AIN1  |  PIN_E2
// AIN2  |  PIN_E1
// AIN3  |  PIN_E0
// AIN4  |  PIN_D3
// AIN5  |  PIN_D2
// AIN6  |  PIN_D1
// AIN7  |  PIN_D0
// AIN8  |  PIN_E5
// AIN9  |  PIN_E4
// AIN10 |  PIN_B4
// AIN11 |  PIN_B5

// Resolution is a property of the microcontroller
// and is 12 bits for the LM4F

// The number of ADC modules also varies per microcontroller,
// this program takes care to spread the created ADCs accross
// the available modules, however special care must be take
// to make sure the ADC / Module ratio does not exceed 8
// which is the maximum ADCs the first sequence can hold
#define ADC_MODULE_COUNT 2


// Internally used struct representing
// a single ADC module
struct adc_module {
    // Constant StellarisWare values
    // specific to each module
    const uint_t BASE;
    const uint_t PERIPH;
    const uint_t INT;

    // The ADCs being read are stored in this linked list
    struct adc *pending;
    struct adc **pending_end;

    // A flag for initialization
    bool initialized;
};

// Count of current adc structures
static int adc_count = 0;

// Array of available modules
static struct adc_module adc_modules[ADC_MODULE_COUNT] = {
    {ADC0_BASE, SYSCTL_PERIPH_ADC0, INT_ADC0SS0},
    {ADC1_BASE, SYSCTL_PERIPH_ADC1, INT_ADC1SS0},
};


// Function to initialize an entire module. Checks
// if module has already been intialized. Does not
// actually start any sequences
static void adc_module_init(struct adc_module *mod) {
    // Check if we have already initialized
    if (mod->initialized)
        return;

    // Reset the queue appropriately
    mod->pending = 0;
    mod->pending_end = &mod->pending;

    // Enable clocking for ADC
    SysCtlPeripheralEnable(mod->PERIPH);

    // Enable hardware oversampling (higher precision results)
    ADCHardwareOversampleConfigure(mod->BASE, ADC_OVERSAMPLING_FACTOR);

    // Enable the ADC interrupt
    IntEnable(mod->INT);
    ADCIntEnable(mod->BASE, 0);
    ADCIntEnable(mod->BASE, 1);

    mod->initialized = true;
}

// pin mappings
static uint_t adc_channel(pin_t pin);
static struct adc_module *adc_active_mod(void);


// Function to initialize an ADC on a pin, if ADC
// is not supported in hardware on the given pin,
// then a null pointer is returned
// The returned pointer can be used by the ADCRead functions
void adc_init(struct adc *adc, pin_t pin) {
    // assert channel is valid
    adc_channel(pin);

    // assign it to an adc module
    adc->module = &adc_modules[adc_count++ % ADC_MODULE_COUNT];

    // Check for module initialization
    adc_module_init(adc->module);

    // Setup initial data
    adc->pin = pin;

    SysCtlPeripheralEnable(gpio_periph(pin));
    GPIOPinTypeADC(gpio_port(pin), gpio_pin(pin));
}

// Internally used function to read the next pending single adc.
static void adc_trigger(struct adc_module *mod) {
    struct adc *adc = mod->pending;

    // Create the sequence from scratch with moderate priority
    ADCSequenceConfigure(mod->BASE, 0, ADC_TRIGGER_PROCESSOR, 1);

    // Create the sequence step, which there is only one of
    // with the next adc's pin.
    ADCSequenceStepConfigure(mod->BASE, 0, 0,
        ADC_CTL_IE | ADC_CTL_END | adc_channel(adc->pin));

    // Enable the sequence and trigger it
    ADCSequenceEnable(mod->BASE, 0);
    ADCProcessorTrigger(mod->BASE, 0);
}

// Interrupt handlers for adc reading
void adc_isr(void) {
    struct adc_module *mod = adc_active_mod();
    struct adc *adc = mod->pending;

    ADCIntClear(mod->BASE, 0);
    ADCSequenceDataGet(mod->BASE, 0, &adc->value);
    ADCSequenceDisable(mod->BASE, 0);

    mod->pending = adc->next;

    if (mod->pending)
        adc_trigger(mod);
    else
        mod->pending_end = &mod->pending;

    if (adc->task)
        thread_spawn(adc->task, (word_t)(adc->value / (float)ADC_MAX));

    adc->pending = false;
}


// This function sets up an ADC to be run in the background
void adc_call_on_read(struct adc *adc, struct task *task) {
    uint_t gl = gl_acquire();
    struct adc_module *mod = adc->module;

    // Fail here since we can't register multiple threads
    // and this is most likely a user issue
    assert(!adc->pending);

    // Setup the task
    adc->task = task;
    adc->pending = true;

    // Add us to the queue
    adc->next = 0;
    *mod->pending_end = adc;
    mod->pending_end = &adc->next;

    // If nothing was being read, we need to trigger the module
    if (mod->pending == adc)
        adc_trigger(mod);

    gl_release(gl);
}


// This function returns the value measured as a percentage
// in a busy waiting fashion
float adc_read(struct adc *adc) {
    adc_call_on_read(adc, 0);

    while (adc->pending) thread_yield();

    return adc->value / (float)ADC_MAX;
}


// pin mappings
static uint_t adc_channel(pin_t pin) {
    switch ((uint_t)pin) {
        case (uint_t)PIN_B4: return ADC_CTL_CH10;
        case (uint_t)PIN_B5: return ADC_CTL_CH11;
        case (uint_t)PIN_D0: return ADC_CTL_CH7;
        case (uint_t)PIN_D1: return ADC_CTL_CH6;
        case (uint_t)PIN_D2: return ADC_CTL_CH5;
        case (uint_t)PIN_D3: return ADC_CTL_CH4;
        case (uint_t)PIN_E0: return ADC_CTL_CH3;
        case (uint_t)PIN_E1: return ADC_CTL_CH2;
        case (uint_t)PIN_E2: return ADC_CTL_CH1;
        case (uint_t)PIN_E3: return ADC_CTL_CH0;
        case (uint_t)PIN_E4: return ADC_CTL_CH9;
        case (uint_t)PIN_E5: return ADC_CTL_CH8;
        default: unreachable();
    }
}

static struct adc_module *adc_active_mod(void) {
    switch (get_psr() & 0xff) {
        case INT_ADC0SS0: return &adc_modules[0];
        case INT_ADC1SS0: return &adc_modules[1];
        default: unreachable();
    }
}
