/* filename ******** time.h **************
 *
 * System clock and voluntary scheduler
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#ifndef __ADC_H
#define __ADC_H

#include <os/time.h>
#include <os/thread.h>
#include <lib/common.h>
#include <lib/types.h>
#include <periph/gpio.h>


// Constant for ADC oversampling
// Can be 0, 2, 4, 8, 16, 32, 64
// Doubling the oversample doubles to time to read ADC
#define ADC_OVERSAMPLING_FACTOR 0

// Range and resolution of ADC values
#define ADC_RESOLUTION 12
#define ADC_MAX ((1<<ADC_RESOLUTION) - 1)


// Struct representating an ADC pin
struct adc {
    struct adc_module *module; // Pointer to the module its using
    struct adc *next; // The next ADC in linked lists

    pin_t pin;
    struct task *task;

    uint_t value;
    volatile bool pending;
};


// ADC is only supported on a limited number of pins in hardware
// The following pins are supported:
// PIN_B4 PIN_B5
// PIN_D0 PIN_D1 PIN_D2 PIN_D3
// PIN_E0 PIN_E1 PIN_E2 PIN_E3 PIN_E4 PIN_E5


// Initializes an ADC on a pin
void adc_init(struct adc *adc, pin_t pin);

// Returns the voltage as a % of 3.3V
float adc_read(struct adc *adc);

// Sets up an ADC to be run in the background
void adc_call_on_read(struct adc *adc, struct task *task);


#endif // __ADC_H
