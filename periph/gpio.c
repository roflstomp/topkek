/* filename ******** gpio.h **************
 *
 * General Purpose Input and Output
 * Honestly how fancily can they say wires?
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#include "gpio.h"
#include <lib/common.h>
#include <inc/hw_gpio.h>
#include <inc/hw_ints.h>
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>
#include <driverlib/interrupt.h>


// Predefined static lookup tables
static uint_t gpio_active_port(void);
static uint_t gpio_int(pin_t pin);


// Initializes pin as either input or output
static void gpio_enable(pin_t pin) {
    SysCtlPeripheralEnable(gpio_periph(pin));

    // Special cases for NMIs
    if (pin == PIN_F0) {
        HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
        HWREG(GPIO_PORTF_BASE + GPIO_O_CR) = GPIO_PIN_0;
        HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;
    } else if (pin == PIN_D7) {
        HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
        HWREG(GPIO_PORTD_BASE + GPIO_O_CR) = GPIO_PIN_7;
        HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = 0;
    }
}

void gpio_input(pin_t pin) {
    gpio_enable(pin);
    GPIOPinTypeGPIOInput(gpio_port(pin), gpio_pin(pin));
}

void gpio_output(pin_t pin) {
    gpio_enable(pin);
    GPIOPinTypeGPIOOutput(gpio_port(pin), gpio_pin(pin));
}

// Add pull ups or pull downs to pins
void gpio_pull_up(pin_t pin) {
    GPIOPadConfigSet(gpio_port(pin), gpio_pin(pin),
                     GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}

void gpio_pull_down(pin_t pin) {
    GPIOPadConfigSet(gpio_port(pin), gpio_pin(pin),
                     GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
}


// Pin tasks for executing on interrupts are stored
// in a simple table
struct event_handler {
    cb_t cb;
    void *arg;
};

struct event_handler gpio_handlers[PIN_COUNT];

static struct event_handler *gpio_get_handler(pin_t pin) {
    return &gpio_handlers[gpio_number(pin)];
}

static void gpio_set_handler(pin_t pin, cb_t cb, void *arg) {
    struct event_handler *hndlr = &gpio_handlers[gpio_number(pin)];
    hndlr->cb = cb;
    hndlr->arg = arg;
}

void gpio_isr(void) {
    uint_t port = gpio_active_port();
    uint_t status = GPIOIntStatus(port, true);

    for (uint_t pin = 0x01; pin < 0x100; pin <<= 1) {
        if (status & pin) {
            struct event_handler *hndlr = gpio_get_handler(PIN(port, pin));
            hndlr->cb(hndlr->arg);
        }
    }

    GPIOIntClear(port, status);
}

void gpio_call_on(pin_t pin, void *handler, void *arg, uint_t type) {
    // disable interrupt to avoid any race conditions
    GPIOIntDisable(gpio_port(pin), gpio_pin(pin));

    uint_t flags = 0;

    if ((type & GPIO_CALL_RISING) && (type & GPIO_CALL_FALLING))
        flags = GPIO_BOTH_EDGES;
    else if (type & GPIO_CALL_RISING)
        flags = GPIO_RISING_EDGE;
    else if (type & GPIO_CALL_FALLING)
        flags = GPIO_FALLING_EDGE;
    else
        return;

    // Setup the interrupt
    gpio_set_handler(pin, handler, arg);
    GPIOIntTypeSet(gpio_port(pin), gpio_pin(pin), flags);
    GPIOIntEnable(gpio_port(pin), gpio_pin(pin));
    IntEnable(gpio_int(pin));
}

// Lookup tables and the like for obtaining pin information
uint_t gpio_port(pin_t pin) {
    return (uint_t)pin & ~0xfff;
}

uint_t gpio_pin(pin_t pin) {
    return ((uint_t)pin & 0xfff) >> 2;
}

uint_t gpio_number(pin_t pin) {
    uint_t number = 0;

    switch (gpio_port(pin)) {
        case GPIO_PORTA_BASE: number += 0*8; break;
        case GPIO_PORTB_BASE: number += 1*8; break;
        case GPIO_PORTC_BASE: number += 2*8; break;
        case GPIO_PORTD_BASE: number += 3*8; break;
        case GPIO_PORTE_BASE: number += 4*8; break;
        case GPIO_PORTF_BASE: number += 5*8; break;
        default: unreachable();
    }

    switch (gpio_pin(pin)) {
        case GPIO_PIN_0: number += 0; break;
        case GPIO_PIN_1: number += 1; break;
        case GPIO_PIN_2: number += 2; break;
        case GPIO_PIN_3: number += 3; break;
        case GPIO_PIN_4: number += 4; break;
        case GPIO_PIN_5: number += 5; break;
        case GPIO_PIN_6: number += 6; break;
        case GPIO_PIN_7: number += 7; break;
        default: unreachable();
    }

    return number;
}

static uint_t gpio_active_port(void) {
    switch (get_psr() & 0xff) {
        case INT_GPIOA: return GPIO_PORTA_BASE;
        case INT_GPIOB: return GPIO_PORTB_BASE;
        case INT_GPIOC: return GPIO_PORTC_BASE;
        case INT_GPIOD: return GPIO_PORTD_BASE;
        case INT_GPIOE: return GPIO_PORTE_BASE;
        case INT_GPIOF: return GPIO_PORTF_BASE;
        default: unreachable();
    }
}

static uint_t gpio_int(pin_t pin) {
    switch (gpio_port(pin)) {
        case GPIO_PORTA_BASE: return INT_GPIOA;
        case GPIO_PORTB_BASE: return INT_GPIOB;
        case GPIO_PORTC_BASE: return INT_GPIOC;
        case GPIO_PORTD_BASE: return INT_GPIOD;
        case GPIO_PORTE_BASE: return INT_GPIOE;
        case GPIO_PORTF_BASE: return INT_GPIOF;
        default: unreachable();
    }
}

uint_t gpio_periph(pin_t pin) {
    switch (gpio_port(pin)) {
        case GPIO_PORTA_BASE: return SYSCTL_PERIPH_GPIOA;
        case GPIO_PORTB_BASE: return SYSCTL_PERIPH_GPIOB;
        case GPIO_PORTC_BASE: return SYSCTL_PERIPH_GPIOC;
        case GPIO_PORTD_BASE: return SYSCTL_PERIPH_GPIOD;
        case GPIO_PORTE_BASE: return SYSCTL_PERIPH_GPIOE;
        case GPIO_PORTF_BASE: return SYSCTL_PERIPH_GPIOF;
        default: unreachable();
    }
}


