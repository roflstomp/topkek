/* filename ******** gpio.h **************
 *
 * General Purpose Input and Output
 * Honestly how fancily can they say wires?
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/5/2014
 */

#ifndef __GPIO_H
#define __GPIO_H

#include <lib/types.h>
#include <driverlib/gpio.h>
#include <inc/hw_memmap.h>
#include <os/thread.h>

// Pins are special in that they can be directly dereferenced
typedef volatile uint_t *pin_t;

// Macro for creating pin combinations
#define PIN(port, pins) (pin_t)((port) + ((pins) << 2))

// Initializes pin as either input or output
void gpio_input(pin_t pin);
void gpio_output(pin_t pin);

// Add pull ups or pull downs to pins
void gpio_pull_up(pin_t pin);
void gpio_pull_down(pin_t pin);

// Registering callbacks on pin state changes
void gpio_call_on(pin_t pin, void *handler, void *arg, uint_t flags);

// Obtains information for TI calls
uint_t gpio_periph(pin_t pin);
uint_t gpio_port(pin_t pin);
uint_t gpio_pin(pin_t pin);
uint_t gpio_number(pin_t pin);

#define GPIO_CALL_CANCEL    (0)
#define GPIO_CALL_RISING    (1 << 0)
#define GPIO_CALL_FALLING   (1 << 1)

// Follows is the definition of every pin available
#define PIN_A0 PIN(GPIO_PORTA_BASE, GPIO_PIN_0)
#define PIN_A1 PIN(GPIO_PORTA_BASE, GPIO_PIN_1)
#define PIN_A2 PIN(GPIO_PORTA_BASE, GPIO_PIN_2)
#define PIN_A3 PIN(GPIO_PORTA_BASE, GPIO_PIN_3)
#define PIN_A4 PIN(GPIO_PORTA_BASE, GPIO_PIN_4)
#define PIN_A5 PIN(GPIO_PORTA_BASE, GPIO_PIN_5)
#define PIN_A6 PIN(GPIO_PORTA_BASE, GPIO_PIN_6)
#define PIN_A7 PIN(GPIO_PORTA_BASE, GPIO_PIN_7)

#define PIN_B0 PIN(GPIO_PORTB_BASE, GPIO_PIN_0)
#define PIN_B1 PIN(GPIO_PORTB_BASE, GPIO_PIN_1)
#define PIN_B2 PIN(GPIO_PORTB_BASE, GPIO_PIN_2)
#define PIN_B3 PIN(GPIO_PORTB_BASE, GPIO_PIN_3)
#define PIN_B4 PIN(GPIO_PORTB_BASE, GPIO_PIN_4)
#define PIN_B5 PIN(GPIO_PORTB_BASE, GPIO_PIN_5)
#define PIN_B6 PIN(GPIO_PORTB_BASE, GPIO_PIN_6)
#define PIN_B7 PIN(GPIO_PORTB_BASE, GPIO_PIN_7)

#define PIN_C0 PIN(GPIO_PORTC_BASE, GPIO_PIN_0)
#define PIN_C1 PIN(GPIO_PORTC_BASE, GPIO_PIN_1)
#define PIN_C2 PIN(GPIO_PORTC_BASE, GPIO_PIN_2)
#define PIN_C3 PIN(GPIO_PORTC_BASE, GPIO_PIN_3)
#define PIN_C4 PIN(GPIO_PORTC_BASE, GPIO_PIN_4)
#define PIN_C5 PIN(GPIO_PORTC_BASE, GPIO_PIN_5)
#define PIN_C6 PIN(GPIO_PORTC_BASE, GPIO_PIN_6)
#define PIN_C7 PIN(GPIO_PORTC_BASE, GPIO_PIN_7)

#define PIN_D0 PIN(GPIO_PORTD_BASE, GPIO_PIN_0)
#define PIN_D1 PIN(GPIO_PORTD_BASE, GPIO_PIN_1)
#define PIN_D2 PIN(GPIO_PORTD_BASE, GPIO_PIN_2)
#define PIN_D3 PIN(GPIO_PORTD_BASE, GPIO_PIN_3)
#define PIN_D4 PIN(GPIO_PORTD_BASE, GPIO_PIN_4)
#define PIN_D5 PIN(GPIO_PORTD_BASE, GPIO_PIN_5)
#define PIN_D6 PIN(GPIO_PORTD_BASE, GPIO_PIN_6)
#define PIN_D7 PIN(GPIO_PORTD_BASE, GPIO_PIN_7)

#define PIN_E0 PIN(GPIO_PORTE_BASE, GPIO_PIN_0)
#define PIN_E1 PIN(GPIO_PORTE_BASE, GPIO_PIN_1)
#define PIN_E2 PIN(GPIO_PORTE_BASE, GPIO_PIN_2)
#define PIN_E3 PIN(GPIO_PORTE_BASE, GPIO_PIN_3)
#define PIN_E4 PIN(GPIO_PORTE_BASE, GPIO_PIN_4)
#define PIN_E5 PIN(GPIO_PORTE_BASE, GPIO_PIN_5)
#define PIN_E6 PIN(GPIO_PORTE_BASE, GPIO_PIN_6)
#define PIN_E7 PIN(GPIO_PORTE_BASE, GPIO_PIN_7)

#define PIN_F0 PIN(GPIO_PORTF_BASE, GPIO_PIN_0)
#define PIN_F1 PIN(GPIO_PORTF_BASE, GPIO_PIN_1)
#define PIN_F2 PIN(GPIO_PORTF_BASE, GPIO_PIN_2)
#define PIN_F3 PIN(GPIO_PORTF_BASE, GPIO_PIN_3)
#define PIN_F4 PIN(GPIO_PORTF_BASE, GPIO_PIN_4)
#define PIN_F5 PIN(GPIO_PORTF_BASE, GPIO_PIN_5)
#define PIN_F6 PIN(GPIO_PORTF_BASE, GPIO_PIN_6)
#define PIN_F7 PIN(GPIO_PORTF_BASE, GPIO_PIN_7)

#define PIN_COUNT (6*8)

#endif // __GPIO_H
