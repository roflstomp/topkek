//*****************************************************************************
//
// sonar - Software Sonar driver
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE AUTHORS OF THIS FILE
// SHALL NOT, UNDER ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
// OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of RASLib Rev0 of the RASWare2013 package.
//
// Written by: 
// The student branch of the 
// IEEE - Robotics and Automation Society 
// at the University of Texas at Austin
//
// Website: ras.ece.utexas.edu
// Contact: ut.ieee.ras@gmail.com
//
//*****************************************************************************

#include "sonar.h"
#include <os/time.h>

#include <math.h>

void sonar_init(struct sonar *snr, pin_t trigger, pin_t echo) {
    gpio_output(trigger);
    gpio_input(echo);
    *trigger = 0x0;
    snr->state = SONAR_ST_READY;
    snr->trigger = trigger;
    snr->echo = echo;
    snr->start = time_current() - SONAR_DELAY;
    snr->value = INFINITY;
}

void sonar_recv(struct sonar *snr) {
    uint_t gl = gl_acquire();
    switch (snr->state) {
    case SONAR_ST_WAIT:
        if (*snr->echo) {
            snr->start = time_current();
            snr->state = SONAR_ST_TIMING;
        }
        break;
    case SONAR_ST_TIMING:
        if (!*snr->echo) {
            snr->value = (time_current() - snr->start) / (float)SONAR_MAX;
            snr->state = SONAR_ST_CALLBACK;
            thread_spawn(snr->task, snr->data);
        } else {
            snr->state = SONAR_ST_CALLBACK;
            thread_spawn(snr->task, snr->data);
        }
        break;
    default:
        break;
    }
    gl_release(gl);
}

void sonar_sequence(struct sonar *snr, struct task *task, void *data) {
    assert(snr->state == SONAR_ST_READY);
    snr->task = task;
    snr->data = data;
    // Exhaust the delay
    time_wait(SONAR_DELAY - (time_current() - snr->start));

    gpio_call_on(snr->echo, sonar_recv, snr,
            GPIO_CALL_FALLING | GPIO_CALL_RISING);
    // Raise the trigger pin to start the sonar
    snr->state = SONAR_ST_PULSE;
    *snr->trigger = 0xff;
    time_wait(SONAR_PULSE);
    snr->state = SONAR_ST_WAIT;
    *snr->trigger = 0;
}

float sonar_read(struct sonar *snr) {
    assert(snr->state == SONAR_ST_CALLBACK);
    float val = snr->value;
    snr->state = SONAR_ST_READY;
    return val;
}

