//*****************************************************************************
//
// sonar - Software Sonar driver
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE AUTHORS OF THIS FILE
// SHALL NOT, UNDER ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
// OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of RASLib Rev0 of the RASWare2013 package.
//
// Written by: 
// The student branch of the 
// IEEE - Robotics and Automation Society 
// at the University of Texas at Austin
//
// Website: ras.ece.utexas.edu
// Contact: ut.ieee.ras@gmail.com
//
//*****************************************************************************

#ifndef _R_SONAR_H_
#define _R_SONAR_H_

#include "gpio.h"
#include <os/thread.h>

#ifdef __cplusplus
extern "C" {
#endif


// Constants used for sonar timing
// Each is given in units of microseconds
#define SONAR_TIMEOUT 36000
#define SONAR_MAX     18000
#define SONAR_DELAY   10000
#define SONAR_PULSE   10

// Definition of struct sonar
struct sonar {
    // The last read sonar value
    float value;
    // The time when the return pulse started
    time_t start;

    // Callback information
    struct task *task;
    void *data;

    // Which pins the sonar is plugged into
    pin_t trigger;
    pin_t echo;

    // State machine for the sonar
    volatile enum {
        SONAR_ST_READY,
        SONAR_ST_PULSE,
        SONAR_ST_WAIT,
        SONAR_ST_TIMING,
        SONAR_ST_CALLBACK,
    } state : 4;
};

void sonar_recv(struct sonar *snr);

/**
 * Initializes a sonar on a pair of pin
 * @param trigger Pin plugged into the sonar's tigger line
 * @param echo Pin plugged into the sonar's echo line
 * @return Pointer to an initialized struct sonar, can be used by the
 * sonar_read function
 */
void sonar_init(struct sonar *snr, pin_t trigger, pin_t echo);

/** Sets up an sonar to be run in the background
 * @param snr Pointer to an initialized struct sonar
 * @param task Task to be scheduled the next time the sonar read completes, in
 * which a call to sonar_read will return with the newly obtained value
 * immediately
 * @param data Argument sent to the provided callback function whenever it is
 * called
 */
void sonar_sequence(struct sonar *snr, struct task *task, void *data);

/**
 * Returns the distance measured from the sonar
 * @param snr Pointer to an initialized struct sonar
 * @return The distance measured as a percentage of maximum range of the sonar.
 * If no response is detected, a value of infinity is returned.  Note: if the
 * sonar is not continously reading, then the function will busy wait for the
 * results
 */
float sonar_read(struct sonar *snr);


#ifdef __cplusplus
}
#endif

#endif // _R_SONAR_H_
