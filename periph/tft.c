/* filename ******** tft.c **************
 *
 * Driver for the TFT_320QVT
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include "tft.h"
#include <os/time.h>
#include <os/sync.h>
#include "lib/common.h"

#include <driverlib/ssi.h>
#include <driverlib/gpio.h>
#include <driverlib/sysctl.h>
#include <inc/hw_types.h>
#include <inc/hw_gpio.h>
#include <inc/hw_memmap.h>


static void tft_data(uint16_t data) {
    *TFT_DLL = data >> TFT_DLL_S; // Setup data
    *TFT_DLH = data >> TFT_DLH_S;
    *TFT_DHL = data >> TFT_DHL_S;
    *TFT_DHH = data >> TFT_DHH_S;

    *TFT_WR = 0x00; // Clock out
    *TFT_WR = 0xff;
}

static void tft_addr(uint8_t data) {
    *TFT_RS = 0x00; // Indicate register selection

    *TFT_DLL = data; // Setup the data
    *TFT_DLH = data;
    *TFT_DHL = 0;
    *TFT_DHH = 0;

    *TFT_WR = 0x00; // Clock out
    *TFT_DLL = data; // Setup the data
    *TFT_DLH = data;
    *TFT_DHL = 0;
    *TFT_DHH = 0;
    *TFT_WR = 0xff;

    *TFT_RS = 0xff; // Leave in data mode
}

static void tft_cmd(uint8_t reg, uint16_t data) {
    tft_addr(reg);
    tft_data(data);
}

void tft_init(void) {
    gpio_output(TFT_WR); // Setup pin directions
    gpio_output(TFT_RS);
    gpio_output(TFT_DLL);
    gpio_output(TFT_DLH);
    gpio_output(TFT_DHL);
    gpio_output(TFT_DHH);
    gpio_output(TFT_RESET);

    *TFT_WR = 0xff; // These all use negative logic
    *TFT_RS = 0xff;
    *TFT_RESET = 0xff;

    *TFT_RESET = 0x00; // Reset the screen
    *TFT_RESET = 0xff;

    tft_cmd(0x00, 0x0001); // Enable clock

    tft_cmd(0x03, 0xa8a4); // Setup power stuff
    tft_cmd(0x0c, 0x0000); //
    tft_cmd(0x0d, 0x080c); //
    tft_cmd(0x0e, 0x2b00); //
    tft_cmd(0x1e, 0x00b7); //

    tft_cmd(0x01, 0x6b3f); // Setup output
    tft_cmd(0x02, 0x0600); //

    tft_cmd(0x10, 0x0000); // Exit sleep mode

    tft_cmd(0x11, 0x6070); // Setup entry mode

    tft_cmd(0x05, 0x0000); // Clear compare register
    tft_cmd(0x06, 0x0000); //

    tft_cmd(0x16, 0xef1c); // Setup H/V porches
    tft_cmd(0x17, 0x0003); //

    tft_cmd(0x07, 0x0233); // Setup display control
    tft_cmd(0x0b, 0x0000); // Setup frame control
    tft_cmd(0x0f, 0x0000); // Setup gate scan
    tft_cmd(0x41, 0x0000); // Setup vertical scrolling
    tft_cmd(0x42, 0x0000); //
    tft_cmd(0x48, 0x0000); // Setup driving position
    tft_cmd(0x49, 0x013f); //
    tft_cmd(0x4a, 0x0000); //
    tft_cmd(0x4b, 0x0000); //

    tft_cmd(0x30, 0x0707); // Setup gamma adjustments
    tft_cmd(0x31, 0x0204); //
    tft_cmd(0x32, 0x0204); //
    tft_cmd(0x33, 0x0502); //
    tft_cmd(0x34, 0x0507); //
    tft_cmd(0x35, 0x0204); //
    tft_cmd(0x36, 0x0204); //
    tft_cmd(0x37, 0x0502); //
    tft_cmd(0x3a, 0x0302); //
    tft_cmd(0x3b, 0x0302); //

    tft_cmd(0x23, 0x0000); // Clear data masks
    tft_cmd(0x24, 0x0000); //

    tft_cmd(0x25, 0x8000); // Set frame frequency

    tft_clear_rect(); // Clear the rectangle
    tft_clear(); // Clear the display

    tft_addr(0x22); // Leave in color mode
}


void tft_set_address(uint16_t x, uint16_t y) {
    tft_cmd(0x4f, 0x01ff & x);  // Set x and y
    tft_cmd(0x4e, 0x00ff & y);
    tft_addr(0x22); // Ready to enter color
}

void tft_set_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
    x &= 0x01ff;
    y &= 0x00ff;
    w = 0x01ff & (x+w-1);
    h = 0x00ff & (y+h-1);

    tft_cmd(0x45, x); // Set address bounds
    tft_cmd(0x46, w);
    tft_cmd(0x44, y | (h << 8));

    tft_cmd(0x4f, x);  // Set x and y
    tft_cmd(0x4e, y);

    tft_addr(0x22); // Ready to enter color
}

void tft_clear_rect(void) {
    tft_cmd(0x45, 0x0000); // Set default bounds
    tft_cmd(0x46, (TFT_WIDTH-1));
    tft_cmd(0x44, (TFT_HEIGHT-1) << 8);

    tft_addr(0x22); // Ready to enter color
}

void tft_set_color(uint16_t rgb) {
    tft_data(rgb); // Assume we were in color mode
}

void tft_set_pixel(uint16_t x, uint16_t y, uint16_t rgb) {
    tft_cmd(0x4f, 0x01ff & x);  // Set x and y
    tft_cmd(0x4e, 0x00ff & y);
    tft_cmd(0x22, rgb);
}

void tft_clear(void) {
    int i;

    tft_clear_rect();
    tft_set_address(0, 0);

    for (i=0; i < TFT_HEIGHT*TFT_WIDTH; i++) {
        tft_set_color(0);
    }

    tft_set_address(0, 0);
}
