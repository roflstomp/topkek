/* filename ******** tft.h **************
 *
 * Driver for the TFT_320QVT
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/19/2014
*/

#include <lib/types.h>
#include "gpio.h"

#define TFT_HEIGHT 240
#define TFT_WIDTH 320

#define TFT_WR PIN(GPIO_PORTA_BASE, 0x04)
#define TFT_RS PIN(GPIO_PORTA_BASE, 0x08)

#define TFT_DLL PIN(GPIO_PORTD_BASE, 0x0f)
#define TFT_DLL_S 0
#define TFT_DLH PIN(GPIO_PORTC_BASE, 0xf0)
#define TFT_DLH_S 0
#define TFT_DHL PIN(GPIO_PORTE_BASE, 0x0f)
#define TFT_DHL_S 8
#define TFT_DHH PIN(GPIO_PORTA_BASE, 0xf0)
#define TFT_DHH_S 8

#define TFT_RESET PIN(GPIO_PORTD_BASE, 0x40)


void tft_init(void);

void tft_clear(void);

void tft_set_address(uint16_t x, uint16_t y);

// [rrrrrggggggbbbbb] 5x6x5
void tft_set_color(uint16_t rgb);
void tft_set_pixel(uint16_t x, uint16_t y, uint16_t rgb);

void tft_set_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
void tft_clear_rect(void);

