/* filename ******** test.c **************
 *
 * Initialize and run tests
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 03/04/2015
*/

#include "test.h"

#include <string.h>
#include <stdarg.h>

#include <io/io.h>
#include <io/tftio.h>
#include <gui/screen.h>

#include <lib/mem.h>

#include <os/time.h>
#include <os/thread.h>

#include <driverlib/sysctl.h>


module_func mem_test;
module_func fsm_test;
module_func llist_test;
module_func vector_test;
module_func pqueue_test;
module_func thread_test;
module_func sync_test;

bool first_module = true;
bool module_first_case = true;

void no_prefix(va_list UNUSED) {
}

void no_suffix(va_list UNUSED) {
}

void test_module(const char *module_name, module_func func) {
    printk("%s\"%s\": {", (first_module) ? "" : ",", module_name);
    module_first_case = true;
    func();
    printk("}");
    first_module = false;
}

void testcase(const char *casename, bool (func)(va_list),
        void prefix(va_list), void suffix(va_list), ...) {
    va_list args;
    va_start(args, suffix);
    uint_t mem_before = mem_used;
    printk("%s\"%s\": {", (module_first_case) ? "" : ",", casename);

    prefix(args);
    time_t start = time_current();
    bool pass = func(args);
    time_t end = time_current();
    suffix(args);
    bool leak = mem_used != mem_before;

    printk("\"pass\": %s,", (pass) ? "true" : "false");
    printk("\"leak\": %s,", (leak) ? "true" : "false");
    printk("\"time\": %f", ((double)(end - start)) / 1000000);
    printk("}");
    va_end(args);
    module_first_case = false;
}

int main(void) {
    mem_init();
    time_init();
    console_init();

    printk("{");

    test_module("lib/mem", mem_test);
    test_module("lib/fsm", fsm_test);
    test_module("lib/llist", llist_test);
    test_module("lib/vector", vector_test);
    test_module("lib/pqueue", pqueue_test);

    thread_init();
    test_module("os/thread", thread_test);
    test_module("os/sync", sync_test);

    printk("}\n");
    debug_halt();
}

