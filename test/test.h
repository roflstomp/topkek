
#include <stdarg.h>

#include <lib/types.h>
#include <lib/common.h>
#include <lib/mem.h>

#include <io/console.h>
#define printk console_printf

typedef void (module_func)(void);
typedef bool (testcase_func)(va_list args);

#define test_assert(x) if (!(x)) { return false; }

void testcase(const char *casename, bool (func)(va_list),
        void prefix(va_list), void suffix(va_list), ...);
void no_prefix(va_list);
void no_suffix(va_list);

#define ARRAY_SIZE 1024
extern const uint32_t unsorted[ARRAY_SIZE];

