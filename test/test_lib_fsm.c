/* filename ******** test_lib_fsm.c **************
 *
 * Test code for finite state machine.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/06/2014
*/

#include "test.h"

#define FSM_SIZE 2
#define FSM_OUT_T int
#include <lib/fsm.h>

struct state states[3] = {
    {0, {&states[1], &states[2]}},
    {1, {&states[0], &states[2]}},
    {2, {&states[0], &states[1]}}
};

bool fsm_test(void) {
    struct state* cur = &states[0];
    assert(*fsm_step(&cur, 0) == 1);
    assert(*fsm_step(&cur, 1) == 2);
    assert(*fsm_step(&cur, 0) == 0);
    assert(cur == &states[0]);
    return (cur == &states[0]);
}

