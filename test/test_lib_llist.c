/* filename ******** test_lib_llist.c **************
 *
 * Test code for linked list.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/06/2014
*/

#include "test.h"
#include <lib/types.h>
#include <lib/common.h>
#include <lib/llist.h>

struct test_struct { int data; struct _llist list; };

struct llist test_list;
struct test_struct test_elements[10];

bool list_less(void *a, void *b) {
    return llist_get(struct test_struct, (struct _llist *)a, list)->data <
        llist_get(struct test_struct, (struct _llist *)b, list)->data;
}

// Testing against NULL pointer
bool llist_test_null (va_list UNUSED) {
    llist_init(NULL);
    return !llist_append(NULL, &test_elements[0].list);
}

// Testing llist_append function
bool llist_test_append (va_list UNUSED) {
    llist_init(&test_list);

    test_assert(!llist_append(&test_list, NULL));
    test_assert(llist_append(&test_list, &test_elements[0].list));
    test_assert(llist_append(&test_list, &test_elements[1].list));
    test_assert(!llist_append(&test_list, NULL));
    test_assert(llist_append(&test_list, &test_elements[2].list));

    int i;
    struct _llist *e;
    for (i = 0, e = llist_iter_start(&test_list);
            e != llist_iter_end(&test_list);
            i++, e = llist_iter_next(e)) {
        test_assert(llist_get(struct test_struct, e, list)->data == i);
    }
    test_assert(i == 3);
    return true;
}

// Testing llist_remove function
bool llist_test_remove(va_list UNUSED) {
    int before[] = {0, 2};
    int after[] = {2, 3, 6};
    int i;
    struct _llist *e;
    llist_remove(&test_elements[1].list);
    for (i = 0, e = llist_iter_start(&test_list);
            e != llist_iter_end(&test_list);
            i++, e = llist_iter_next(e)) {
        int dat = llist_get(struct test_struct, e, list)->data;
        test_assert(dat == before[i]);
    }
    test_assert(i == sizeof(before)/sizeof(int));

    test_assert(llist_append(&test_list, &test_elements[3].list));
    test_assert(llist_append(&test_list, &test_elements[4].list));
    test_assert(llist_remove(&test_elements[4].list));
    test_assert(llist_remove(&test_elements[0].list));
    test_assert(llist_append(&test_list, &test_elements[5].list));
    test_assert(!llist_remove(&test_elements[6].list));
    test_assert(llist_append(&test_list, &test_elements[6].list));
    test_assert(llist_remove(&test_elements[5].list));
    test_assert(!llist_remove(&test_elements[0].list));

    for (i = 0, e = llist_iter_start(&test_list);
            e != llist_iter_end(&test_list);
            i++, e = llist_iter_next(e)) {
        int dat = llist_get(struct test_struct, e, list)->data;
        test_assert(dat == after[i]);
    }
    test_assert(i == sizeof(after)/sizeof(int));
    return true;
}

// Testing llist_insert function
bool llist_test_insert(va_list UNUSED) {
    int expected[] = {6, 3, 5, 2};
    test_assert(llist_insert(&test_elements[2].list, &test_elements[5].list));
    test_assert(!llist_insert(&test_elements[4].list, &test_elements[7].list));
    test_assert(!llist_insert(NULL, &test_elements[7].list));
    test_assert(!llist_insert(&test_elements[2].list, NULL));

    int i;
    struct _llist *e;
    e = llist_iter_end(&test_list);
    i = llist_get(struct test_struct, e, list)->data;
    for (i = 0, e = llist_riter_start(&test_list);
            e != llist_riter_end(&test_list);
            i++, e = llist_riter_next(e)) {
        int dat = llist_get(struct test_struct, e, list)->data;
        test_assert(dat == expected[i]);
    }
    test_assert(i == sizeof(expected)/sizeof(int));
    return true;
}

// Testing llist_prepend function
bool llist_test_prepend(va_list UNUSED) {
    int expected[] = {6, 3, 5, 2, 7, 0};
    test_assert(!llist_prepend(&test_list, NULL));
    test_assert(!llist_prepend(NULL, &test_elements[2].list));
    test_assert(llist_prepend(&test_list, &test_elements[7].list));
    test_assert(llist_prepend(&test_list, &test_elements[0].list));

    int i;
    struct _llist *e;
    for (i = 0, e = llist_riter_start(&test_list);
            e != llist_riter_end(&test_list);
            i++, e = llist_riter_next(e)) {
        int dat = llist_get(struct test_struct, e, list)->data;
        test_assert(dat == expected[i]);
    }
    test_assert(i == sizeof(expected)/sizeof(int));
    return true;
}

// Testing list_pop functions
bool llist_test_pop(va_list UNUSED) {
    int before[] = {6, 3, 0, 7, 5};
    int after[] = {2};
    int found[5];
    found[0] = llist_get(struct test_struct,
            llist_pop_back(&test_list), list)->data;
    found[1] = llist_get(struct test_struct,
            llist_pop_back(&test_list), list)->data;
    found[2] = llist_get(struct test_struct,
            llist_pop_front(&test_list), list)->data;
    found[3] = llist_get(struct test_struct,
            llist_pop_front(&test_list), list)->data;
    found[4] = llist_get(struct test_struct,
            llist_pop_back(&test_list), list)->data;

    int i;
    for (i = 0; i < 5; i++)
        test_assert(found[i] == before[i]);

    struct _llist *e;
    for (i = 0, e = llist_iter_start(&test_list);
            e != llist_iter_end(&test_list);
            i++, e = llist_iter_next(e)) {
        int dat = llist_get(struct test_struct, e, list)->data;
        test_assert(dat == after[i]);
    }
    test_assert(i == sizeof(after)/sizeof(int));
    return true;
}

// Testing llist_insert_ordered function
bool llist_test_ordered(va_list UNUSED) {
    int before[] = {5, 0, 2, 3};
    int expected[] = {0, 2, 3, 5};
    llist_init(&test_list);
    for (int i = 0; i < 4; i++)
        test_assert(llist_insert_ordered(&test_list,
                &test_elements[before[i]].list, list_less));

    for (int i = 0; i < 4; i++)
        test_assert(expected[i] == llist_get(struct test_struct,
                llist_pop_front(&test_list), list)->data);

    return true;
}

void llist_test (void) {
    for (int i = 0; i < 10; i++)
    {
        test_elements[i].data = i;
        test_elements[i].list.next = 0;
        test_elements[i].list.prev = 0;
    }

    testcase("null", llist_test_null, no_prefix, no_suffix);
    testcase("append", llist_test_append, no_prefix, no_suffix);
    testcase("remove", llist_test_remove, no_prefix, no_suffix);
    testcase("insert", llist_test_insert, no_prefix, no_suffix);
    testcase("prepend", llist_test_prepend, no_prefix, no_suffix);
    testcase("pop", llist_test_pop, no_prefix, no_suffix);
    testcase("ordered", llist_test_ordered, no_prefix, no_suffix);
}

