/* filename ******** test_lib_mem.c **************
 *
 * Test code for heap.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/04/2015
*/

#include "test.h"
#include <lib/types.h>
#include <lib/mem.h>
#include <stdlib.h>

#define TEST_SIZE 128
uint8_t *pointers[TEST_SIZE];

void shuffle_array(void **array, uint32_t size) {
    // Knuth shuffle
    for (int i = 0; i < size; i++) {
        int j = (rand() % (size - i)) + i;
        void *tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}

bool mem_chunk_test(va_list UNUSED) {
    for (int i = 0; i < TEST_SIZE; i++) {
        pointers[i] = chunk_alloc();
        for (int j = 0; j < CHUNK_SIZE; j++)
            pointers[i][j] = j;
    }

    shuffle_array((void **) pointers, TEST_SIZE);

    for (int i = 0; i < TEST_SIZE; i++) {
        for (int j = 0; j < CHUNK_SIZE; j++)
            test_assert(pointers[i][j] == j);
        chunk_dealloc(pointers[i]);
    }

    return true;
}

bool mem_heap_test(va_list UNUSED) {
    for (int i = 0; i < TEST_SIZE; i++) {
        pointers[i] = mem_alloc(2 * CHUNK_SIZE);
        for (int j = 0; j < 2 * CHUNK_SIZE; j++)
            pointers[i][j] = j;
    }

    shuffle_array((void **) pointers, TEST_SIZE);

    for (int i = 0; i < TEST_SIZE; i++) {
        for (int j = 0; j < 2 * CHUNK_SIZE; j++)
            test_assert(pointers[i][j] == j);
        mem_dealloc(pointers[i]);
    }

    return true;
}

void mem_test(void) {
    testcase("chunk", mem_chunk_test, no_prefix, no_suffix);
    testcase("heap", mem_heap_test, no_prefix, no_suffix);
}
