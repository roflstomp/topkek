/* filename ******** test_lib_pqueue.c **************
 *
 * Test code for pqueue.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/04/2015
*/

#include "test.h"
#include <lib/types.h>
#include <lib/pqueue.h>

struct pqueue __testpqueue;
struct pqueue *testpqueue = &__testpqueue;

bool less(void* a, void* b) {
    return a < b;
}

void pqueue_prefix(va_list args) {
    uint_t count = va_arg(args, uint_t);
    pqueue_init(testpqueue, count, less);
}

void pqueue_suffix(va_list args) {
    pqueue_deinit(testpqueue);
}

// Testing pqueue_insert function
bool pqueue_test_insert(va_list UNUSED) {
    pqueue_insert(testpqueue, (void *) 2);
    pqueue_insert(testpqueue, (void *) 1);
    test_assert(pqueue_peek(testpqueue) == (void *) 1);
    return true;
}

// Testing pqueue_pop function
bool pqueue_test_pop(va_list UNUSED) {
    pqueue_insert(testpqueue, (void *) 1);
    pqueue_insert(testpqueue, (void *) 2);
    pqueue_insert(testpqueue, (void *) 0);
    test_assert(pqueue_pop(testpqueue) == (void *) 0);
    test_assert(pqueue_pop(testpqueue) == (void *) 1);
    test_assert(pqueue_pop(testpqueue) == (void *) 2);
    return true;
}

// Testing pqueue_find function
bool pqueue_test_find(va_list UNUSED) {
    pqueue_insert(testpqueue, (void *) 1);
    pqueue_insert(testpqueue, (void *) 2);
    test_assert(pqueue_find(testpqueue, (void *) 0) == PQUEUE_NOT_FOUND);
    test_assert(pqueue_get(testpqueue, pqueue_find(testpqueue, (void *) 1)) ==
            (void *) 1);
    test_assert(pqueue_get(testpqueue, pqueue_find(testpqueue, (void *) 2)) ==
            (void *) 2);
    return true;
}

// Testing pqueue_remove function
bool pqueue_test_remove(va_list UNUSED) {
    pqueue_insert(testpqueue, (void *) 1);
    pqueue_insert(testpqueue, (void *) 2);
    pqueue_insert(testpqueue, (void *) 2);
    pqueue_insert(testpqueue, (void *) 2);
    test_assert(!pqueue_remove(testpqueue, (void *) 0));
    test_assert(pqueue_remove(testpqueue, (void *) 2));
    test_assert(pqueue_peek(testpqueue) == (void *) 1);
    test_assert(pqueue_remove(testpqueue, (void *) 2));
    test_assert(pqueue_remove(testpqueue, (void *) 2));
    test_assert(!pqueue_remove(testpqueue, (void *) 2));
    test_assert(pqueue_peek(testpqueue) == (void *) 1);
    test_assert(pqueue_remove(testpqueue, (void *) 1));
    test_assert(!pqueue_remove(testpqueue, (void *) 1));
    test_assert(pqueue_peek(testpqueue) == PQUEUE_INVALID);
    return true;
}

// Testing on a large dataset
bool pqueue_test_largeset(va_list UNUSED) {
    for (int i = 0; i < ARRAY_SIZE; i++)
        test_assert(pqueue_insert(testpqueue, (void *) unsorted[i]));

    for (int i = 0; i < ARRAY_SIZE; i++) {
        uint32_t index = pqueue_find(testpqueue, (void *) unsorted[i]);
        test_assert(index != PQUEUE_NOT_FOUND);
        uint32_t found = (uint32_t) pqueue_get(testpqueue, index);
        test_assert(found == unsorted[i]);
        test_assert(pqueue_remove(testpqueue, (void *) found));
    }
    test_assert(pqueue_peek(testpqueue) == PQUEUE_INVALID);
    return true;
}

// Testing heapsort
static uint32_t sorted[ARRAY_SIZE];
bool pqueue_test_heapsort(va_list UNUSED) {
    for (int i = 0; i < ARRAY_SIZE; i++)
        test_assert(pqueue_insert(testpqueue, (void *) unsorted[i]));

    sorted[0] = (uint32_t) pqueue_pop(testpqueue);
    for (int i = 1; i < ARRAY_SIZE; i++) {
        sorted[i] = (uint32_t) pqueue_pop(testpqueue);
        test_assert(sorted[i] > sorted[i-1]);
    }
    return true;
}

void pqueue_test(void) {
    testcase("insert", pqueue_test_insert, pqueue_prefix, pqueue_suffix, 8);
    testcase("pop", pqueue_test_pop, pqueue_prefix, pqueue_suffix, 8);
    testcase("find", pqueue_test_find, pqueue_prefix, pqueue_suffix, 8);
    testcase("remove", pqueue_test_remove, pqueue_prefix, pqueue_suffix, 8);
    testcase("largeset", pqueue_test_largeset,
            pqueue_prefix, pqueue_suffix, ARRAY_SIZE);
    testcase("heapsort", pqueue_test_heapsort,
            pqueue_prefix, pqueue_suffix, ARRAY_SIZE);
}
