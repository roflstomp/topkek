/* filename ******** test_lib_vector.c **************
 *
 * Test code for vector.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/04/2015
*/

#include "test.h"
#include <lib/types.h>
#include <lib/vector.h>

struct vector __test_vector;
struct vector *test_vector = &__test_vector;

void vector_prefix(va_list args) {
    vector_init(test_vector);
}

void vector_suffix(va_list args) {
    vector_deinit(test_vector);
}

bool vector_test_init(va_list UNUSED) {
    for (int i = 0; i < 8; i++)
        vector_set(test_vector, i, (void *) i);
    for (int i = 0; i < 8; i++)
        test_assert(vector_get(test_vector, i) == (void *) i);
    test_assert(vector_get(test_vector, 8) == VECTOR_INVALID);
    return true;
}

bool vector_test_append(va_list UNUSED) {
    for (int i = 0; i < 16; i++)
        vector_set(test_vector, i, (void *) i);
    for (int i = 0; i < 16; i++)
        test_assert(vector_get(test_vector, i) == (void *) i);
    return true;
}

bool vector_test_large_grow(va_list UNUSED) {
    for (int i = 0; i < 8; i++)
        vector_set(test_vector, i, (void *) i);

    vector_set(test_vector, 64, (void *) 64);

    int i;
    for (i = 0; i < 8; i++)
        test_assert(vector_get(test_vector, i) == (void *) i);
    for (; i < 64; i++)
        test_assert(vector_get(test_vector, i) == VECTOR_INVALID);
    test_assert(vector_get(test_vector, 64) == (void *) 64);
    for (i = 65; i < 128; i++)
        test_assert(vector_get(test_vector, i) == VECTOR_INVALID);
    return true;
}

bool vector_test_find(va_list UNUSED) {
    vector_set(test_vector, 64, (void *) 64);
    for (int i = 0; i < 8; i++)
        test_assert(vector_get(test_vector, i) == VECTOR_INVALID);
    test_assert(vector_get(test_vector, 64) == (void *) 64);
    for (int i = 65; i < 128; i++)
        test_assert(vector_get(test_vector, i) == VECTOR_INVALID);

    test_assert(vector_find(test_vector, (void *) 64) == 64);
    return true;
}

bool vector_test_largeset(va_list UNUSED) {
    for (int i = 0; i < ARRAY_SIZE; i++)
        vector_set(test_vector, i, (void *) unsorted[i]);
    for (int i = 0; i < ARRAY_SIZE; i++)
        test_assert(vector_get(test_vector, i) == (void *) unsorted[i]);
    return true;
}

void quicksort(struct vector *vec, int_t low, int_t high) {
    if (low >= high) return;

    int p = low;
    for (int i = p + 1; i <= high; i++) {
        void *vi = vector_get(vec, i),
             *vp = vector_get(vec, p);
        if (vi < vp) {
            vector_set(vec, i, vp);
            vector_set(vec, p, vi);
            if (i != p + 1) {
                void *tmp = vector_get(vec, p + 1);
                vi = vector_get(vec, i);
                vector_set(vec, i, tmp);
                vector_set(vec, p + 1, vi);
            }
            p = p + 1;
        }
    }

    quicksort(vec, low, p - 1);
    quicksort(vec, p + 1, high);
}

bool vector_test_quicksort(va_list UNUSED) {
    for (int i = 0; i < ARRAY_SIZE; i++)
        vector_set(test_vector, i, (void *) unsorted[i]);
    quicksort(test_vector, 0, ARRAY_SIZE - 1);

    for (int i = 0; i < ARRAY_SIZE; i++)
        test_assert(vector_find(test_vector, (void* )unsorted[i]) != -1);

    int i;
    void *last = vector_get(test_vector, 0);
    test_assert(last != VECTOR_INVALID);
    for (i = 1; i < ARRAY_SIZE; i++) {
        void *cur = vector_get(test_vector, i);
        test_assert(last <= cur);
        last = cur;
    }

    return true;
}

void merge(struct vector *vec, int_t lowl, int_t highl,
        int_t lowr, int_t highr) {
    int tmp_low = lowl;
    int tmp_idx = 0;
    struct vector tmp_vec;
    vector_init(&tmp_vec);

    while (lowl <= highl && lowr <= highr) {
        void *vlowl = vector_get(vec, lowl),
             *vlowr = vector_get(vec, lowr);
        if (vlowl < vlowr) {
            vector_set(&tmp_vec, tmp_idx++, vlowl);
            lowl++;
        } else if (vlowr < vlowl) {
            vector_set(&tmp_vec, tmp_idx++, vlowr);
            lowr++;
        } else {
            vector_set(&tmp_vec, tmp_idx++, vlowl);
            vector_set(&tmp_vec, tmp_idx++, vlowr);
            lowl++;
            lowr++;
        }
    }

    while (lowl <= highl)
        vector_set(&tmp_vec, tmp_idx++, vector_get(vec, lowl++));
    while (lowr <= highr)
        vector_set(&tmp_vec, tmp_idx++, vector_get(vec, lowr++));

    for (int i = 0; i < tmp_idx; i++) {
        vector_set(vec, tmp_low + i, vector_get(&tmp_vec, i));
    }
    vector_deinit(&tmp_vec);
}

void mergesort(struct vector *vec, int low, int high) {
    if (low >= high) return;

    int mid = low + (high - low) / 2;
    mergesort(vec, low, mid);
    mergesort(vec, mid + 1, high);
    merge(vec, low, mid, mid + 1, high);
}

bool vector_test_mergesort(va_list UNUSED) {
    for (int i = 0; i < ARRAY_SIZE; i++)
        vector_set(test_vector, i, (void *) unsorted[i]);
    mergesort(test_vector, 0, ARRAY_SIZE - 1);

    for (int i = 0; i < ARRAY_SIZE; i++)
        test_assert(vector_find(test_vector, (void* )unsorted[i]) != -1);

    int i;
    void *last = vector_get(test_vector, 0);
    test_assert(last != VECTOR_INVALID);
    for (i = 1; i < ARRAY_SIZE; i++) {
        void *cur = vector_get(test_vector, i);
        test_assert(last <= cur);
        last = cur;
    }

    return true;
}

void vector_test(void) {
    testcase("init", vector_test_init, vector_prefix, vector_suffix);
    testcase("append", vector_test_append, vector_prefix, vector_suffix);
    testcase("large_grow", vector_test_large_grow, vector_prefix, vector_suffix);
    testcase("find", vector_test_find, vector_prefix, vector_suffix);
    testcase("largeset", vector_test_largeset, vector_prefix, vector_suffix);
    testcase("quicksort", vector_test_quicksort, vector_prefix, vector_suffix);
    testcase("mergesort", vector_test_mergesort, vector_prefix, vector_suffix);
}
