
#include <os/time.h>
#include <os/pipe.h>

#include <io/print.h>
#include <periph/gpio.h>
#include <periph/adc.h>
#include <periph/tft.h>
#include <gui/screen.h>

struct pipe pipe;
struct adc adc0;
struct adc adc1;

void button_0(void);
void button_1(void);
void time_0(void);
void adc_0(void *eh, uint_t adc);
void time_1(void);
void adc_1(void *eh, uint_t adc);

struct task button_0_task = {
    .func = button_0,
    .stack = 128*4,
};

struct task button_1_task = {
    .func = button_1,
    .stack = 128*4,
};

struct task time_0_task = {
    .func = time_0,
    .stack = 128*4,
};

struct task time_1_task = {
    .func = time_1,
    .stack = 128*4,
};

struct task adc_0_task = {
    .func = adc_0,
    .stack = 128*4,
};

struct task adc_1_task = {
    .func = adc_1,
    .stack = 128*4,
};

void button_0(void) {
    *PIN_F2 = 0xff;
    pipe_give(&pipe, "Hi from button 1!\n");
    *PIN_F2 = 0x00;
}

void button_1(void) {
    *PIN_F1 = 0xff;
    pipe_give(&pipe, "Hi from button 2!\n");
    *PIN_F1 = 0x00;
}

void time_0(void) {
    time_call_in(100000, &time_0_task);
    adc_call_on_read(&adc0, &adc_0_task);
}

void time_1(void) {
    time_call_in(100010, &time_1_task);
    adc_call_on_read(&adc1, &adc_1_task);
}

void adc_0(void *eh, uint_t adc) {
    cprintf(screen_putc_left, "ADC 0 val: %d\n", adc);
}

void adc_1(void *eh, uint_t adc) {
    cprintf(screen_putc_left, "ADC 1 val: %d\n", adc);
}

void periph_test(void) {

    pipe_init(&pipe, 4);
    adc_init(&adc0, PIN_E4);
    adc_init(&adc1, PIN_E5);

    gpio_output(PIN_F1);
    gpio_output(PIN_F2);
    gpio_output(PIN_F3);

    gpio_input(PIN_F0);
    gpio_pull_up(PIN_F0);
    gpio_call_on_falling(PIN_F0, &button_0_task);

    gpio_input(PIN_F4);
    gpio_pull_up(PIN_F4);
    gpio_call_on_falling(PIN_F4, &button_1_task);

    time_call_in(100000, &time_0_task);
    time_call_in(100010, &time_1_task);

}

