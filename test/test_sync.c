/* filename ******** test_sync.c **************
 *
 * Test code for semaphores and locks
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 2/06/2014
*/

struct sema coord0;
struct sema coord1;

#include "test.h"
#include <lib/types.h>
#include <lib/common.h>
#include <lib/mem.h>
#include <os/thread.h>
#include <os/sync.h>

void sema_func(struct sema *sema, uint32_t *data, uint32_t val);
struct task sema_task0 = TASK_INIT(sema_func, 0x00, 2*128);
struct task sema_task1 = TASK_INIT(sema_func, 0x00, 2*128);
struct task sema_task2 = TASK_INIT(sema_func, 0x00, 2*128);

void sync_prefix(va_list args) {
    uint_t count0 = va_arg(args, uint_t);
    uint_t count1 = va_arg(args, uint_t);
    sema_init(&coord0, count0);
    sema_init(&coord1, count1);
}

void sync_suffix(va_list args) {
    sema_deinit(&coord0);
    sema_deinit(&coord1);
    for (int i = 0; i < 100; i++) {
        if (!(sema_task0.state == THR_DEAD &&
                sema_task1.state == THR_DEAD &&
                sema_task2.state == THR_DEAD))
            thread_yield();
        else return;
    }
    unreachable();
}

void sema_func(struct sema *sema, uint32_t *data, uint32_t val) {
    sema_down(&coord0);
    sema_down(sema);
    thread_yield();
    *data = val;
    sema_up(sema);
    sema_up(&coord1);
}

// single thread sync test
bool sync_test_single(va_list UNUSED) {
    struct sema share;

    int count = 0;
    sema_init(&share, 1);
    thread_preemption(0);

    thread_spawn(&sema_task0, &share, &count, 1);
    sema_up(&coord0);
    sema_down(&coord1);

    sema_deinit(&share);
    return count == 1;
}

// multiple thread sync test
bool sync_test_multiple(va_list UNUSED) {
    struct sema share;

    int count = 0;
    sema_init(&share, 1);
    thread_preemption(0);

    thread_spawn(&sema_task0, &share, &count, 2);
    sema_up(&coord0);
    thread_spawn(&sema_task1, &share, &count, 1);
    sema_up(&coord0);

    sema_down(&coord1);
    sema_down(&coord1);

    sema_deinit(&share);
    return count == 1;
}

// preemption sync test
bool sync_test_preempt(va_list UNUSED) {
    struct sema share;

    int count = 0;
    sema_init(&share, 1);
    thread_preemption(20000);

    thread_spawn(&sema_task0, &share, &count, 2);
    sema_up(&coord0);
    thread_spawn(&sema_task1, &share, &count, 1);
    sema_up(&coord0);

    for (int i = 0; i < 400000; i++)
        if (count == 1) break;

    thread_preemption(0);
    sema_deinit(&share);
    return count == 1;
}

// priority threading test
bool sync_test_priority(va_list UNUSED) {
    struct sema share;

    // NOTE: not portable!
    sema_task1.priority = 0x01;
    sema_task2.priority = 0x02;

    sema_init(&share, 1);
    thread_preemption(0);

    int count = 0;
    thread_spawn(&sema_task0, &share, &count, 3);
    thread_spawn(&sema_task1, &share, &count, 2);
    thread_spawn(&sema_task2, &share, &count, 1);

    // The thread with 0 priority should get the semaphore first
    for (int i = 0; i < 3; i++)
        sema_up(&coord0);

    for (int i = 0; i < 3; i++)
        sema_down(&coord1);

    sema_deinit(&share);
    return count == 1;
}

void sync_test(void) {
    testcase("single", sync_test_single, sync_prefix, sync_suffix, 0, 0);
    testcase("multiple", sync_test_multiple, sync_prefix, sync_suffix, 0, 0);
    testcase("preempt", sync_test_preempt, sync_prefix, sync_suffix, 0, 0);
    testcase("priority", sync_test_priority, sync_prefix, sync_suffix, 0, 0);
}

