/* filename ******** test_thread.c **************
 *
 * Test code for threading.
 *
 * Christopher Haster
 * Kevin George
 *
 * Used on board
 * 3/05/2015
*/

#include "test.h"
#include <lib/types.h>
#include <lib/common.h>
#include <lib/mem.h>
#include <os/thread.h>
#include <os/sync.h>

#define STACK_SIZE 128

void counter_inc(uint32_t set, uint32_t *data);

struct task test_task0 = TASK_INIT(counter_inc, 0x00, 128);
struct task test_task1 = TASK_INIT(counter_inc, 0x01, 128);
struct task test_task2 = TASK_INIT(counter_inc, 0x02, 128);


void thread_suffix(va_list args) {
    for (int i = 0; i < 100; i++) {
        if (!(test_task0.state == THR_DEAD &&
                test_task1.state == THR_DEAD &&
                test_task2.state == THR_DEAD))
            thread_yield();
        else return;
    }
    unreachable();
}

void counter_inc(uint32_t set, uint32_t *data) {
    *data = set;
    thread_yield();
}

// coop threading test
bool thread_test_coop(va_list UNUSED) {
    int count = 0;

    thread_preemption(0);
    thread_spawn(&test_task0, 1, &count);
    thread_yield();
    return count == 1;
}

// preemption threading test
bool thread_test_preempt(va_list UNUSED) {
    int count = 0;

    thread_preemption(1000000);
    thread_spawn(&test_task0, 1, &count);
    for (int i = 0; i < 4000000; i++)
        if (count == 1) break;
    return count == 1;
}

// priority threading test
bool thread_test_priority(va_list UNUSED) {
    int count = 0;

    thread_preemption(0);
    uint_t old = gl_acquire();
    thread_spawn(&test_task0, 3, &count);
    thread_spawn(&test_task1, 2, &count);
    thread_spawn(&test_task2, 1, &count);
    gl_release(old);

    for (int i = 0; i < 3; i++)
        thread_yield();

    return count == 1;
}

void thread_test(void) {
    testcase("coop", thread_test_coop, no_prefix, thread_suffix);
    testcase("preempt", thread_test_preempt, no_prefix, thread_suffix);
    testcase("priority", thread_test_priority, no_prefix, thread_suffix);
}

