
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <io/fs.h>
#include <io/fat.h>
#include <io/navi.h>

#define BLOCK_SIZE 512

#define test_assert(expr, fmt, ...) \
    if (!(expr)) { fprintf(stderr, fmt, __VA_ARGS__); return false; }

void *fat_lo = NULL;
char *lo_name = NULL;
unsigned char buffer[BLOCK_SIZE];
unsigned char large_buffer[BLOCK_SIZE * 128];
unsigned char large_rand[BLOCK_SIZE * 128];

bool fat_lo_read(block_t lba, uint8_t *buf) {
    memcpy(buf, fat_lo + (lba * BLOCK_SIZE), BLOCK_SIZE);
    return true;
}

bool fat_lo_write(block_t lba, uint8_t *buf) {
    memcpy(fat_lo + (lba * BLOCK_SIZE), buf, BLOCK_SIZE);
    return true;
}

struct device fat_loopback = {
    fat_lo_read,
    fat_lo_write,
    BLOCK_SIZE,
};

bool base_tests(void) {
    char name[MAX_FILENAME_LENGTH + 1];
    file_t root, f;

    test_assert(navi_mount(&fat_loopback),
            "%s: Cound not mount filesystem\n", lo_name);

    test_assert(navi_file_open(&fat_loopback, NULL, NULL, &root),
            "%s: Could not open root directory\n", lo_name);

    while (navi_next_entry(&root, name)) {
        printf("%s: %s\n", lo_name, name);
    }

    test_assert(navi_file_open(&fat_loopback, "hello.txt", &root, &f),
            "%s: Could not open file 'hello.txt'\n", lo_name);
    test_assert(navi_file_write(&f, "Hello World!", 0,
                sizeof("Hello World!")) == sizeof("Hello World!"),
            "%s: Could not write file\n", lo_name);

    memset(buffer, 0, sizeof(buffer));

    test_assert(navi_file_read(&f, buffer, 0, 512) != 0,
            "%s: Could not read file\n", lo_name);

    printf("%s[%s]: %s\n", lo_name, "hello.txt", buffer);

    test_assert(navi_file_read(&f, buffer, 1, 512) != 0,
            "%s: Could not read file\n", lo_name);

    return true;
}

bool large_tests(void) {
    file_t root, f;

    test_assert(navi_file_open(&fat_loopback, NULL, NULL, &root),
            "%s: Could not open root directory\n", lo_name);
    test_assert(navi_file_open(&fat_loopback, "hello.txt", &root, &f),
            "%s: Could not open file large.txt\n", lo_name);

    for (int i = 0; i < sizeof(buffer); i++)
        buffer[i] = i;

    test_assert(navi_file_write(&f, buffer, 0,
                sizeof(buffer)) == sizeof(buffer),
            "%s: Could not write file\n", lo_name);

    for (int i = 0; i < sizeof(buffer); i++)
        buffer[i] = 0;

    test_assert(navi_file_read(&f, buffer, 0,
                sizeof(buffer)) == sizeof(buffer),
            "%s: Could not read file\n", lo_name);

    for (int i = 0; i < sizeof(buffer); i++)
        test_assert(buffer[i] == (uint8_t) i, "%s: Block read failed: %d\n", lo_name, i);

    printf("%s: Passed block read/write tests\n", lo_name);

    for (uint32_t i = 0; i < sizeof(large_rand); i++) {
        large_rand[i] = rand();
    }

    memcpy(large_buffer, large_rand, sizeof(large_buffer));

    test_assert(navi_file_write(&f, large_buffer, 0,
                sizeof(large_buffer)) == sizeof(large_buffer),
            "%s: Could not write file\n", lo_name);

    for (int i = 0; i < sizeof(large_buffer); i++)
        large_buffer[i] = 0;

    test_assert(navi_file_read(&f, large_buffer, 0,
                sizeof(large_buffer)) == sizeof(large_buffer),
            "%s: Could not read file\n", lo_name);

    for (int i = 0; i < sizeof(large_buffer); i++) {
        test_assert(large_buffer[i] == (uint8_t) i,
                "%s: Block read failed: %d\n", lo_name, i);
    }

    printf("%s: Passed multi-block read/write tests\n", lo_name);

    return true;
}

bool create_tests(void) {
    char name[MAX_FILENAME_LENGTH + 1];
    file_t root, f;

    test_assert(navi_file_open(&fat_loopback, NULL, NULL, &root),
            "%s: Could not open root directory\n", lo_name);

    test_assert(navi_file_create(&root, "new.txt",false),
            "%s: Could not create new file\n", lo_name);
    test_assert(navi_file_open(&fat_loopback, "new.txt", &root, &f),
            "%s: Could not open file 'new.txt'\n", lo_name);
    test_assert(navi_file_write(&f, "Goodbye World!", 0,
                sizeof("Goodbye World!")) == sizeof("Goodbye World!"),
            "%s: Could not write file\n", lo_name);

    return true;
}

bool remove_tests(void) {
    char name[MAX_FILENAME_LENGTH + 1];
    file_t root, f;

    test_assert(navi_file_open(&fat_loopback, NULL, NULL, &root),
            "%s: Could not open root directory\n", lo_name);

    test_assert(navi_file_remove(&root, "new.txt"),
            "%s: Could not open file 'new.txt'\n", lo_name);
    test_assert(!navi_file_open(&fat_loopback, "new.txt", &root, &f),
            "%s: File not deleted 'new.txt'\n", lo_name);

    return true;
}

int main(int argc, char **argv) {
    bool ret;
    int err;

    if (argc != 2) {
        fprintf(stderr, "Invalid number of arguments\n");
        return -1;
    }
    lo_name = argv[1];

    // Open the file and map it into memory
    int fat_lo_fd = open(lo_name, O_SYNC | O_RDWR | O_LARGEFILE);
    err = errno;

    if (fat_lo_fd == -1)
        goto err0;

    struct stat st;
    fstat(fat_lo_fd, &st);

    fat_lo = mmap(0, 16*1024*1024, PROT_READ | PROT_WRITE, MAP_SHARED, fat_lo_fd, 0);
    err = errno;

    if ((int32_t) fat_lo == -1)
        goto err0;

    file_t d, f;
    if(!base_tests()) {
        ret = false;
        fprintf(stderr, "%s: Failed base tests!\n", lo_name);
        goto err1;
    }

    if(!large_tests()) {
        ret = false;
        fprintf(stderr, "%s: Failed large tests!\n", lo_name);
        goto err1;
    }

    if(!create_tests()) {
        ret = false;
        fprintf(stderr, "%s: Failed create tests!\n", lo_name);
        goto err1;
    }

    if(!remove_tests()) {
        ret = false;
        fprintf(stderr, "%s: Failed create tests!\n", lo_name);
        goto err1;
    }

err1:
    munmap(fat_lo, st.st_size);
    close(fat_lo_fd);
    return (ret) ? 0 : -1;

err0:
    fprintf(stderr, "Unable to open the file '%s'\n", lo_name);
    return -2;
}

